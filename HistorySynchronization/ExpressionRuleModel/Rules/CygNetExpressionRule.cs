﻿using GenericRuleModel.Rules;
using System.ComponentModel;
using TechneauxReportingDataModel.CygNet.Rules;
using XmlDataModelUtility;

namespace ExpressionRuleModel.Rules
{
    public class CygNetExpressionRule : NotifyCopyDataModel
    {
        public CygNetExpressionRule() : this(true)
        {
        }

        public CygNetExpressionRule(bool IsFullRuleVal = true)
        {
            //Default params
            if (IsFullRuleVal)
            {
                CygRule = new CygNetRollupRuleBase();
            }
            else
            {
                CygRule = new CygNetRuleBase();
            }
            RuleType = CygNetExpRuleType.CygNetElement;

            //Default to long
            IsFullRule = IsFullRuleVal;
        }


        //********************************** CygNetRule Attributes **********************************
        public bool IsFullRule;

        //Cygnet Input Configuration
        public enum CygNetExpRuleType
        {
            [Description("CygNet Element")]
            CygNetElement,

            [Description("Random Number")]
            RandomNumber
        }

        [HelperClasses.ControlTitle("CygNet Rule Type", "Type")]
        public CygNetExpRuleType RuleType
        {
            get => GetPropertyValue<CygNetExpRuleType>();
            set => SetRuleType(value);
        }

        private void SetRuleType(CygNetExpRuleType value)
        {
            if (!value.Equals(RuleType))
            {
                switch (value)
                {
                    case CygNetExpRuleType.CygNetElement:
                        if (IsFullRule)
                        {
                            if (!(CygRule is CygNetRollupRuleBase))
                                CygRule = new CygNetRollupRuleBase();
                        }
                        else
                        {
                            if (!(CygRule is CygNetRuleBase))
                                CygRule = new CygNetRuleBase();
                        }
                        break;
                    case CygNetExpRuleType.RandomNumber:
                        if (!(CygRule is RandomNumber))
                            CygRule = new RandomNumber();
                        break;
                    default:
                        if (!(CygRule is EmptyElement))
                            CygRule = new EmptyElement();
                        break;
                }
            }
            SetPropertyValue(value, nameof(RuleType));

        }

        //Rule Configuration
        public NotifyCopyDataModel CygRule
        {
            get => GetPropertyValue<NotifyCopyDataModel>();
            set => SetPropertyValue(value);
        }
    }
}
