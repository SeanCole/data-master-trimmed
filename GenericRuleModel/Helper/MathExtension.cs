﻿using System;

namespace GenericRuleModel.Helper
{
    public static class MathExtension
    {
        public static double CeilingWithDecimalPlaces(this double number, int digitsAfterPoint)
        {
            return Math.Ceiling(number * (double)Math.Pow(10, digitsAfterPoint))
                   / (double)Math.Pow(10, digitsAfterPoint);
        }
        public static double FlooringWithDecimalPlaces(this double number, int digitsAfterPoint)
        {
            return Math.Floor(number * (double)Math.Pow(10, digitsAfterPoint))
                   / (double)Math.Pow(10, digitsAfterPoint);
        }
        public static double RoundingWithDecimalPlaces(this double number, int digitsAfterPoint)
        {
            return Math.Round(number * (double)Math.Pow(10, digitsAfterPoint))
                   / (double)Math.Pow(10, digitsAfterPoint);
        }
    }
}
