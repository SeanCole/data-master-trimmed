﻿using System;
using System.Collections.Generic;

namespace GenericRuleModel.Resolvers
{
    public class RandomNumberGenerator
    {
        public static RandomNumberGenerator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new RandomNumberGenerator();
                }

                return instance;
            }
        }

        private static RandomNumberGenerator instance;
        private RandomNumberGenerator()
        {
            randomGenMap = new Dictionary<IntMinMax, Random>();
        }

        private readonly Dictionary<IntMinMax, Random> randomGenMap;
        public int Next(IntMinMax minMaxPair)
        {
            if (!randomGenMap.ContainsKey(minMaxPair))
                randomGenMap.Add(minMaxPair, new Random());
            //Note here that we are including the max value in our implementation
            return randomGenMap[minMaxPair].Next(minMaxPair.MinValue, minMaxPair.MaxValue+1);
        }
    }
    public struct IntMinMax
    {
        public IntMinMax(
            int newMin,
            int newMax)
        {
            MinValue = newMin;
            MaxValue = newMax;
        }

        public int MinValue;
        public int MaxValue;

        public override bool Equals(object obj)
        {
            var CompareObj = (IntMinMax)obj;
            return (MinValue == CompareObj.MinValue && MaxValue == CompareObj.MaxValue);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override string ToString()
        {
            return $"min={MinValue}|max={MaxValue}";
        }
    }
}
