﻿using GenericRuleModel.Helper;
using GenericRuleModel.Rules;
using Serilog;
using System;
using System.Globalization;
using Serilog.Context;
using Techneaux.CygNetWrapper;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace GenericRuleModel.Resolvers
{
    public class FormattingResolver
    {
        public static CygNetConvertableValue Resolve(CygNetConvertableValue resVal, FormattingOptions formatConfig)
        {
            try
            {
                //Check if for error
                if (resVal.HasError)
                {
                    return resVal;
                }

                //Check if formatting is enabled
                if (!formatConfig.EnableFormat)
                {
                    return resVal;
                }

                switch (formatConfig.DataType)
                {
                    case FormattingOptions.OutputDataType.Text:
                        //apply custom format
                        try
                        {
                            string formattedString;
                            if (formatConfig.CustomFormat == "")
                            {
                                formattedString = resVal.ToString();
                            }
                            else
                            {
                                formattedString = String.Format(formatConfig.CustomFormat, resVal.ToString(CultureInfo.InvariantCulture));
                            }
                            return new CygNetConvertableValue(formattedString);
                        }
                        catch (FormatException ex)
                        {
                            Log.Debug(ex, "Text formatting error");
                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Could not format variable (" + resVal.RawValue + ") with custom format: " + formatConfig.CustomFormat + " :ERROR: " + ex.Message);
                        }

                    case FormattingOptions.OutputDataType.Boolean:
                        if (resVal.TryGetBool(out var valBool))
                        {
                            return new CygNetConvertableValue(valBool);
                        }
                        else
                        {
                            return new CygNetConvertableValue(formatConfig.DefaultIfNotBoolean);
                        }

                    case FormattingOptions.OutputDataType.DateTime:
                        if (resVal.TryGetDateTime(out var valDateTime))
                        {
                            return new CygNetConvertableValue(valDateTime);
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(formatConfig.DefaultIfNotDateTime))
                            {
                                return new CygNetConvertableValue(formatConfig.DefaultIfNotDateTime);
                            }
                            else
                            {
                                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Could not format variable (" + resVal.RawValue + ") into a DateTime");
                            }
                        }

                    case FormattingOptions.OutputDataType.Numeric:
                        if (resVal.TryGetDouble(out var valDouble))
                        {
                            //Good, keep going
                        }
                        else
                        {
                            return new CygNetConvertableValue(formatConfig.DefaultIfNotNumeric);
                        }

                        //Scaling
                        switch (formatConfig.ScalingTypeChoice)
                        {
                            case FormattingOptions.ScalingType.CustomScaling:
                                valDouble = valDouble * formatConfig.ScalingFactor;
                                break;

                            case FormattingOptions.ScalingType.FeetToInches:
                                valDouble = valDouble * 12;
                                break;

                            case FormattingOptions.ScalingType.InchesToFeet:
                                valDouble = valDouble / 12;
                                break;

                            case FormattingOptions.ScalingType.NoScaling:
                                break;

                            default:
                                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Unsupported FormatConfig for ScalingTypeChoice.  Type found was: " + formatConfig.ScalingTypeChoice);
                        }

                        // NumberPart NumberPartOption;
                        //int NumDecimalPlaces;
                        //Parsing
                        switch (formatConfig.NumberPartOption)
                        {
                            case FormattingOptions.NumberPart.DecimalPart:
                                //Truncate the digits before the period
                                var resDecPart = valDouble - Math.Truncate(valDouble);

                                //rounding with decimal places
                                switch (formatConfig.RoundingTypeChoice)
                                {
                                    case FormattingOptions.RoundingType.Ciel:
                                        valDouble = resDecPart.CeilingWithDecimalPlaces(formatConfig.NumDecimalPlaces);
                                        break;

                                    case FormattingOptions.RoundingType.Floor:
                                        valDouble = resDecPart.FlooringWithDecimalPlaces(formatConfig.NumDecimalPlaces);
                                        break;

                                    case FormattingOptions.RoundingType.Normal:
                                        valDouble = resDecPart.RoundingWithDecimalPlaces(formatConfig.NumDecimalPlaces);
                                        break;

                                    default:
                                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Unsupported FormatConfig for RoundingTypeChoice.  Type found was: " + formatConfig.RoundingTypeChoice);

                                }
                                return new CygNetConvertableValue(valDouble);

                            case FormattingOptions.NumberPart.FloatingPoint:
                                //rounding with decimal places
                                switch (formatConfig.RoundingTypeChoice)
                                {
                                    case FormattingOptions.RoundingType.Ciel:
                                        valDouble = valDouble.CeilingWithDecimalPlaces(formatConfig.NumDecimalPlaces);
                                        break;

                                    case FormattingOptions.RoundingType.Floor:
                                        valDouble = valDouble.FlooringWithDecimalPlaces(formatConfig.NumDecimalPlaces);
                                        break;

                                    case FormattingOptions.RoundingType.Normal:
                                        valDouble = valDouble.RoundingWithDecimalPlaces(formatConfig.NumDecimalPlaces);
                                        break;

                                    default:
                                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Unsupported FormatConfig for RoundingTypeChoice.  Type found was: " + formatConfig.RoundingTypeChoice);

                                }
                                return new CygNetConvertableValue(valDouble);

                            case FormattingOptions.NumberPart.IntegerPart:
                                //Done here
                                var retVal = (int)valDouble;
                                return new CygNetConvertableValue(retVal);

                            default:
                                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Unsupported FormatConfig for NumberPartOption.  Type found was: " + formatConfig.NumberPartOption);

                        }

                    case FormattingOptions.OutputDataType.EnumerationTable:
                        //Look up the appropriate enum table
                        var tableName = formatConfig.TableName;
                        //EnumerationTable table = GlobalEnums?.GetTableByName(tableName);


                        //Look up value in the table
                        if (!EnumerationTableHandler.TryLookupValue(resVal.ToString(CultureInfo.InvariantCulture), tableName, out var value))
                        {
                            if (!string.IsNullOrEmpty(formatConfig.DefaultIfUnavailable))
                            {
                                return new CygNetConvertableValue(formatConfig.DefaultIfUnavailable);
                            }

                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Value in enumeration tale could not be found.  Table name: " + tableName + " :value name: " + resVal);
                        }
                        //string value = EnumerationTableHandler.LookupValue(resVal.ToString(), tableName);

                        if (value == null)
                        {
                            if (!string.IsNullOrEmpty(formatConfig.DefaultIfUnavailable))
                            {
                                return new CygNetConvertableValue(formatConfig.DefaultIfUnavailable);
                            }

                            return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Value in enumeration tale could not be found.  Table name: " + tableName + " :value name: " + resVal);
                        }
                        else
                        {
                            return new CygNetConvertableValue(value);
                        }

                    default:
                        return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.FormattingErr, "Unsupported FormatConfig for DataType.  Type found was: " + formatConfig.DataType);
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Debug(ex, "Unhandled exception parsing formatting rule");

                return new CygNetConvertableValue(CygNetConvertableValue.ErrorType.UnknownErr, "Uknown exception thrown[5C]: " + ex.Message);
            }
        }
    }
}
