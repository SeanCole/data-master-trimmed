﻿using Serilog;
using System;
using System.ComponentModel;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;


namespace GenericRuleModel.Rules
{
    public static class EnumerationTableHandler
    {
        public static bool AutoLoadNew { get; set; } = false;

        static EnumerationTableHandler()
        {
            // Load enums if available
            var loadedDef = EnumerationTableUtils.LoadEnumTables();

            EnumTableDefinition = loadedDef;

            // hook events for saving enums
            EnumTableDefinition.PropertyChanged += EnumTableDefinition_PropertyChanged;

            Observable.Interval(TimeSpan.FromSeconds(30))
                .Subscribe(async evt =>
                {
                    if (!AutoLoadNew) return;

                    try
                    {
                        await EnumTableLock.WaitAsync();

                        EnumTableDefinition = EnumerationTableUtils.LoadEnumTables();
                    }
                    finally
                    {
                        EnumTableLock.Release();
                    }
                });
        }

        private static SemaphoreSlim EnumTableLock { get; } = new SemaphoreSlim(1, 1);

        private static void EnumTableDefinition_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            Log.Debug($"Enum Tables Prop Change Event.");
            Log.Debug($"Enum table change : {e.PropertyName}");

            // if ((sender is EnumerationTables newTables))
            // {
            SaveAsync();

            //EnumerationTableUtils.SaveEnumTables(EnumTableDefinition);
            //}
        }

        private static async Task SaveAsync()
        {
            try
            {
                await EnumTableLock.WaitAsync();

                await EnumerationTableUtils.SaveEnumTables(EnumTableDefinition);
                return;

                Log.Debug("Enum tables cast failed for save.");
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
            finally
            {
                EnumTableLock.Release();
            }
        }

        public static bool TryLookupValue(string resVal, string tableName, out string value)
        {
            value = "";

            try
            {
                EnumTableLock.Wait();

                var table = GetTable(tableName);
                if (table == null)
                    return false;
                foreach (var pair in table.EnumerationPairs)
                {
                    if (pair.Key.Equals(resVal))
                    {
                        value = pair.Value;
                        return true;

                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while looking up enum table value");

                return false;
            }
            finally
            {
                EnumTableLock.Release();
            }
        }

        public static EnumerationTables EnumTableDefinition { get; private set; }

        private static EnumerationTable GetTable(string tableName)
        {
            foreach (var table in EnumTableDefinition.EnumTables)
            {
                if (table.Name.Equals(tableName))
                    return table;
            }

            return null;
        }
    }
}
