﻿using XmlDataModelUtility;

namespace GenericRuleModel.Rules
{
    public class RandomNumber : NotifyCopyDataModel
    {
        public RandomNumber()
        {
            //Default params
            MinValue = 0;
            MaxValue = 100;
        }

        //********************************** Sequential Number Attributes **********************************

        [HelperClasses.ControlTitle("Min", "Min")]
        public int MinValue
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [HelperClasses.ControlTitle("Max", "Max")]
        public int MaxValue
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }
    }
}
