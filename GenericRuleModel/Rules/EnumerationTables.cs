using System.ComponentModel;
using XmlDataModelUtility;

namespace GenericRuleModel.Rules
{
    public class EnumerationTables : NotifyCopyDataModel
    {
        public BindingList<EnumerationTable> EnumTables
        {
            get => GetPropertyValue<BindingList<EnumerationTable>>();
            set => SetPropertyValue(value);
        }

        public EnumerationTables()
        {
            EnumTables = new BindingList<EnumerationTable>();
        }

        // Classes (enum)

        public EnumerationTable GetTable(string tableName)
        {
            foreach (var table in EnumTables)
            {
                if (table.Name.Equals(tableName))
                    return table;
            }
            return null;
        }
    }
}
