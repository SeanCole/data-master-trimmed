﻿using CygNetRuleModel.Resolvers;
using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.CygNet.Utility;
using TechneauxReportingDataModel.SmartSheet;
using TechneauxUtility;

namespace TechneauxSmartSheetSync.Resolver
{
    public static class WriteReportToSmartSheet
    {
        public static async Task<SmartSheetSyncResults> UploadReport(
            CygNetGeneralOptions cygOpts,
            SmartSheetSyncConfigModel ssOpts,
            RollupPeriod rollupPeriod,
            CancellationToken ct)
        {


            try
            {
                // Validate cygnet options
                // -- when supported

                // Try to get CygNet facs
                //if(cygOpts.FacSiteService == null)
                //{
                //    return new SmartSheetSyncResults
                //    {
                //        TaskResult = SmartSheetTaskResults.FailedValidation,
                //        Details = $"Failed to get CygNet facilities with error: No FacSiteService selected."
                //    };
                //}
                
                var facRes = new CachedFacilityResolver(cygOpts);

                var facsSearch = await Task.Run(() => facRes.FindFacilities(ct));

                if (!facsSearch.SearchSucceeded || !facsSearch.FacsFound.IsAny())
                    return new SmartSheetSyncResults
                    {
                        TaskResult = SmartSheetTaskResults.FailedFacSearch,
                        Details = $"Failed to get CygNet facilities with error: {facsSearch.ExcpDetails}"
                    };

                var cachedFacs = facsSearch.FacsFound;

                // Try to get SmartSheet
                if (!ValidateSmartSheetOptions(ssOpts).isValid)
                    return new SmartSheetSyncResults
                    {
                        TaskResult = SmartSheetTaskResults.FailedValidation,
                        Details = ValidateSmartSheetOptions(ssOpts).message
                    };

                var sht = await Task.Run(() =>
                    SmartSheetModel.TryGetSheet(ssOpts.SmartSheetConnectOpts.AccessToken, ssOpts.SheetSelectionOpts));

                if (!sht.IsLinkedSheet)
                {
                    return new SmartSheetSyncResults
                    {
                        TaskResult = SmartSheetTaskResults.FailedSmartSheetConnection,
                        Details = SmartSheetTaskResults.FailedSmartSheetConnection.ToString()
                    };
                }

                // Get report rows
                var validRules = ssOpts.ColumnRules.Where(rule => rule.IsRuleValid).ToList();

                sht.RowColumnIdKey = ssOpts.SheetSelectionOpts.KeyColumn;

                if (!validRules.Any())
                {
                    return new SmartSheetSyncResults
                    {
                        TaskResult = SmartSheetTaskResults.NoValidRules,
                        Details = SmartSheetTaskResults.NoValidRules.ToString()
                    };
                }
                
                var reportRows = await Task.Run(() => GetReportRows(cygOpts, rollupPeriod, cachedFacs, validRules, ct));

                // Populate smartsheet
                var keyValId = ssOpts.SheetSelectionOpts.KeyColumn;

                foreach (var row in reportRows)
                {
                    var shtRow = sht.GetRowByKeyValue(row[keyValId.Name].StringValue);


                    if (shtRow == null)
                        continue;


                    foreach (var key in row.CellValues.Keys)
                    {
                        var str = row.CellValues[key].RawValue;
                        shtRow.SetCellValue(sht.ColNameToId[key], str);
                    }
                }

                sht.FinalizeUpdates();
                return new SmartSheetSyncResults
                {
                    TaskResult = SmartSheetTaskResults.Successful,
                    Details = "Success"
                };
            }
            catch (OperationCanceledException cx)
            {
                Log.Error(cx, "Cancellation occured.");
                return new SmartSheetSyncResults
                {
                    TaskResult = SmartSheetTaskResults.UnknownError,
                    Details = $"Cancellation occured: {cx.Message} ."
                };
            }
            catch (Exception e)
            {
                Log.Error(e, "General failure");

                return new SmartSheetSyncResults
                {
                    TaskResult = SmartSheetTaskResults.FailedValidation,
                    Details = $"General Failure: {e.Message} . Please check the log for more details."
                };
            }

            return new SmartSheetSyncResults
            {
                TaskResult = SmartSheetTaskResults.Successful,
                Details = ""
            };
        }


        private static async Task<List<SmartSheetHistoryRow>> GetReportRows(CygNetGeneralOptions cygOpts, RollupPeriod rollupPeriod, List<Techneaux.CygNetWrapper.Facilities.CachedCygNetFacility> cachedFacs, List<TechneauxReportingDataModel.SmartSheet.Enumerable.SmartSheetReportElm> validRules, CancellationToken ct)
        {
            var reportRows = new List<SmartSheetHistoryRow>();
            foreach (var fac in cachedFacs)
            {
                var thisRow = await ResolveSmartsheet.ResolveSingleRow(fac,
                    cygOpts.FacilityFilteringRules.ChildGroups, validRules, rollupPeriod, ct);

                reportRows.Add(thisRow);
            }

            return reportRows;
        }



        private static (bool isValid, string message) ValidateSmartSheetOptions(
            SmartSheetSyncConfigModel ssOpts)
        {
            if (!ssOpts.SmartSheetConnectOpts.IsRuleValid)
                return (false, ssOpts.SmartSheetConnectOpts.ValidationErrorMessage);

            if (!ssOpts.SheetSelectionOpts.IsRuleValid)
                return (false, ssOpts.SheetSelectionOpts.ValidationErrorMessage);

            return (true, "No Error");
        }
    }

    public class SmartSheetSyncResults
    {
        public SmartSheetTaskResults TaskResult { get; set; }

        public string Details { get; set; }

        public bool WasSuccessful => TaskResult == SmartSheetTaskResults.Successful;
    }


    public enum SmartSheetTaskResults
    {
        [Description("Successful")] Successful,

        [Description("Failed validation")] FailedValidation,

        [Description("Failed to find CygNet facilities")]
        FailedFacSearch,

        [Description("Failed to connect to specified SmartSheet")]
        FailedSmartSheetConnection,

        [Description("No valid rules found")] NoValidRules,

        [Description("Unknown Error")]
        UnknownError
    }
}