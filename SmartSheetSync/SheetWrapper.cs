﻿using Smartsheet.Api;
using Smartsheet.Api.Models;
using Serilog;
using Serilog.Context;
using System;
using System.Collections.Generic;
using static XmlDataModelUtility.GlobalLoggers;
using System.Linq;
using TechneauxReportingDataModel.SmartSheet.SubOptions;

namespace TechneauxSmartSheetSync
{
    public class SheetWrapper
    {
        SmartsheetClient ParentSheetClient { get; set; }

        public Sheet ParentSheet { get; set; }

        public SmartSheetItemId SheetId { get; }

        private SmartSheetItemId _RowColumnIdKey;

        public SmartSheetItemId RowColumnIdKey
        {
            get => _RowColumnIdKey;
            set
            {
                if (!ColNameToId.ContainsKey(value.Name))
                    throw new ArgumentException("Row column id key not found");

                _RowColumnIdKey = value;

                foreach (var row in ParentSheet.Rows)
                {
                    var thisRowWrapper = new RowWrapper(row, this);

                    var rowVal = thisRowWrapper.GetCellValue(value.Id);
                    if (rowVal != null && !Rows.ContainsKey(rowVal.ToString()))
                    {
                        Rows[rowVal.ToString()] = thisRowWrapper;
                    }
                }
            }
        }

        public bool IsLinkedSheet { get; } = false;

        public SheetWrapper(
            SmartsheetClient parentClient,
            Sheet srcSheet)
        {
            ParentSheetClient = parentClient;
            ParentSheet = srcSheet;

            if (ParentSheet.Columns != null)
            {
                ColIdToName = ParentSheet.Columns.ToDictionary(col => col.Id.Value, col => col.Title);
                ColNameToId = ParentSheet.Columns.ToDictionary(col => col.Title, col => col.Id.Value);

                ColumnIds = ParentSheet.Columns.Select(col => new SmartSheetItemId() { Id = col.Id.Value, Name = col.Title }).ToList();
            }
            else
            {
                ColIdToName = new Dictionary<long, string>();
                ColNameToId = new Dictionary<string, long>();
                ColumnIds = new List<SmartSheetItemId>();
            }
            
            IsLinkedSheet = true;

            SheetId = new SmartSheetItemId() { Id = srcSheet.Id.Value, Name = srcSheet.Name };
        }


        public SheetWrapper()
        {
            ParentSheetClient = null;
            ParentSheet = null;
            SheetId = new SmartSheetItemId() { Id = long.MinValue, Name = "" };
                   ColIdToName = new Dictionary<long, string>();
            ColNameToId = new Dictionary<string, long>();

            ColumnIds = new List<SmartSheetItemId>();

            Rows = new Dictionary<string, RowWrapper>();
          //  RowColumnIdKey = null;

     

            IsLinkedSheet = false;


        }

        public List<SmartSheetItemId> ColumnIds { get; }

        public Dictionary<string, long> ColNameToId { get; }

        public Dictionary<long, string> ColIdToName { get; }

        public Dictionary<string, RowWrapper> Rows { get; private set; } = new Dictionary<string, RowWrapper>();

        public RowWrapper GetRowByKeyValue(object keyVal)
        {
            if (!Rows.TryGetValue(keyVal.ToString(), out var row))
            {
                if (keyVal == null)
                    return null;

                row = new RowWrapper(this);
                
                row.SetCellValue(RowColumnIdKey.Id, keyVal);

                Rows[keyVal.ToString()] = row;
            }

            return row;
        }


        public override bool Equals(object obj)
        {
            if (obj != null && obj is SheetWrapper)
            {
                return SheetId.Equals((obj as SheetWrapper).SheetId);
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return SheetId.GetHashCode();
        }

        public void FinalizeUpdates()
        {
            var UpdatedRows = Rows.Values
                .Where(row => !row.IsNew)
                .Select(roww => roww.GetFinalRow()).ToList();

            var NewRows = Rows.Values
                .Where(row => row.IsNew)
                .Select(roww => roww.GetFinalRow()).ToList();

            //foreach (var row in UpdatedRows)
            //{
            //    ParentSheetClient.SheetResources.RowResources.UpdateRows(ParentSheet.Id.Value, new List<Row>{row});
            //}
            foreach (var updatedRow in UpdatedRows)
            {
                try
                {
                    ParentSheetClient.SheetResources.RowResources.UpdateRows(ParentSheet.Id.Value, new List<Row> { updatedRow });
                }
                catch (Exception ex)
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                        Log.Error(ex, "Failed to add new row to smartsheet.");
                }
            }
            foreach (var newRow in NewRows)
            {
                try
                {
                    ParentSheetClient.SheetResources.RowResources.AddRows(ParentSheet.Id.Value, new List<Row> { newRow });
                }
                catch (Exception ex)
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                        Log.Error(ex, "Failed to add new row to smartsheet.");
                }
            }
            //ParentSheetClient.SheetResources.RowResources.AddRows(ParentSheet.Id.Value, NewRows);
        }
    }
}
