using System.Collections.Generic;
using System.Linq;
using TechneauxReportingDataModel.Helper;
using static TechneauxReportingDataModel.ExcelTextFile.Enumerable.ReportElement;

namespace TechneauxReportingDataModel.ExcelTextFile.Enumerable
{
    public static class ReportElmInformation
    {
        public static ElementProperties GetElmProperties(this IList<ReportElement> ReportElements, ReportElement thisElm)
        {
            var ThisInfo = new ElementProperties(
                        thisElm,
                        ReportElements.IndexOf(thisElm),
                        ReportElements.GroupedElms().IndexOf(thisElm),
                        ReportElements.UngroupedElms().IndexOf(thisElm),
                        ReportElements.GroupedElms().Count,
                        ReportElements.UngroupedElms().Count,
                        ReportElements.Count(elm => elm.IsTableHistoryValue));

            return ThisInfo;
        }

        public static List<ElementProperties> ElmsInfoNew(this IList<ReportElement> ReportElements)
        {
            var ElmIds = new List<ElementProperties>();

            foreach (var elm in ReportElements)
            {
                ElmIds.Add(ReportElements.GetElmProperties(elm));
            }

            return ElmIds.OrderBy(thisId => thisId.Index).ToList();
        }

        //Get grouped elms
        public static List<ReportElement> GroupedElms(this IList<ReportElement> ReportElements)
        {
            return ReportElements.Where(elm => elm.IsGrouped).ToList();
        }

        //Get ungrouped elms
        public static List<ReportElement> UngroupedElms(this IList<ReportElement> ReportElements)
        {
            return ReportElements.Where(elm => !elm.IsGrouped).ToList();
        }

        public static bool HasGrouping(this IList<ReportElement> ReportElements)
        {
            return ReportElements.GroupedElms().Any();
        }

        public static bool HasSummary(this IList<ReportElement> ReportElements)
        {
            return ReportElements.Any(elm => elm.SummaryType != SummaryTypes.None);
        }
    }
}
