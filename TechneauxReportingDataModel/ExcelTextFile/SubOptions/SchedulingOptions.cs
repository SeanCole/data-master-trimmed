using System;
using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class SchedulingOptions : NotifyCopyDataModel
    {
        public enum ScheduleType
        {
            RelativeToCurrentDate,
            FromFixedDateRange
        }

        public  enum TimeDurationUnits
        {
            [Description("Week(s)")]
            Weeks,

            [Description("Day(s)")]
            Days,

            [Description("Hour(s)")]
            Hours,

            [Description("Minute(s)")]
            Minutes
        }

        public SchedulingOptions()
        {
            ReportDurationUnits = TimeDurationUnits.Days;
            RelativeDaysForStart = 0;
            StartTime = DateTime.Now;
            DurationUnitCount = 1;
        }

        [XmlAttribute]
        public TimeDurationUnits ReportDurationUnits
        {
            get => GetPropertyValue<TimeDurationUnits>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int RelativeDaysForStart
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public DateTime StartTime
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int DurationUnitCount
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }
    }
}
