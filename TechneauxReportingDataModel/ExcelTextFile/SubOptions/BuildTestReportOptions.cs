using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class BuildTestReportOptions : NotifyCopyDataModel
    {



        [XmlIgnore()]
        public bool UseTestFileName { set; get; } = false;


    }
}
