using System.ComponentModel;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class DataValueSummaryOptions : NotifyCopyDataModel
    {
        public enum EnumTableValueTypes
        {
            [Description("Count Only")]
            Count,

            [Description("% Only")]
            Percent,

            [Description("Both")]
            CountAndPercent
        }

        public DataValueSummaryOptions()
        {
            ExcelTemplateOpts = new ExcelTemplateOptions();
        }

        public ExcelTemplateOptions ExcelTemplateOpts
        {
            get => GetPropertyValue<ExcelTemplateOptions>();
            set => SetPropertyValue(value);
        }

        public EnumTableValueTypes EnumTableValueType
        {
            get => GetPropertyValue<EnumTableValueTypes>();
            set => SetPropertyValue(value);
        }

    }
}
