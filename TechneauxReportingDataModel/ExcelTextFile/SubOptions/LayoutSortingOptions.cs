using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class LayoutSortingOptions : NotifyCopyDataModel
    {
        public LayoutSortingOptions()
        {
            // Layout and Sorting
            KeepDaysTogether = true;
            KeepFacilitiesTogether = true;

            FacilitySortOrder = SortTypesLimitedNew.Ascending;
            ReportDateOrder = ReportDateOrders.NewerDatesHigher;
        }

        public enum RowColumnDateLayout
        {
            [Description("One Row per Date")]
            OneRowPerDate,

            [Description("One Column per Date")]
            OneColumnPerDate
        }

        public enum ReportDateOrders
        {
            [Description("Newer Dates Lower")]
            NewerDatesLower,

            [Description("Newer Dates Higher")]
            NewerDatesHigher
        }

        [XmlAttribute]
        public ReportDateOrders ReportDateOrder
        {
            get => GetPropertyValue<ReportDateOrders>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool KeepDaysTogether
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public bool KeepFacilitiesTogether
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        public enum SortTypesLimitedNew
        {
            [Description("Ascending")]
            Ascending,

            [Description("Descending")]
            Descending
        }

        [XmlAttribute]
        public SortTypesLimitedNew FacilitySortOrder
        {
            get => GetPropertyValue<SortTypesLimitedNew>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public BindingList<KeyValuePair<string, int>> BindingSortedItemList { get; } = new BindingList<KeyValuePair<string, int>>();

        [XmlAttribute]
        public int DateSortNumber
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public int FacSortNumber
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        private SortedList<int, KeyValuePair<string, int>> SortingList = new SortedList<int, KeyValuePair<string, int>>();

        public const int ConstDateSortItemNumber = -10;
        public const int ConstFacSortItemNumber = -20;

    }
}
