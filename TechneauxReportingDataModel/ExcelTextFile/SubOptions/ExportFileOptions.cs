using System.ComponentModel;
using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.ExcelTextFile.SubOptions
{
    public class ExportFileOptions : NotifyCopyDataModel
    {
        public enum FileFormats
        {
            [Description("Text - Comma Separated (.csv)")]
            csv,
            
            [Description("Text - Tab Delimited (.txt)")]
            txtTab,

            [Description("Text - Semicolon Delimited (.txt)")]
            txtSemi,

            [Description("Excel (.xlsx)")]
            xlsx
        }

        public enum FileFormatsTextOnly
        {
            [Description("Text - Comma Separated (.csv)")]
            csv,

            [Description("Text - Tab Delimited (.txt)")]
            txtTab,

            [Description("Text - Semicolon Delimited (.txt)")]
            txtSemi
        }

        public enum FileFormatsExcelOnly
        {
            [Description("Excel (.xlsx)")]
            xlsx
        }

        public ExportFileOptions()
        {
            // Export file options
            ExportName = "";
            ExportPath = "";
            FileFormat = FileFormats.csv;
            FileNameDateSource = DateSources.LatestContractStart;
        }

        [XmlIgnore()]
        public string DelimChar
        {
            get
            {
                switch (FileFormat) //Switches between the delimiter types
                {
                    case FileFormats.csv:
                        return ",";

                    case FileFormats.txtTab:
                        return "\t";

                    case FileFormats.txtSemi:
                        return ";";
                    default:
                        return ",";
                }
            }
        }


        // Export file options
        [XmlAttribute]
        public string ExportName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string ExportPath
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        
        public enum DateSources
        {
            [Description("Latest Contract Start")]
            LatestContractStart,

            [Description("Latest Contract End")]
            LatestContractEnd,

            [Description("Current Date & Time")]
            CurrentDateTime
        }

        [XmlAttribute]
        public DateSources FileNameDateSource
        {
            get => GetPropertyValue<DateSources>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        [XmlIgnore]
        public FileFormats FileFormat { get; private set; }
    }
}
