using System.ComponentModel;
using TechneauxReportingDataModel.SmartSheet.Enumerable;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SmartSheet
{
    public class SmartSheetSyncConfigModel : NotifyCopyDataModel
    {
        public SmartSheetSyncConfigModel()
        {
            SmartSheetConnectOpts = new SmartSheetConnectionOptions();
            SheetSelectionOpts = new SheetSelectionOptions();
            ColumnRules = new BindingList<SmartSheetReportElm>();
        }

        public SmartSheetConnectionOptions SmartSheetConnectOpts
        {
            get => GetPropertyValue<SmartSheetConnectionOptions>();
            set => SetPropertyValue(value);
        }

        public SheetSelectionOptions SheetSelectionOpts
        {
            get => GetPropertyValue<SheetSelectionOptions>();
            set => SetPropertyValue(value);
        }

        public BindingList<SmartSheetReportElm> ColumnRules
        {
            get => GetPropertyValue<BindingList<SmartSheetReportElm>>();
            set => SetPropertyValue(value);
        }
    }
}
