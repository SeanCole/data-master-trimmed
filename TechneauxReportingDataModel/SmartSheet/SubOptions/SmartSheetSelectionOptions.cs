﻿using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SmartSheet.SubOptions
{
    public class SheetSelectionOptions : NotifyCopyDataModel, IValidatedRule
    {
        public SheetSelectionOptions() { SheetId = new SmartSheetItemId();
            KeyColumn = new SmartSheetItemId();
        }

        public SmartSheetItemId SheetId
        {
            get => GetPropertyValue<SmartSheetItemId>();
            set => SetPropertyValue(value);
        }

        public SmartSheetItemId KeyColumn
        {
            get => GetPropertyValue<SmartSheetItemId>();
            set => SetPropertyValue(value);
        }

        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!SheetId.IsRuleValid)
                    return (false, $"Sheet Selection is invalid because SheetId is null or blank.");

                if (!KeyColumn.IsRuleValid)
                    return (false, $"Key Column Selection is invalid because KeyColumn is null or blank.");

                return (true, "");
            }
        }
    }
}
