﻿using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.SmartSheet.SubOptions
{
    public class SmartSheetConnectionOptions : NotifyCopyDataModel, IValidatedRule
    {
        public SmartSheetConnectionOptions()
        {
            AccessToken = "";
        }
        
        [XmlAttribute]
        public string AccessToken
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
      
        public override string ToString()
        {
            return AccessToken;
        }

        public override int GetHashCode()
        {
            return AccessToken.GetHashCode();
        }


        public bool IsRuleValid => CheckIfValid.isValid;

        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (string.IsNullOrWhiteSpace(AccessToken))
                    return (false, $"{nameof(AccessToken)} must not be blank");
                
                return (true, "No Error");
            }
        }
    }
}