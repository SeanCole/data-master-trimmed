using System;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.FacilityPointOptions
{
    
    public class PointHistoryPollingRetentionOptions : NotifyCopyDataModel, IValidatedRule
    {
        public PointHistoryPollingRetentionOptions()
        {
            RetentionDays = 7;
            DeleteExpired = false;      
            PollAtLeastEveryXMinutes = 0;
        }

        [XmlAttribute()]
        public Double RetentionDays
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public bool DeleteExpired
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public Double PollAtLeastEveryXMinutes
        {
            get => GetPropertyValue<Double>();
            set => SetPropertyValue(value);
        }

        public override string ToString()
        {
            return $"Retention: [{RetentionDays}], Delete Expired: [{DeleteExpired}], Poll X mins: [{PollAtLeastEveryXMinutes}]";
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is PointHistoryPollingRetentionOptions copts)
            {
                return ToString() == copts.ToString();
            }
            else
            {
                return false;
            }
        }
        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => (true, "No error");
    }
}
