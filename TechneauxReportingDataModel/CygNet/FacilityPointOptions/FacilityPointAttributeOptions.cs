using GenericRuleModel.Helper;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.CygNet.FacilityPointOptions
{
    public class FacilityPointAttributeOptions : NotifyCopyDataModel, IValidatedRule
    {
        public FacilityPointAttributeOptions() 
        {
            AttributeKey = new IdDescKey();

            IdDescOrValue = AttributeMetaType.Value;
            SmartSheetAttributeMetaTypes = EnumHelper.GetAllValuesAndDescriptions(IdDescOrValue.GetType()).ToList();

        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetAttributeMetaTypes
        {
            get => GetPropertyValue<List<ValueDescription>>();
            private set => SetPropertyValue(value);
        }


        public IdDescKey AttributeKey
        {

            get => GetPropertyValue<IdDescKey>();
            set => SetPropertyValue(value);
        }      

        public enum AttributeMetaType
        {
            [Description("Attribute Value")]
            Value,
            [Description("Attribute ID")]
            ID,
            [Description("Attribute Description")]
            Description
        }
        
        [XmlAttribute()]
        public AttributeMetaType IdDescOrValue
        {
            get => GetPropertyValue<AttributeMetaType>();
            set => SetPropertyValue(value);
        }


        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!AttributeKey.IsRuleValid)
                    return (false, $"Attribute Id is invalid because Id is blank.");

                return (true, "No error");
            }
        }
            
    
    }
}
