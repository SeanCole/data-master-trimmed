using System;
using GenericRuleModel.Rules;
using GenericRuleModel.Helper;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.CygNet.FmsSubOptions;
using TechneauxReportingDataModel.CygNet.NoteSubOptions;
using System.Linq;
using TechneauxUtility;
using XmlDataModelUtility;
using System.Collections.Generic;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public class CygNetRuleBase : NotifyCopyDataModel, IValidatedRule
    {
        public CygNetRuleBase()
        {
            DataElement = DataElements.FacilityAttribute;
            FormatConfig = new FormattingOptions();
            AttributeOptions = new FacilityPointAttributeOptions();
            FmsPropertyOptions = new FmsOptions();
            PointHistoryOptions = new PointHistoryGeneralOptions();
            NoteHistoryOptions = new NoteOptions();
            SmartSheetDataElements = EnumHelper.GetAllValuesAndDescriptions(DataElement.GetType()).ToList();
            //SmartSheetDataElements = SmartSheetDataElements.Where(x => (DataElements)x.Value != DataElements.FixedText);

        }
        

        [XmlAttribute]
        public string DefaultIfError
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public List<ValueDescription> SmartSheetDataElements
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }

        public enum DataElements
        {
            [Description("Facility Attribute")]
            FacilityAttribute,

            [Description("FMS Property")]
            FmsProperty,

            //[Description("Facility Note")]
            //FacilityNote,

            [Description("Point Attribute")]
            PointAttribute,

            //[Description("Point Note")]
            //PointNote,

            //[Description("Fixed Text")]
            //FixedText,

            [Description("Point History")]
            PointHistory,

            [Description("Point Current Value")]
            PointCurrentValue

        }

        [XmlAttribute]
        public DataElements DataElement
        {
            get => GetPropertyValue<DataElements>();
            set => SetPropertyValue(value);
        }

        public string FixedText
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        //only for Point       

        // Attribute Properties
        public FacilityPointAttributeOptions AttributeOptions
        {
            get => GetPropertyValue<FacilityPointAttributeOptions>();
            set => SetPropertyValue(value);
        }

        public FmsOptions FmsPropertyOptions
        {
            get => GetPropertyValue<FmsOptions>();
            set => SetPropertyValue(value);
        }



        public NoteOptions NoteHistoryOptions
        {
            get => GetPropertyValue<NoteOptions>();
            set => SetPropertyValue(value);
        }

        public PointHistoryGeneralOptions PointHistoryOptions
        {
            get => GetPropertyValue<PointHistoryGeneralOptions>();
            set => SetPropertyValue(value);
        }

        //Output configuration
        public FormattingOptions FormatConfig
        {
            get => GetPropertyValue<FormattingOptions>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore]
        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        //private (bool isValid, string message) CheckIfValid
        //{
        //    get
        //    {
        //        if (!AttributeKey.IsRuleValid)
        //            return (false, $"{nameof(AttributeKey)} is invalid because {AttributeKey.ValidationErrorMessage}");

        //        return (true, "No error");
        //    }
        //}
      
        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                if (!FormatConfig.IsRuleValid)
                    return (false, $"{FormatConfig.ValidationErrorMessage}");

                switch (DataElement)
                {
                    case DataElements.FacilityAttribute:
                        if (AttributeOptions.IsRuleValid)
                            return (true, "No Error");
                        else
                        {
                            return (false, $"{AttributeOptions.ValidationErrorMessage}"); 
                        }
                        break;
                    case DataElements.PointAttribute:
                        
                        if (AttributeOptions.IsRuleValid)
                            return (true, "No Error");
                        else
                        {
                            return (false, $"{AttributeOptions.ValidationErrorMessage}");
                        }

                        break;
                    case DataElements.FmsProperty:
                        if (FmsPropertyOptions.IsRuleValid)
                            return (true, "No Error");
                        else
                        {
                            return (false, $"{FmsPropertyOptions.ValidationErrorMessage}");
                        }

                        break;
                    
                    //case DataElements.FixedText:
                    //    if(string.IsNullOrWhiteSpace(FixedText))
                    //    {
                    //        return (false, $"{nameof(FixedText)} is invalid because it is either null or blank.");
                    //    }
                    //    else
                    //    {
                    //        return (true, "No error");
                    //    }
                    //    break;
                    case DataElements.PointHistory:
                        if (PointHistoryOptions.IsRuleValid)
                            return (true, "No Error");
                        else
                        {
                            return (false, $"{PointHistoryOptions.ValidationErrorMessage}");
                        }

                        break;
                    case DataElements.PointCurrentValue:
                        if (PointHistoryOptions.IsRuleValid)
                            return (true, "No Error");
                        else
                        {
                            return (false, $"{PointHistoryOptions.ValidationErrorMessage}");
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
               
                return (true, "No error");
            }
        }



    }
}
