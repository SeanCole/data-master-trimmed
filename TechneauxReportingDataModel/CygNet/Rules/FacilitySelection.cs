using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;
using TechneauxReportingDataModel.Helper;
using System.Linq;
using GenericRuleModel.Helper;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public class FacilitySelection : NotifyCopyDataModel, IFacilitySelection, IValidatedRule
    {
        //Default params
        public FacilitySelection()
        {
            FacilityChoice = FacilityChoices.BaseFacility;
            ChildGroupKey = new IdDescKey();
            ChildOrdinal = 1;

            SmartSheetFacilityChoices = EnumHelper.GetAllValuesAndDescriptions(FacilityChoice.GetType()).ToList() ;
        }

        [XmlIgnore]
        public List<ValueDescription> SmartSheetFacilityChoices
        {
            get => GetPropertyValue<List<ValueDescription>>();
            private set =>  SetPropertyValue(value);
        }        

        public enum FacilityChoices
        {
            [Description("Base Facility")]
            BaseFacility,

            [Description("Parent Facility")]
            ParentFacility,

            [Description("Child Facility")]
            ChildFacility
        }

        [XmlAttribute()]
        public FacilityChoices FacilityChoice
        {
            get => GetPropertyValue<FacilityChoices>();
            set => SetPropertyValue(value);
        }

        //Only for Child
        public IdDescKey ChildGroupKey
        {
            get => GetPropertyValue<IdDescKey>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute()]
        public int ChildOrdinal
        {
            get => GetPropertyValue<int>();
            set => SetPropertyValue(value);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is FacilityPointSelection fps)
            {
                return fps.ToString() == ToString();
            }

            return false;
        }

        public override string ToString()
        {
            return $"FacChoice={FacilityChoice},ChildGroupId={ChildGroupKey.Id},ChildOrd={ChildOrdinal.ToString()}";
        }
        
        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid
        {
            get
            {
                switch (FacilityChoice)
                {
                    case FacilityChoices.BaseFacility:
                    case FacilityChoices.ParentFacility:
                        break;

                    case FacilityChoices.ChildFacility:
                        if (!ChildGroupKey.IsRuleValid)
                            return (false, $"{nameof(ChildGroupKey)} is not valid because ChildGroupKey is blank or null.");

                        break;

                    default:
                        throw new ArgumentOutOfRangeException();
                }

                return (true, "No Error");
            }
        }
    }
}
