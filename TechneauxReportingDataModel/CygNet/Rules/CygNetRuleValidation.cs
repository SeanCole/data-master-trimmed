﻿using System;
using System.ComponentModel;
using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using XmlDataModelUtility.Rule_Model;
using static TechneauxReportingDataModel.CygNet.Rules.CygNetRuleBase;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public static class CygNetRuleValidation
    {
        public static RuleValueTypes GetRuleValueType(CygNetRollupRuleBase srcRule)
        {
            switch (srcRule.DataElement)
            {
                case DataElements.FacilityAttribute:
                case DataElements.PointAttribute:
                    return RuleValueTypes.AttributeValue;


                    // Need to have option for getting all notes

                    throw new NotImplementedException();

                case DataElements.PointHistory:
                    switch (srcRule.PointHistoryOptions.HistoryType)
                    {
                        case PointHistoryGeneralOptions.HistoryTypesNew.First_AfterRollupPeriodBegins:
                        case PointHistoryGeneralOptions.HistoryTypesNew.First_BeforeRollupPeriodEnds:
                        case PointHistoryGeneralOptions.HistoryTypesNew.First_BeforeRollupPeriodEndsExtend:
                        case PointHistoryGeneralOptions.HistoryTypesNew.First_AfterRollupPeriodEnds:
                        case PointHistoryGeneralOptions.HistoryTypesNew.Value_Current:
                        case PointHistoryGeneralOptions.HistoryTypesNew.Value_AtThisRollupPeriodStart:
                        case PointHistoryGeneralOptions.HistoryTypesNew.Value_AtLastRollupPeriodStart:
                        case PointHistoryGeneralOptions.HistoryTypesNew.Value_AtNextRollupPeriodStart:
                        case PointHistoryGeneralOptions.HistoryTypesNew.RollupPeriod_CalcWeightedAvg:
                        case PointHistoryGeneralOptions.HistoryTypesNew.RollupPeriod_CalcMean:
                        case PointHistoryGeneralOptions.HistoryTypesNew.RollupPeriod_CalcMin:
                        case PointHistoryGeneralOptions.HistoryTypesNew.RollupPeriod_CalcMax:
                        case PointHistoryGeneralOptions.HistoryTypesNew.RollupPeriod_CalcDelta:
                            return RuleValueTypes.HistoryValue;
                        case PointHistoryGeneralOptions.HistoryTypesNew.AllHistoryEntries:
                            return RuleValueTypes.HistoryList;
                        default:
                            throw new InvalidOperationException("This history type is not recognized");
                    }
                //case DataElements.HistoryEntryFlags:
                //case DataElements.HistoryEntryPointTag:
                //case DataElements.HistoryEntryTimestamp:
                //case DataElements.HistoryEntryValue:
                //    return RuleValueTypes.ReportContextValue;
                default:
                    throw new InvalidOperationException("Data element type is not recognized");
            }
        }

        public static bool IsRuleValid(CygNetRuleBase srcRule)
        {
            return CheckRule(srcRule) == ValidationErrorConditions.None;
        }

        public static ValidationErrorConditions CheckRule(this CygNetRuleBase srcRule)
        {
            switch (srcRule.DataElement)
            {
                case DataElements.FacilityAttribute:
                    if (string.IsNullOrWhiteSpace(srcRule.AttributeOptions.AttributeKey.Id))
                        return ValidationErrorConditions.InvalidFacAttribute;
                    break;
                //case DataElements.FacilityNote:
                //    break;
                case DataElements.PointAttribute:
                    // Check if attribute ID is valid
                    var ValidAttrs = PointAttribute.AllPointAttributes;
                    if (!ValidAttrs.ContainsKey(srcRule.AttributeOptions.AttributeKey.Id))
                    {
                        return ValidationErrorConditions.InvalidPointAttribute;
                    }
                    break;
                //case DataElements.PointNote:
                //    break;
                case DataElements.PointHistory:
                    // Check if UDC is null
                    //if (string.IsNullOrWhiteSpace(srcRule.UDC))
                    //{
                    //    return ResolverErrorConditions.UdcBlank;
                    //}
                    return ValidationErrorConditions.None;
                    break;
                case DataElements.PointCurrentValue:
                    //if (string.IsNullOrWhiteSpace(srcRule.UDC))
                    //{
                    //    return ResolverErrorConditions.UdcBlank;
                    //}
                    return ValidationErrorConditions.None;
                    break;
                //case DataElements.HistoryEntryFlags:
                //case DataElements.HistoryEntryPointTag:
                //case DataElements.HistoryEntryTimestamp:
                //case DataElements.HistoryEntryValue:
                //    return ResolverErrorConditions.None;
                default:
                    throw new InvalidOperationException("Data element type is not recognized.");
            }

            return ValidationErrorConditions.None;
        }

        public enum ValidationErrorConditions
        {
            [Description("Succeeded, no errors")]
            None,

            [Description("Parent facility was not found")]
            ParentFacNotAvailable,

            [Description("Validation Error: Child facility group id blank")]
            ChildGroupIdBlank,

            [Description("Child facility group id is invalid or not defined")]
            ChildInvalidGroupId,

            [Description("Child facility not available because chosen ordinal was not found")]
            ChildFacOrdinalNotFound,

            [Description("Udc is blank or whitespace")]
            UdcBlank,

            [Description("UDC not found")]
            UdcNotFound,

            [Description("Point attribute not valid")]
            InvalidPointAttribute,

            [Description("Error retrieving point attribute value")]
            PointAttrValueRetrievalError,

            [Description("Error retrieving point current value")]
            PointCurrentValueRetrievalError,

            [Description("Facility attribute invalid or disabled")]
            InvalidFacAttribute,

            [Description("Error retrieving point attribute value")]
            FacAttrValueRetrievalError
        }
    }
}
