﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechneauxUtility;

namespace TechneauxReportingDataModel.CygNet.Rules
{
    public interface IFacilitySelection
    {
        FacilitySelection.FacilityChoices FacilityChoice { get; set; }

        IdDescKey ChildGroupKey { get; set; }
        int ChildOrdinal { get; set; }
    }
}
