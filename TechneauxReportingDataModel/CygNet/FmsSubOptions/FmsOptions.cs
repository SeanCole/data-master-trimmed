﻿using GenericRuleModel.Helper;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxUtility;
using XmlDataModelUtility;
using System.Linq;

namespace TechneauxReportingDataModel.CygNet.FmsSubOptions
{
    public class FmsOptions : NotifyCopyDataModel, IValidatedRule
    {
        public FmsOptions()
        {
            FmsDataElm = FmsDataElms.NodeName;
            SmartSheetFmsDataOptions = EnumHelper.GetAllValuesAndDescriptions(FmsDataElm.GetType()).ToList();
        }
        [XmlIgnore]
        public List<ValueDescription> SmartSheetFmsDataOptions
        {
            get => GetPropertyValue<List<ValueDescription>>();
            set => SetPropertyValue(value);
        }

        public enum FmsDataElms
        {
            //[Description("FMS Node Id")]
            //NodeId,

            [Description("FMS Node Name")]
            NodeName,

            [Description("Is FMS Meter (bool)")]
            IsFmsMeter
        }

        [XmlAttribute]
        public FmsDataElms FmsDataElm
        {
            get => GetPropertyValue<FmsDataElms>();
            set => SetPropertyValue(value);
        }


        public bool IsRuleValid => CheckIfValid.isValid;
        public string ValidationErrorMessage => CheckIfValid.message;

        private (bool isValid, string message) CheckIfValid => (true, "No error");
    }
}
