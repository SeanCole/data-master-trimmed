﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using CygNet.Data.Core;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxReportingDataModel.CygNet
{
    public class StatusBitMetadata
    {
        public StatusBitMetadata()
        { }
    
        public StatusBitMetadata(dynamic cygNetStatus)
        {
            var bitMetadataList = new List<BitMetadata>();
            foreach(var status in cygNetStatus)
            {
                bitMetadataList.Add(new BitMetadata(status));
            }

            StatusTable = bitMetadataList.ToDictionary(x => (x.PointScheme, x.PointType, x.BitId), y => y);
        }

        public async Task<List<BitMetadata>> GetBitIdList(PointScheme pointScheme, PointTypes pointType)
        {
            if(pointScheme == null)
            {
                return new List<BitMetadata>();
            }
            var scheme = pointScheme.Id;
            var type = Convert.ToInt32(pointType);
            var bitIdKeyList = StatusTable.Keys.ToList();
            BitMetadata bitIdVal;
            List<BitMetadata> bitIdList = new List<BitMetadata>();
            foreach (var keyId in bitIdKeyList)
            {
                if(keyId.pointType == type && keyId.scheme == scheme)
                {
                    
                    if(StatusTable.TryGetValue( keyId,out bitIdVal))
                    {
                        bitIdList.Add(bitIdVal);
                    }                    
                }
            }
            return bitIdList;
        }

        private Dictionary<(int scheme, int pointType, int bitId), BitMetadata> StatusTable = new Dictionary<(int, int, int), BitMetadata>();
        public class BitMetadata
        {
            public BitMetadata()
            {

            }
            public BitMetadata(dynamic status)
            {
                if (status != null)
                {
                    PointScheme = status[0];
                    PointType = status[1];
                    BitId = status[2];
                    CalcOrder = status[3];
                    CalcType = status[4];
                    IsActive = status[5];
                    PointStateId = status[6];
                    StatusBitDescription = status[7];
                    DisplayOrder = status[8];
                    Param1Enabled = status[9];
                    Param1Description = status[10];
                    Param2Enabled = status[11];
                    Param2Description = status[12];
                }
            }

            public int PointScheme { get; }
            public int PointType { get; }
            public int BitId { get; }
            public int CalcOrder { get; }
            public int CalcType { get; }
            public bool IsActive { get; }
            public int PointStateId { get; }
            public string StatusBitDescription { get; }
            public int DisplayOrder { get; }
            public bool Param1Enabled {get; }
            public string Param1Description { get; }
            public bool Param2Enabled { get; }
            public string Param2Description { get; }
        }
        //    public 
    }
}
