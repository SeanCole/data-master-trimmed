using System;
using System.ComponentModel;
using System.Xml.Serialization;
using TechneauxReportingDataModel.CygNet;
using TechneauxReportingDataModel.SmartSheet;

using TechneauxReportingDataModel.SqlHistory;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.General
{
    [Serializable]
    [XmlRoot("ConfigItems"), XmlType("ConfigItems")]
    public class ReportConfigModel : NotifyCopyDataModel
    {
        const string ExcludedDomain = "wpx";
    // Class used to store all the configuration data
        public ReportConfigModel()
        {
            //if(Environment.UserDomainName.Trim().ToLower().Contains(ExcludedDomain))
            //{
            //    AvailableOptions.MainType = AvailableOptions.SyncOptions.SqlHistory;
            //}
            //else
            //{
            //    AvailableOptions.MainType = AvailableOptions.SyncOptions.SqlAndSmartSheet;
            //}
            
            CygNetGeneral = new CygNetGeneralOptions();
            SqlConfigModel = new SqlHistorySyncConfigModel();
            SmartSheetConfigModel = new SmartSheetSyncConfigModel();
        }

        public CygNetGeneralOptions CygNetGeneral
        {
            get => GetPropertyValue<CygNetGeneralOptions>();
            set => SetPropertyValue(value);
        }

        public SqlHistorySyncConfigModel SqlConfigModel
        {
            get => GetPropertyValue<SqlHistorySyncConfigModel>();
            set => SetPropertyValue(value);
        }

        public SmartSheetSyncConfigModel SmartSheetConfigModel
        {
            get => GetPropertyValue<SmartSheetSyncConfigModel>();
            set => SetPropertyValue(value);
        }
    }
}