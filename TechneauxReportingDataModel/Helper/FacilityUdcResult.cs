﻿using System.Collections.Generic;
using Techneaux.CygNetWrapper.Facilities;

namespace TechneauxReportingDataModel.Helper
{
    public class FacilityUdcResult
    {
        public FacilityUdcResult(ICachedCygNetFacility srcFac, IEnumerable<string> udcList) 
        {
            Facility = srcFac;
            Udcs.UnionWith(udcList);
        }

        public FacilityUdcResult(ICachedCygNetFacility srcFac)
        {
           Facility = srcFac;
        }

        public void AddUdc(string udc)
        {
            Udcs.Add(udc);
        }
        
        public ICachedCygNetFacility Facility {get; private set;}

        private HashSet<string> Udcs { get; set; } = new HashSet<string>();

        public string FacilityTag => Facility.TagString;

        public bool HasUdc(string udc)
        {
            return Udcs.Contains(udc);
        }

    }
}
