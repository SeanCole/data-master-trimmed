﻿using Techneaux.CygNetWrapper.Points;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TechneauxReportingDataModel.Helper
{
    public class CachedCygNetPointWithRule : CachedCygNetPoint
    {
        public SqlPointSelection SrcRule { get; }

        public CachedCygNetPointWithRule(
            ICachedCygNetPoint srcPnt,
            SqlPointSelection srcRule) 
            : base(srcPnt.PntService,
                  srcPnt.CvsService,
                  srcPnt.Facility,
                  srcPnt.Tag
                )
        {
            SrcRule = srcRule;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
