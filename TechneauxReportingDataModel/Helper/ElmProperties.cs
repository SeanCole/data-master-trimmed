using TechneauxReportingDataModel.ExcelTextFile.Enumerable;

namespace TechneauxReportingDataModel.Helper
{
    public class ElementProperties
    {
        public ReportElement Parent { get; }

        public int Index { get; }

        public int GroupedIndex { get; }
        public int UngroupedIndex { get; }

        public int GroupedCount { get; }
        public int UngroupedCount { get; }

        public string UniqueIdTag { get; }

        public int TableHistoryValueCount { get; }

        public string Key
        {
            get
            {
                if (string.IsNullOrEmpty(Parent.ColumnHeader))
                {
                    return $"Unnamed Col {Index}";
                }
                else
                {
                    return $"{Parent.ColumnHeader}.{Index}";
                }
            }
        }

        public ElementProperties(
            ReportElement parent,
            int index,
            int groupedIndex,
            int ungroupedIndex,
            int groupedCount,
            int ungroupedCount,
            int tableHistoryValueCount) : base()
        {
            Parent = parent;

            Index = index;

            GroupedIndex = groupedIndex;
            UngroupedIndex = ungroupedIndex;

            GroupedCount = groupedCount;
            UngroupedCount = ungroupedCount;

            UniqueIdTag = $"{Index}: {Parent.ColumnHeader}";

            TableHistoryValueCount = tableHistoryValueCount;
        }

        public override bool Equals(object obj)
        {
            var CompareElm = obj as ElementProperties;

            return Key == CompareElm.Key;
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

    }
}