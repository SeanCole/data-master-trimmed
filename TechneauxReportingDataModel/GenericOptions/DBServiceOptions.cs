using System.Xml.Serialization;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.GenericOptions
{
    public class DBServiceOptions : NotifyCopyDataModel
    {
        public DBServiceOptions()
        {
            // Database options
            DBExport = false;
            DBFacID = "";
            DBService = "";
            DBTable = "";
            DBTimestamp = "";
        }

        // Database options
        [XmlAttribute]
        public bool DBExport
        {
            get => GetPropertyValue<bool>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DBFacID
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DBService
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DBTable
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        [XmlAttribute]
        public string DBTimestamp
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

    }
}
