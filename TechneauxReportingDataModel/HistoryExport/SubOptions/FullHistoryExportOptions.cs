using TechneauxReportingDataModel.ExcelTextFile.SubOptions;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.HistoryExport.SubOptions
{
    public class FullHistoryExportOptions : NotifyCopyDataModel
    {
        public FullHistoryExportOptions()
        {
            ExcelTemplateOpts = new ExcelTemplateOptions();
        }

        public ExcelTemplateOptions ExcelTemplateOpts
        {
            get => GetPropertyValue<ExcelTemplateOptions>();
            set => SetPropertyValue(value);
        }
    }
}
