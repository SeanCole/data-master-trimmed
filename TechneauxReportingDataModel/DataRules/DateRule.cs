using System.ComponentModel;
using XmlDataModelUtility;

namespace TechneauxReportingDataModel.DataRules
{
    public class DateRule : NotifyCopyDataModel
    {
        public DateRule()
        {
            DateType = DateTypes.RollupStart;
            DateFormatString = "";
        }

        public enum DateTypes
        {
            [Description("Rollup Period Start Date")]
            RollupStart,

            [Description("Rollup Period End Date")]
            RollupEnd,

            [Description("Today's Date")]
            Today
        }

        [HelperClasses.ControlTitle("Date Type", "Type")]
        public DateTypes DateType
        {
            get => GetPropertyValue<DateTypes>();
            set => SetPropertyValue(value);
        }

        [HelperClasses.ControlTitle("Date Format String", "Format String")]
        public string DateFormatString
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
    }
}
