﻿using System;
using CygNet.Data.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TryTesting
{
    [TestClass]
    public class CygNetTests
    {
        [TestMethod]
        public void EqualityTest()
        {
            var a = new ServiceDefinition();
            a.SiteService = new DomainSiteService(5410, "site", "service");

            var b = new ServiceDefinition();
            b.SiteService = new DomainSiteService(5410, "site", "service");

            Assert.IsTrue(a == b);
            Assert.AreEqual(a, b);
        }
    }
}
