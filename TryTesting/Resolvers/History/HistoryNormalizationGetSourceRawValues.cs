﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CygNetRuleModel.Resolvers.History;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Techneaux.CygNetWrapper.Services.VHS;
using System.Threading.Tasks;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using static CygNetRuleModel.Resolvers.History.HistoryNormalization;

namespace CygNetRuleModel.Resolvers.History.Tests
{
    [TestClass()]
    public class HistoryNormalizationGetSourceRawValues
    {
        [TestMethod()]
        public void HistoryNormalizationTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void NormalizeTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void NormalizeTest1()
        {
            Assert.Fail();
        }




        //[TestMethod()]
        //public void GetSourceRawValuesInRangeForNormTimestampsTest()
        //{
        //    var earliestDate = new DateTime(1987, 9, 7, 3, 3, 3, DateTimeKind.Local);
        //    var latestDate = new DateTime(1987, 9, 7, 5, 3, 3, DateTimeKind.Local);
        //    HistoryNormalization normalizer = new HistoryNormalization(new PointHistoryNormalizationOptions
        //    {
        //        EnableNormalization = true,
        //        NumWindowHistEntries = 12,
        //        NormalizeWindowStartTime = TimeSpan.FromSeconds(0),
        //        NormalizeWindowIntervalLength = 1,
        //        SelectedSampleType = PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_After
        //    } , earliestDate ,latestDate);

        //    var normTimestamps = normalizer.GetNormalizedTimestampsInWindow();
        //    var NormInRange =  normTimestamps.Select(ts => new NormDateEntriesInRange(ts)).ToList();

        //    List<CygNetHistoryEntry> srcData = new List<CygNetHistoryEntry>();


        //    //test #1
        //    var FirstTimeStamp = earliestDate.AddMinutes(-10).AddSeconds(-1);
        //    var LastTimeStamp = latestDate.AddMinutes(10).AddSeconds(-1);
        //    var CurrentTimeStamp = FirstTimeStamp;
        //    while (CurrentTimeStamp <= LastTimeStamp)
        //    {
        //        var newHist = new CygNetHistoryEntry
        //        {
        //            RawTimestamp = CurrentTimeStamp,
        //            ServerUtcOffset = DateTimeOffset.Now.Offset,
        //            RawValue = 0
        //        };
        //        srcData.Add(newHist);
        //        CurrentTimeStamp = CurrentTimeStamp.AddMinutes(1);
        //    }
        //    HistoryNormalization.GetSourceRawValuesInRangeForNormTimestamps(srcData, NormInRange);
        //    bool CountIsEqual;
        //    if((NormInRange.Where(x => x.SrcItems.Count > 0).Count()) > 0)
        //    {
        //        CountIsEqual = true;
        //    }
        //    else
        //    {
        //        CountIsEqual = false;
        //    }
        //    Assert.IsTrue(CountIsEqual);
        //    bool TimestampsAreValid;
        //    if (NormInRange.Where(x => (x.SrcItems.Count > 0) && (x.SrcItems.Where(z => (z.AdjustedTimestamp <= x.NormTimestamp.LatestTimestampAllowed) && (z.AdjustedTimestamp >= x.NormTimestamp.EarliestTimestampAllowed)).Count() > 0)).Count() > 0 )
        //    {
        //        TimestampsAreValid = true;
        //    }   
        //    else
        //    {
        //        TimestampsAreValid = false;
        //    }

        //    Assert.IsTrue(TimestampsAreValid);



        //    //test #2
        //    List<CygNetHistoryEntry> srcDataFail = new List<CygNetHistoryEntry>();
        //    var FirstTimeStampFail = earliestDate.AddMinutes(-10);
        //    var LastTimeStampFail = latestDate.AddMinutes(10);
        //    var CurrentTimeStampFail = FirstTimeStampFail;
        //    while (CurrentTimeStampFail <= LastTimeStampFail)
        //    {
        //        var newHist = new CygNetHistoryEntry
        //        {
        //            RawTimestamp = CurrentTimeStampFail,
        //            ServerUtcOffset = DateTimeOffset.Now.Offset,
        //            RawValue = 0
        //        };
        //        srcDataFail.Add(newHist);
        //        CurrentTimeStampFail = CurrentTimeStampFail.AddMinutes(1);
        //    }
        //    var normTimestampsFail = normalizer.GetNormalizedTimestampsInWindow();
        //    var NormInRangeFail =  normTimestampsFail.Select(ts => new NormDateEntriesInRange(ts)).ToList();
        //    HistoryNormalization.GetSourceRawValuesInRangeForNormTimestamps(srcDataFail, NormInRangeFail);
        //    bool CountIsEqualFail;
        //    if ((NormInRangeFail.Where(x => x.SrcItems.Count > 0).Count()) > 0)
        //    {
        //        CountIsEqualFail = true;
        //    }
        //    else
        //    {
        //        CountIsEqualFail = false;
        //    }
        //    Assert.IsTrue(CountIsEqualFail);
        //    bool TimestampsAreValidFail;
        //    if (NormInRangeFail.Where(x => (x.SrcItems.Count > 0) && (x.SrcItems.Where(z => (z.AdjustedTimestamp <= x.NormTimestamp.LatestTimestampAllowed) && (z.AdjustedTimestamp >= x.NormTimestamp.EarliestTimestampAllowed)).Count() > 0)).Count() > 0)
        //    {
        //        TimestampsAreValidFail = true;
        //    }
        //    else
        //    {
        //        TimestampsAreValidFail = false;
        //    }
        //    Assert.IsTrue(TimestampsAreValidFail);

        //    //test # 3 
        //    HistoryNormalization normalizerExt = new HistoryNormalization(new PointHistoryNormalizationOptions
        //    {
        //        EnableNormalization = true,
        //        NumWindowHistEntries = 12,
        //        NormalizeWindowStartTime = TimeSpan.FromSeconds(0),
        //        NormalizeWindowIntervalLength = 1,
        //        SelectedSampleType = PointHistoryNormalizationOptions.SamplingType.Take_Nearest_Before_Extended
        //    }, earliestDate, latestDate);
        //    List<CygNetHistoryEntry> srcDataExt = new List<CygNetHistoryEntry>();
        //    var FirstTimeStampExt = earliestDate.AddMinutes(-10).AddSeconds(-1);
        //    var LastTimeStampExt = latestDate.AddMinutes(10).AddSeconds(-1);
        //    var CurrentTimeStampExt = FirstTimeStampExt;
        //    var normTimestampsExt = normalizer.GetNormalizedTimestampsInWindow();
        //    var NormInRangeExt = normTimestampsExt.Select(ts => new NormDateEntriesInRange(ts)).ToList();
        //    while (CurrentTimeStampExt <= LastTimeStampExt)
        //    {
        //        var newHistExt = new CygNetHistoryEntry
        //        {
        //            RawTimestamp = CurrentTimeStampExt,
        //            ServerUtcOffset = DateTimeOffset.Now.Offset,
        //            RawValue = 0
        //        };
        //        srcDataExt.Add(newHistExt);
        //        CurrentTimeStampExt = CurrentTimeStampExt.AddMinutes(1);
        //    }
        //    HistoryNormalization.GetSourceRawValuesInRangeForNormTimestamps(srcDataExt, NormInRangeExt);
        //    bool CountIsEqualExt;
        //    //if (((NormInRangeExt.Where(x => x.SrcItemsExtended.Count > 0).Count()) > 0) && (NormInRangeExt.Where(x => x.SrcItemsExtended.Count == srcDataExt.Count()).Count() == NormInRangeExt.Count()))
        //    //{
        //    //    CountIsEqualExt = true;
        //    //}
        //    //else
        //    //{
        //    //    CountIsEqualExt = false;
        //    //}
        //   // Assert.IsTrue(CountIsEqualExt);
        //    //bool TimestampsAreValid;
        //    //if (NormInRange.Where(x => (x.SrcItems.Count > 0) && (x.SrcItems.Where(z => (z.AdjustedTimestamp <= x.NormTimestamp.LatestTimestampAllowed) && (z.AdjustedTimestamp >= x.NormTimestamp.EarliestTimestampAllowed)).Count() > 0)).Count() > 0)
        //    //{
        //    //    TimestampsAreValid = true;
        //    //}
        //    //else
        //    //{
        //    //    TimestampsAreValid = false;
        //    //}

        //    //Assert.IsTrue(TimestampsAreValid);
        //}

        [TestMethod()]
        public void GetNextFutureNormalizedTimestampTest()
        {
            Assert.Fail();
        }
    }
}