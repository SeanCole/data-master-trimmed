﻿using System;
using System.Collections.Generic;

namespace TechneauxHistorySynchronization.Helper
{
    public static class EnumExtensions
    {
        public static IEnumerable<List<T>> SplitList<T>(this List<T> srcList, int nSize = 30)
        {
            for (var i = 0; i < srcList.Count; i += nSize)
            {
                yield return srcList.GetRange(i, Math.Min(nSize, srcList.Count - i));
            }
        }
    }
}
