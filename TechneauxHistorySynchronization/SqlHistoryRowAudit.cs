﻿using System;

namespace TechneauxHistorySynchronization
{
    public class SqlHistoryRowAudit : SqlHistoryRow
    {
        public bool IsVolatile { get; set; }

        public DateTime CritCygEarliestDate { get; set; }

        public DateTime CritCygLatestDate { get; set; }
    }
}
