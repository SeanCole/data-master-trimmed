﻿using Serilog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Points.PointCaching;
using TechneauxHistorySynchronization.Helper;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxReportingDataModel.SqlHistory.SubOptions;
using XmlDataModelUtility;
using static TechneauxReportingDataModel.SqlHistory.SubOptions.SqlGeneralOptions;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations
{
    public class Database
    {
        public string ConnectionString { get; set; }

        public SqlGeneralOptions SqlOptions { get; set; }

        public List<Table> Tables { get; set; } = new List<Table>();

        public string Name { get; set; }

        public string SqlConnectionString { get; set; }

        public bool IsAccessible { get; set; }

        public async Task<List<Table>> RefreshAllTablesList(CancellationToken ct)
        {
            try
            {
                Tables.Clear();

                if (!await IsDatabaseAccessible(SqlOptions, ct))
                {
                    IsAccessible = false;
                    return null;
                }
                IsAccessible = true;

                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(SqlOptions, true);

                using (var sqlCon = new SqlConnection(connStr))
                {
                    await sqlCon.OpenAsync(ct);

                    //string[] restrictions = new string[4];
                    //restrictions[2] = SqlOptions.TableName;
                    DataTable table = await Task.Run(() => sqlCon.GetSchema("Tables"), ct); //, restrictions);

                    var allTableNames = table.Rows.Cast<DataRow>().Select(row => row[2].ToString());

                    foreach (var tblName in allTableNames)
                    {
                        var tableOptsCopy = SqlOptions.Copy();
                        tableOptsCopy.TableName = tblName;

                        Tables.Add(new Table
                        {
                            Name = tblName,
                            SqlOptions = tableOptsCopy
                        });
                    }
                }

                IsAccessible = true;

                return Tables;
            }
            catch (Exception ex)
            {
                IsAccessible = false;

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Warning(ex, "Failed to get SQL Table Schema");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Warning(ex, "Failed to get SQL Table Schema, UI and sync cannot proceed until Schema can be read. Another retry will occur shortly.");

                return null;
            }
        }

        public static async Task<bool> IsDatabaseAccessible(SqlGeneralOptions opts, CancellationToken ct)
        {
            try
            {
                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(opts, false);

                using (var sqlCon = new SqlConnection(connStr))
                {
                    await sqlCon.OpenAsync(ct);

                    var userCheckSql = $@"DECLARE @DBuser_sql VARCHAR(4000) 
                    DECLARE @DBuser_table TABLE (DBName VARCHAR(200), UserName VARCHAR(250), LoginType VARCHAR(500), AssociatedRole VARCHAR(200)) 
                    SET @DBuser_sql='SELECT ''?'' AS DBName,a.name AS Name,a.type_desc AS LoginType,USER_NAME(b.role_principal_id) AS AssociatedRole FROM ?.sys.database_principals a 
                        LEFT OUTER JOIN ?.sys.database_role_members b ON a.principal_id=b.member_principal_id 
                    WHERE a.sid NOT IN (0x01,0x00) AND a.sid IS NOT NULL AND a.type NOT IN (''C'') AND a.is_fixed_role <> 1 AND a.name NOT LIKE ''##%'' AND ''?'' NOT IN (''master'',''msdb'',''model'',''tempdb'') ORDER BY Name'
                    INSERT @DBuser_table 
                    EXEC sp_MSforeachdb @command1=@dbuser_sql 
                    SELECT * FROM @DBuser_table WHERE DBName = '{opts.DatabaseName}' AND AssociatedRole IN ('db_datawriter', 'db_owner') ORDER BY DBName ";

                    using (var userCheckSqlCmd = new SqlCommand(userCheckSql, sqlCon))
                    {
                        var results = await userCheckSqlCmd.ExecuteScalarAsync(ct);

                        if (results == null)
                            return false;
                        else
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex);
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled Exception while attempting to check database security");

                return false;
            }

        }


        //public async Task<bool> CheckIfAccessible()
        //{

        //}
    }

    public class Table
    {
        public string Name { get; set; }

        public List<SimpleSqlColumn> Columns { get; set; }

        public SqlGeneralOptions SqlOptions { get; set; }

        public SimpleSqlTableSchema TableSchema { get; set; }

        public async Task<SimpleSqlTableSchema> GetRefreshedSchema(CancellationToken ct)
        {
            try
            {
                if (!SqlOptions.HasValidTableOpts)
                {
                    Log.Debug("SQL connection options {@opts} are not valid", SqlOptions);
                    return SimpleSqlTableSchema.Empty();
                }

                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(SqlOptions, true);

                using (var sqlCon = new SqlConnection(connStr))
                {
                    await sqlCon.OpenAsync(ct);

                    var pkeyCommand = $@"SELECT ColumnName = col.column_name 
                                            FROM information_schema.table_constraints tc 
                                             INNER JOIN information_schema.key_column_usage col 
                                                ON col.Constraint_Name = tc.Constraint_Name 
                                            AND col.Constraint_schema = tc.Constraint_schema 
                                            WHERE tc.Constraint_Type = 'Primary Key' AND col.Table_name = '{SqlOptions.TableName}';";

                    var pkeyColNames = new HashSet<string>();
                    using (var sqlPkeyCmd = new SqlCommand(pkeyCommand, sqlCon)
                    {
                        CommandTimeout = 300
                    })
                    using (var pkeyReader = await sqlPkeyCmd.ExecuteReaderAsync(ct))
                    {
                        pkeyColNames = new HashSet<string>(pkeyReader.Cast<IDataRecord>().Select(row => row["ColumnName"].ToString()));
                    }

                    var colInfoCommand = $@"SELECT ORDINAL_POSITION, COLUMN_NAME, DATA_TYPE, IS_NULLABLE, 
                                            IIF(CHARACTER_MAXIMUM_LENGTH IS NULL,0,CHARACTER_MAXIMUM_LENGTH) CHARACTER_MAXIMUM_LENGTH
                                                FROM INFORMATION_SCHEMA.COLUMNS 
                                            WHERE TABLE_NAME = '{SqlOptions.TableName}'";

                    using (var sqlColInfoCmd = new SqlCommand(colInfoCommand, sqlCon)
                    {
                        CommandTimeout = 300
                    })
                    using (var colInfoReader = await sqlColInfoCmd.ExecuteReaderAsync(ct))
                    {
                        Columns = colInfoReader
                            .Cast<IDataRecord>()
                            .Select(row => new SimpleSqlColumn
                            {
                                ID = (int)row["ORDINAL_POSITION"],
                                Name = (string)row["COLUMN_NAME"],
                                DataType = (SqlDbType)Enum.Parse(typeof(SqlDbType), (string)row["DATA_TYPE"], true),
                                FieldLength = (int)row["CHARACTER_MAXIMUM_LENGTH"],
                                IsNullable = string.Equals(
                                    (string)row["IS_NULLABLE"], "yes", StringComparison.InvariantCultureIgnoreCase),
                                InPrimaryKey = pkeyColNames.Contains((string)row["COLUMN_NAME"])
                            })
                            .ToList();
                    }

                    //string[] restrictions = new string[4];
                    //restrictions[3] = SqlOptions.TableName;
                    //DataTable table = sqlCon.GetSchema("Columns", new[] { SqlOptions.DatabaseName, null, SqlOptions.TableName });
                    //DataTable table = sqlCon.GetSchema("Columns", new[] { "DBName", null, "TableName" });

                    //foreach (DataRow row in table.Rows)
                    //{
                    //    Columns.Add(new Column
                    //    {
                    //        ID = row.Field<short>(4),
                    //        Name = row.Field<string>(2),
                    //        SqlType = (SqlDbType)Enum.Parse(typeof(SqlDbType), row.Field<string>(5), true),
                    //        Nullable = string.Equals(row.Field<string>(6), "yes", StringComparison.InvariantCultureIgnoreCase),
                    //        InPrimaryKey = pkeyColNames.Contains(Name)
                    //    });
                    //    //yield return new DbColumnInfo()
                    //    //{
                    //    //    Name = row.Field<string>(3),
                    //    //    OrdinalPosition = row.Field<short>(4),
                    //    //    DataType = this.FormatDataType(row),
                    //    //    IsNullable = string.Equals(row.Field<string>(6), "yes", StringComparison.InvariantCultureIgnoreCase),
                    //    //    IsPrimaryKey = // ... ?
                    //    //};
                    //}
                }

                // Build schema here and assign to property
                return new SimpleSqlTableSchema(SqlOptions, Columns);
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Warning(ex, "Failed to get SQL Table Schema for connection options {@sqlOpts}", SqlOptions);

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Warning(ex, "Failed to get SQL Table Schema for connection options {@sqlOpts}. Sync will not proceed until a retry succeeds", SqlOptions);

                return SimpleSqlTableSchema.Empty();
            }
        }
    }

    //public class Column
    //{
    //    public int ID { get; set; }

    //    public string Name { get; set; }

    //    public bool InPrimaryKey { get; set; }
    //    public bool Nullable { get; set; }

    //    //public Type DataType { get; set; }

    //    public SqlDbType SqlType { get; set; }

    //    public int MaximumLength { get; set; }
    //}


    public class Server
    {
        public string ConnStr;

        public SqlGeneralOptions SqlOptions;

        public List<Database> Databases { get; private set; }

        //public async Task<bool> TryConnect()
        //{
        //    try
        //    {
        //        var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(SqlOptions);

        //        const string sqlGetDbs = "SELECT name FROM master.sys.databases";
        //        using (var sqlCon = new SqlConnection(connStr))
        //        {
        //            await sqlCon.OpenAsync();
        //        }

        //        CouldConnect = true;
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Warning(ex, "Failed to connect to server");
        //        CouldConnect = false;
        //        return false;
        //    }
        //}

        //public bool CouldConnect { get; private set; }

        public async Task<List<Database>> RefreshDatabases(CancellationToken ct)
        {
            try
            {
                Databases = new List<Database>();

                var connStr = SqlServerConnectionUtility.SqlStringConnectionBuilder(SqlOptions);

                const string sqlGetDbs = "SELECT name FROM master.sys.databases WHERE name NOT IN ('master','tempdb','model','msdb')";
                using (var sqlCon = new SqlConnection(connStr))
                {
                    await sqlCon.OpenAsync(ct);

                    using (var cmd = new SqlCommand(sqlGetDbs, sqlCon))
                    {
                        using (var reader = await cmd.ExecuteReaderAsync(ct))
                        {
                            var allDbs = reader.Cast<IDataRecord>().Select(row => row["name"].ToString());

                            foreach (var db in allDbs)
                            {
                                var optionsCopy = SqlOptions.Copy();
                                optionsCopy.DatabaseName = db;

                                Databases.Add(new Database
                                {
                                    Name = reader.GetString(0),
                                    SqlOptions = optionsCopy
                                });
                            }
                        }
                    }
                }

                return Databases;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Warning(ex, "Unhandled exception with database refresh for server {server}. Failed to get latest db list.", SqlOptions.ServerName);

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Warning(ex, "Unhandled exception with database refresh for server {server}. Failed to get latest db list.", SqlOptions.ServerName);


                return null;
            }
        }
    }

    public class SqlServerConnectionUtility
    {
        public static async Task<SimpleCombinedTableSchema> GetTableSchema(SqlGeneralOptions opts, List<SqlTableMapping> sqlElmsList, CancellationToken ct)
        {
            var thisSchema = await CachedSchemas.GetCachedItem(opts, ct);

            return thisSchema?.WithCygNetColumns(sqlElmsList);
        }
        public static async Task<SimpleSqlTableSchema> GetTableSchema(SqlGeneralOptions opts)
        {
            var thisSchema = await CachedSchemas.GetCachedItem(opts, CancellationToken.None);

            return thisSchema;
        }
        public static async Task<SimpleSqlTableSchema> GetTableSchema(SqlGeneralOptions opts, CancellationToken ct)
        {
            var thisSchema = await CachedSchemas.GetCachedItem(opts, ct);

            return thisSchema;
        }

        private static async Task<SimpleSqlTableSchema> GetTableSchemaForCache(SqlGeneralOptions opts, CancellationToken ct)
        {
            var newTable = new Table()
            {
                SqlOptions = opts
            };

            var sqlSchema = await newTable.GetRefreshedSchema(ct);
            return sqlSchema;
        }

        private static readonly ExpiringCache<SqlGeneralOptions, Server> CachedConnections;
        private static readonly ExpiringCache<SqlGeneralOptions, SimpleSqlTableSchema> CachedSchemas;

        static SqlServerConnectionUtility()
        {
            CachedConnections = new ExpiringCache<SqlGeneralOptions, Server>(GetConnectedServerNew, TimeSpan.FromMinutes(10));
            CachedSchemas = new ExpiringCache<SqlGeneralOptions, SimpleSqlTableSchema>(GetTableSchemaForCache, TimeSpan.FromMinutes(10));
        }




        private static readonly SemaphoreSlim LockServerRequests = new SemaphoreSlim(1, 1);

        public static async Task<Server> GetConnectedServer(SqlGeneralOptions srcOpts, CancellationToken ct)
        {
            var clonedOpts = srcOpts.Copy();

            Log.Debug("Caching object {@SqlOpts}", clonedOpts);

            return await CachedConnections.GetCachedItem(clonedOpts, ct);
        }


        //private List<> GetDatabaseList



        private static async Task<Server> GetConnectedServerNew(SqlGeneralOptions srcOpts, CancellationToken ct)
        {
            try
            {
                var connStr = SqlStringConnectionBuilder(srcOpts);

                var key = srcOpts.GetSqlServerConnectionParamString();

                //if (CachedConnections.TryGetValue(key, out Server serv))

                if (!srcOpts.HasValidServerOpts)
                    return null;

                var server = await GetConnectedSqlServer(srcOpts, ct);

                if (server == null)
                    return null;

                ct.ThrowIfCancellationRequested();

                foreach (var db in server.Databases)
                {
                    ct.ThrowIfCancellationRequested();

                    var tables = await Task.Run(() => db.RefreshAllTablesList(ct), ct);

                    ct.ThrowIfCancellationRequested();


                    //db.Tables.Refresh();

                    //foreach (Table table in db.Tables)
                    //{
                    //    table.Columns.Refresh();
                    //}
                }

                return server;
            }
            catch (OperationCanceledException) { return null; }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting connected SQL @server", srcOpts.ServerName);
            }

            return null;
        }

        public async static Task<Server> GetConnectedSqlServer(SqlGeneralOptions opts, CancellationToken ct)
        {
            string connStr;
            try
            {
                //await SqlConnectionLocker.WaitAsync();

                connStr = SqlStringConnectionBuilder(opts);

                using (var sqlCon = new SqlConnection(connStr))
                {
                    await sqlCon.OpenAsync(ct);

                    ct.ThrowIfCancellationRequested();

                    var server = new Server
                    {
                        ConnStr = connStr,
                        SqlOptions = opts
                    };

                    await server.RefreshDatabases(ct);

                    return server;
                }
            }
            catch (OperationCanceledException) { }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting sql server connection");
            }
            finally
            {
                //SqlConnectionLocker?.Release();
            }
            return null;
        }

        public static SemaphoreSlim SqlConnectionLocker { get; private set; } = new SemaphoreSlim(1, 1);
        public static string SqlStringConnectionBuilder(
            SqlGeneralOptions opts,
            bool includeDb = false)
        {
            SqlConnectionStringBuilder connStrBuild;
            try
            {

                if (opts.AuthenticationType == AuthenticationTypes.Integrated)
                {
                    connStrBuild = new SqlConnectionStringBuilder
                    {
                        //InitialCatalog = "dbase",
                        UserID = $@"{Environment.UserDomainName}\{Environment.UserName}",
                        Password = "",
                        DataSource = opts.ServerName,
                        IntegratedSecurity = true,
                        //MaxPoolSize = 10

                    };
                }
                else
                {
                    connStrBuild = new SqlConnectionStringBuilder
                    {
                        //InitialCatalog = "dbase",
                        UserID = opts.Username,
                        Password = opts.Password,
                        DataSource = opts.ServerName,
                        IntegratedSecurity = false,
                        //MaxPoolSize = 10
                    };

                }



                connStrBuild.ConnectTimeout = 60;

                if (includeDb)
                {
                    connStrBuild.InitialCatalog = opts.DatabaseName;
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception getting SQL connection string");

                connStrBuild = new SqlConnectionStringBuilder();
            }

            return connStrBuild.ToString();
        }

        //returns list of tuples. item 1 is a database object, item 2 is a string message indicating there was an error 
        //accessing a table.

        private static readonly SemaphoreSlim LockGetDatabases = new SemaphoreSlim(1, 1);
        private static readonly Dictionary<Server, DateTime> CacheLastDbRefresh = new Dictionary<Server, DateTime>();

        public static async Task<BindingList<Tuple<Database, string>>> GetDatabases(
            Server currentSqlServer,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                await LockGetDatabases.WaitAsync();

                Console.WriteLine("GetDb");

                var dbResults = new BindingList<Tuple<Database, string>>();

                if (currentSqlServer != null && CacheLastDbRefresh.TryGetValue(currentSqlServer, out var lastRefresh) &&
                    (DateTime.Now - lastRefresh).TotalMinutes > 5)
                {
                    forceRefresh = true;
                }

                if (currentSqlServer != null)
                {
                    CacheLastDbRefresh[currentSqlServer] = DateTime.Now;
                    var x = await Task.Run(() => currentSqlServer.RefreshDatabases(ct), ct);

                    //var availableDbCollection = await Task.Run(() => currentSqlServer.Databases.GetAvailableDatabases());

                    foreach (var db in currentSqlServer.Databases)
                    {
                        //var newDb = new Database();
                        var errorMessage = "";
                        try
                        {
                            await db.RefreshAllTablesList(CancellationToken.None);
                        }
                        catch (Exception ex)
                        {
                            using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                                Log.Error(ex, $"Unhandled exception reading db");

                            errorMessage = "Permission Denied";
                        }

                        dbResults.Add(new Tuple<Database, string>(db, errorMessage));
                    }
                }

                return dbResults;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                //if (ex.InnerException != null)
                //{
                //    MessageBox.Show(ex.InnerException.Message);
                //}

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "General exception getting databases");

                throw;
            }
            finally
            {
                LockGetDatabases.Release();
            }
        }
    }
}
