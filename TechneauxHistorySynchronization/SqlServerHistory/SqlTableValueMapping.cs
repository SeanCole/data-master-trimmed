﻿using System;
using System.Collections.Generic;
using System.Linq;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.SqlHistory.Enumerable;

namespace TechneauxHistorySynchronization.SqlServerHistory
{
    public static class SqlTableValueMapping
    {
        //returns bool that tells whether primary keys are right
     

        //returns a list of tuple containing equivalent .NET datatype and column name

        public static List<Tuple<Type, string>> GetValueTypes(List<SimpleSqlColumn> srcCols, IEnumerable<SqlTableMapping> srcElms)
        {
            var valueTypeMappings = new List<Tuple<Type, string>>();

            var columnList = SqlRowResolver.GetMatchedColumnList(srcCols, srcElms);
            var matchedSqlColObjs = srcCols.ToDictionary(item => item.Name, item => item);

            foreach (var colName in columnList)
            {
                var col = matchedSqlColObjs[colName];

                valueTypeMappings.Add(new Tuple<Type, string>(col.ClrDataType, col.Name));
            }

            return valueTypeMappings;
        }
    }
}
