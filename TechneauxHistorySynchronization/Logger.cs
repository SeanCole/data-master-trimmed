﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace TechneauxHistorySynchronization
{
    public static class LibraryLogging
    {
        const string ThisLoggingLibraryName = "TechneauxHistorySyncronization";

        public static LoggingLevelSwitch LogLevelSwitch { get; set; } = new LoggingLevelSwitch();

        public static void InitLogging(
            string parentApplicationId,
            LogEventLevel loggingLevel)
        {
            var fileName = $"{ThisLoggingLibraryName}_Log_({parentApplicationId}_" + "{Date}.txt";

            LogLevelSwitch = new LoggingLevelSwitch(loggingLevel);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(LogLevelSwitch)
                .Enrich.FromLogContext()
                .WriteTo
                .RollingFile($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\{fileName}", shared: true)
                .CreateLogger();
        }

        //private static readonly ConcurrentDictionary<string, Stopwatch> ObjStopWatches = new ConcurrentDictionary<string, Stopwatch>();

        //private static string GetStopWatchKey(string desc, string callerName)
        //{
        //    var currentThreadId = Thread.CurrentThread.ManagedThreadId;
        //    var currentProcessId = Process.GetCurrentProcess().Id;

        //    var newKey = $"{callerName}.{desc}.{currentProcessId}.{currentThreadId}";

        //    return newKey;
        //}

        //internal static void StartTimingProcess(string desc = "", ILogger localLog = null, [CallerMemberName] string callerName = "")
        //{
        //    var thisKey = GetStopWatchKey(desc, callerName);

        //    if (!ObjStopWatches.TryGetValue(thisKey, out var objStopWatch))
        //    {
        //        objStopWatch = new Stopwatch();
        //        ObjStopWatches.TryAdd(thisKey, objStopWatch);
        //    }
        //    //= objStopWatches[thisKey];

        //    objStopWatch.Reset();
        //    objStopWatch.Start();

        //    var logMessage = $"Stopwatch [Method={callerName}, Description={desc}] Started";

        //    if (localLog != null)
        //        localLog.Verbose(logMessage);
        //    else
        //        Log.Verbose(logMessage);
        //}

        //internal static TimeSpan EndTimingProcess(string desc = "", ILogger localLog = null, [CallerMemberName] string callerName = "")
        //{
        //    var thisKey = GetStopWatchKey(desc, callerName);

        //    if (!ObjStopWatches.TryGetValue(thisKey, out var thisSw))
        //    {
        //        if (thisSw == null)
        //            return TimeSpan.MinValue;

        //        if (thisSw.IsRunning)
        //            thisSw.Stop();

        //        var logMessage = $"Stopwatch [Method={callerName}, Description={desc}] Stopped: Elapsed time (sec): {thisSw.Elapsed.TotalSeconds}";

        //        if (localLog != null)
        //            localLog?.Verbose(logMessage);
        //        else
        //            Log.Verbose(logMessage);

        //        return thisSw.Elapsed;
        //    }

        //    return TimeSpan.MinValue;
        //}

        //static Logger() {
        //    // Get calling exe name            


        //    // Init log

        //}

        //public static Logger Instance
        //{
        //    get
        //    {
        //        if (instance == null)
        //        {
        //            instance = new Logger();
        //        }
        //        return instance;
        //    }
        //}

        //public static void LogError(Exception ex, [CallerMemberName]string memberName = "")
        //{
        //    StackFrame frame = new StackFrame(1);
        //    var method = frame.GetMethod();
        //    var type = method.DeclaringType;
        //    var name = method.Name;

        //    Log.Error(ex, "CallerMemberName=" + memberName + "[" + "method=" + method + "," + "type=" + type + "]");
        //}

        //public static void LogInfo(string info, [CallerMemberName]string memberName = "")
        //{
        //    StackFrame frame = new StackFrame(1);
        //    var method = frame.GetMethod();
        //    var type = method.DeclaringType;
        //    var name = method.Name;

        //    Log.Information(info, "CallerMemberName=" + memberName + "[" + "method=" + method + "," + "type=" + type + "]");
        //}

        //public static void LogInfo(string messageTemplate, params object[] logParams)
        //{
        //    StackFrame frame = new StackFrame(1);
        //    var method = frame.GetMethod();
        //    var type = method.DeclaringType;
        //    var name = method.Name;

        //    Log.Information(messageTemplate, logParams);
        //}
    }
}
