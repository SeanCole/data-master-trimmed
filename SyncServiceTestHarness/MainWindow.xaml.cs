﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Configuration;
using TechneauxDataSyncService;
using Serilog;
using Serilog.Core;
using System.Reactive.Linq;
using TechneauxDataSyncService.Main;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using MahApps.Metro.Controls;
using ReactiveUI;
using Serilog.Formatting.Json;
using TechneauxWpfControls;
//using XmlDataModelUtility;
using TechneauxUtility;

namespace SyncServiceTestHarness
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
       public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Closed += MainWindow_Closed;
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            FileCheckSServ?.CancelAll();
        }

        NewFileCheckService FileCheckSServ;
        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (! await WpfLicenseCheck.CheckLicensing())
            {
                //LicenseFailed = true;
                Close();
            }

            // Check if in developer mode
            if (CvsSubscriptionLicensing.InDeveloperMode)
            {
               // DeveloperBanner.Visibility = Visibility.Visible;
            }

            //TechneauxDataSyncService.ServiceOps.LogAllOpResults = true;
            ServiceOps.RunningFromHarness = true;
            ServiceOps.LogPointSetOperationStatistics = true;

            RxApp.MainThreadScheduler = DispatcherScheduler.Current;
            
            Observable.Interval(TimeSpan.FromMinutes(1))
              .ObserveOnDispatcher()
              .Subscribe(evt =>
              {
                  UpdatePointListView();
              });
            //Observable.Interval(TimeSpan.FromMinutes(1))
            //    .ObserveOnDispatcher()
            //    .Subscribe(evt =>  {
            //        UpdateSyncView();
            //    });

            //var logLevelSwitch = new LoggingLevelSwitch(Serilog.Events.LogEventLevel.Warning);
            var LoggingLevelAttr = System.Configuration.ConfigurationManager.AppSettings.Get("LogLevelConfig");
            Serilog.Events.LogEventLevel newLevel = (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), LoggingLevelAttr);
            var logLevelSwitch = new LoggingLevelSwitch(newLevel);
            Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.ControlledBy(logLevelSwitch)
                    .Enrich.FromLogContext()
                    .WriteTo
                    .File(
                        //new JsonFormatter(),
                        $@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Sync Service Harness Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt", 
                        rollOnFileSizeLimit: true,
                        fileSizeLimitBytes: Bytes.FromMegabytes(10),
                        retainedFileCountLimit: 7,
                        shared: true)
                    .CreateLogger();
            
            var ThisExePath = Environment.CurrentDirectory;
            var ThisConfigPath = $@"{ThisExePath}\ServiceConfigs\";
            FileCheckSServ = new NewFileCheckService(ThisConfigPath, true);
            DataContext = FileCheckSServ;
        
            await FileCheckSServ.RunNewFileCheck();
        }


        private void PointListForSelectedFile_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            UpdatePointListView();

        }

        private void UpdatePointListView()
        {
            //for (int i = 0; i < ConfigGridView.Columns.Count; i++)
            //{
            //    PointViewGrid.Columns[i].Width = 0;
            //}

            //for (int j = 0; j < PointViewGrid.Columns.Count; j++)
            //{
            //    PointViewGrid.Columns[j].Width = double.NaN;
            //}
        }

        //private void UpdateSyncView()
        //{
           
        //    for (int i = 0; i < ConfigGridView.Columns.Count; i++)
        //    {
        //        ConfigGridView.Columns[i].Width = 0;
        //    }
        //    // ConfigGridView.

        //    for (int j = 0; j < ConfigGridView.Columns.Count; j++)
        //    {
        //        ConfigGridView.Columns[j].Width = double.NaN;
        //    }
        //}   

        private void ListOfSyncFileUtility_TargetUpdated(object sender, DataTransferEventArgs e)
        {
            //UpdateSyncView();
        }
    }
}
