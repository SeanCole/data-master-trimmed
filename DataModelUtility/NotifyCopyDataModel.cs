﻿using System;

namespace XmlDataModelUtility
{
    public class NotifyCopyDataModel : NotifyModelBase   //, ICloneable
    {
        //public object Clone()
        //{
        //    return this.Copy();
        //}

        static NotifyCopyDataModel()
        {
            DerivedTypes = Helper.FindAllDerivedTypes<NotifyCopyDataModel>().ToArray();
        }

        public static Type[] DerivedTypes { get; private set; }
    }
}
