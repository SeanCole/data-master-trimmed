﻿namespace XmlDataModelUtility.Rule_Model
{
    public enum RuleValueTypes
    {
        AttributeValue,
        ReportContextValue,
        HistoryValue,
        HistoryList
    }
}
