﻿using Serilog;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Microsoft.VisualBasic.CompilerServices;
using System.Runtime.CompilerServices;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;

namespace XmlDataModelUtility
{
    public static class Bytes
    {
        public static long FromMegabytes(int mb) => mb * 1024 * 1024;

        public static long FromKilobytes(int kb) => kb * 1024;
    }

    public class CallerEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            var skip = 3;
            while (true)
            {
                var stack = new StackFrame(skip);
                if (!stack.HasMethod())
                {
                    logEvent.AddPropertyIfAbsent(new LogEventProperty("Caller", new ScalarValue("<unknown method>")));
                    return;
                }

                var method = stack.GetMethod();
                if (method.DeclaringType.Assembly != typeof(Log).Assembly)
                {
                    var caller =
                        $"{method.DeclaringType.FullName}.{method.Name}({string.Join(", ", method.GetParameters().Select(pi => pi.ParameterType.FullName))})";
                    logEvent.AddPropertyIfAbsent(new LogEventProperty("Caller", new ScalarValue(caller)));
                }

                skip++;
            }
        }
    }

    public static class LoggerCallerEnrichmentConfiguration
    {
        public static LoggerConfiguration WithCaller(this LoggerEnrichmentConfiguration enrichmentConfiguration)
        {
            return enrichmentConfiguration.With<CallerEnricher>();
        }
    }

    public static class GlobalLoggers
    {
        public static ILogger CustomerLogger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .Enrich.FromLogContext()
            .WriteTo
            .File($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Customer Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                    rollOnFileSizeLimit: true,
                    fileSizeLimitBytes: Bytes.FromMegabytes(10),
                    retainedFileCountLimit: 7,
                    shared: true)
            .CreateLogger();

        //public static ILogger GeneralExceptionLogger = new LoggerConfiguration()
        //    .MinimumLevel.Verbose()
        //    .Enrich.FromLogContext()
        //    .WriteTo
        //    .File($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\General Exception Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
        //            rollOnFileSizeLimit: true,
        //            fileSizeLimitBytes: Bytes.FromMegabytes(10),
        //            retainedFileCountLimit: 7,
        //            shared: true)
        //    .CreateLogger();

        public static ILogger Here(this ILogger logger,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0)
        {

            return logger
           .ForContext("MemberName", memberName)
           .ForContext("FilePath", sourceFilePath)
           .ForContext("LineNumber", sourceLineNumber);
        }

        public enum SpecialLogTypes
        {
            AdminEvent,
            UnhandledException
        }

        //public const string SpecialLogSerilogProperty = "Special Log";
        //public const string SpecialLogTypeAdmin = "Admin Event";
        //public const string SpecialLogTypeUnhandled = "Unhandled";

        //public static ILogger AdminEvent(this ILogger logger)
        //{
        //    return logger.
        //        ForContext(SpecialLogSerilogProperty, SpecialLogTypeAdmin);
        //}

        //public static ILogger UnhandledEx(this ILogger logger)
        //{
        //    return logger.
        //        ForContext(SpecialLogSerilogProperty, SpecialLogTypeUnhandled);
        //}


        

        public static bool CheckPoint(string longId)
        {
            var filter = ConfigurationManager.AppSettings.Get("LongIdFilter");
            if (filter == null || filter == "")
            {
                return false;
            }
            if (LikeOperator.LikeString(longId, filter, Microsoft.VisualBasic.CompareMethod.Text))
            {
                return true;
            }
            return false;

        }

        public static ILogger PointLogger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .Enrich.FromLogContext()
            .WriteTo
            .File($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Point Log (started on {DateTime.Now:MMMM dd, yyyy, hh-mm}).txt",
                rollOnFileSizeLimit: true,
                fileSizeLimitBytes: Bytes.FromMegabytes(10),
                retainedFileCountLimit: Math.Max(Convert.ToInt32(ConfigurationManager.AppSettings.Get("PointLogFileCount")), 1),
                shared: true)
            .CreateLogger();
    }
}
