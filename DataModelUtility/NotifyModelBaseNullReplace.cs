﻿using System;
using System.Runtime.CompilerServices;

namespace XmlDataModelUtility
{
    public class NotifyModelBaseNullReplace : NotifyModelBase
    {     
        protected new T GetPropertyValue<T>([CallerMemberName] string propertyName = null)
        {
            if (propertyName == null) throw new ArgumentNullException("propertyName");

            if (!PropertyBackingDictionary.TryGetValue(propertyName, out var value))
            {
                if (typeof(T).IsValueType)
                {
                    return default;
                }
                else
                {
                    if (typeof(T) == typeof(string))
                    {
                        SetPropertyValue("", propertyName);
                    }
                    else
                    {
                        var newItem = (T)Activator.CreateInstance(typeof(T));
                        SetPropertyValue(newItem, propertyName);
                    }

                    return GetPropertyValue<T>(propertyName);
                }
            }

            return (T)value;
        }
    }
}
