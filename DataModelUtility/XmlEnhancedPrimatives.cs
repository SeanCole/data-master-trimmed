﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace XmlDataModelUtility
{
    public class XmlEnhancedPrimatives
    {
        public struct Bool01 : IXmlSerializable
        {
            private bool _value;

            public static implicit operator bool(
                Bool01 num01)
            {
                return num01._value;
            }
            
            public static implicit operator Bool01(
                bool b)
            {
                return new Bool01 { _value = b };
            }

            /// <summary>
            /// This is not used
            /// </summary>
            public XmlSchema GetSchema()
            {
                return null;
            }
            
            public void ReadXml(
                XmlReader reader)
            {
                var s = reader.ReadElementContentAsString().ToLowerInvariant();
                _value = (s=="1");
            }

            public void WriteXml(
                XmlWriter writer)
            {
                writer.WriteString(_value ? "1" : "0");
            }
        }

        public struct BoolTrueFalse : IXmlSerializable
        {
            private bool _value;

            public static implicit operator bool(
                BoolTrueFalse num01)
            {
                return num01._value;
            }
            
            public static implicit operator BoolTrueFalse(
                bool b)
            {
                return new BoolTrueFalse { _value = b };
            }

            /// <summary>
            /// This is not used
            /// </summary>
            public XmlSchema GetSchema()
            {
                return null;
            }
            
            public void ReadXml(
                XmlReader reader)
            {
                var s = reader.ReadElementContentAsString().ToLowerInvariant();
                _value = (s=="true");
            }

            public void WriteXml(
                XmlWriter writer)
            {
                writer.WriteString(_value ? "true" : "false");
            }
        }
    }
}
