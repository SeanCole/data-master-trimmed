﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxUtility
{
    public static class Bytes
    {
        public static long FromMegabytes(int mb) => mb * 1024 * 1024;

        public static long FromKilobytes(int kb) => kb * 1024;
    }
}
