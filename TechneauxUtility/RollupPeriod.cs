﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxReportingDataModel.CygNet.Utility
{
    public class RollupPeriod
    {
        public RollupPeriod(
            DateTime thisEarliestTime,
            DateTime thisLatestTime)
        {
            EarliestTime = thisEarliestTime;
            LatestTime = thisLatestTime;
        }

        public DateTime EarliestTime { get; }

        public DateTime LatestTime { get; }
    }
}
