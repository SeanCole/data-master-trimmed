﻿using System;

namespace TechneauxUtility
{
    public class Result<T>
    {
        public Result(T result)
        {
            Value = result;
            WasSuccessful = true;
        }

        public Result(Exception ex)
        {
            WasSuccessful = false;
            Excp = ex;
            ErrorDescription = ex.Message;
        }

        public Result(Exception ex, string errorDesc)
        {
            WasSuccessful = false;
            Excp = ex;
            ErrorDescription = ex.Message;
        }

        public Result(string errorDesc)
        {
            WasSuccessful = false;
            ErrorDescription = errorDesc;
        }

        public T Value { get; } = default;

        public bool WasSuccessful { get; } = false;

        public string ErrorDescription { get; } = "";

        public Exception Excp { get; } = null;
    }
}
