﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxUtility
{
    public static class ExtensionMethods
    {
        public static bool IsLikePattern(this string srcStr, string compStrPattern)
        {
            return LikeOperator.LikeString(srcStr, compStrPattern, Microsoft.VisualBasic.CompareMethod.Text);
        }

        public static bool IsLikePatterns(this string srcStr, IEnumerable<string> patternList)
        {
            return patternList.Any(srcStr.IsLikePattern);
        }

        public static bool IsPatternListIn(this IEnumerable<string> patternList, IEnumerable<string> compList)
        {
            return compList.Any(comp => comp.IsLikePatterns(patternList));
        }

        public static bool IsAny<T>(this IEnumerable<T> srcEnum)
        {
            return (srcEnum != null && srcEnum.Any());
        }

        public static string GetDuplicatesMessage(this IEnumerable<string> duplicates, int length)
        {
            if (!duplicates.IsAny())
                return "";
            string message = "Duplicate Points: ";
            foreach(var dup in duplicates)
            {
                if((message + dup + ";\n").Length >= 250)
                {
                    return message;
                }
                message = message + dup + ";\n";
                
            }
            return message;
        }

    }

    public enum DictCompareFailureTypes
    {
        None,
        KeyMissing,
        ValueMismatch
    }
    public static class DictionaryExtensions
    {
        public static (bool IsEqual, IList<string> KeyNames,  DictCompareFailureTypes FailureReason) IsEqualToCommonKeysVerbose(
            this IDictionary<string, IConvertibleValue> srcDict,
            IDictionary<string, IConvertibleValue> compDict)
        {
            // Check if keys match

            var commonKeySet = new HashSet<string>(srcDict.Keys);
            commonKeySet.IntersectWith(compDict.Keys);

            var mismatchedValues = commonKeySet.Where(key => !srcDict[key].Equals(compDict[key])).ToList();

            if (mismatchedValues.Any())
            {
                return (false, mismatchedValues.ToList(), DictCompareFailureTypes.ValueMismatch);
            }

            return (true, new List<string>(), DictCompareFailureTypes.None);
        }


        public static (bool IsEqual, IList<string> KeyNames,  DictCompareFailureTypes FailureReason) IsEqualToVerbose(
            this IDictionary<string, IConvertibleValue> srcDict,
            IDictionary<string, IConvertibleValue> compDict)
        {
            // Check if keys match

            var srcKeySet = new HashSet<string>(srcDict.Keys);

            if (!srcKeySet.SetEquals(compDict.Keys))
            {
                srcKeySet.SymmetricExceptWith(compDict.Keys);

                return (false, srcKeySet.ToList(), DictCompareFailureTypes.KeyMissing);
            }

            var mismatchedValues = srcKeySet.Where(key => !srcDict[key].Equals(compDict[key])).ToList();

            if (mismatchedValues.Any())
            {
                return (false, mismatchedValues.ToList(), DictCompareFailureTypes.ValueMismatch);
            }

            return (true, new List<string>(), DictCompareFailureTypes.None);
        }

        public static bool IsEqualTo(
            this IDictionary<string, IConvertibleValue> srcDict,
            IDictionary<string, IConvertibleValue> compDict)
        {
            // Check if keys match

            var srcKeySet = new HashSet<string>(srcDict.Keys);

            if (!srcKeySet.SetEquals(compDict.Keys))
                return false;

            var mismatchedValues = srcKeySet.Where(key => !srcDict[key].Equals(compDict[key]));

            return !mismatchedValues.Any();
        }
    }
}
