﻿using System;
using System.Data.SqlTypes;
using static System.Convert;

namespace TechneauxUtility
{
    public class ConvertibleValue : IConvertibleValue
    {
        public class SqlDateTime2
        {
            public DateTime RawDateTime { get; private set; }
            public int Precision { get; private set; }            

            public DateTime AsSqlDateTime => new SqlDateTime(RawDateTime).Value;

            public SqlDateTime2(DateTime raw, int prec)
            {
                RawDateTime = raw;
                if(prec < 3)
                {
                    Precision = 3;
                }
                else
                {
                    Precision = prec;
                }                
            }

            public DateTime RoundedDateTime => GetRoundedDateTime(RawDateTime, Precision);

            public static DateTime GetRoundedDateTimeNearestMs(DateTime srcDate, int prec)
            {
                int thisPrec = Math.Min(prec, 3);

                return GetRoundedDateTime(srcDate, thisPrec);
            }

            public static DateTime GetRoundedDateTime(
                DateTime srcDate, int prec)
            {
              //  var milliseconds = srcDate.TimeOfDay.TotalMilliseconds;
             //   var digits = milliseconds.ToString().Length;
           //     var strMult = "1";
            //    for(int i = 0; i<digits; i++)
           //     {
            //        strMult = strMult + "0";
            //    }
            //    milliseconds = milliseconds / (Convert.ToDouble(strMult));                
             //   milliseconds = Math.Round(milliseconds, Convert.ToInt32(prec));
           //     milliseconds = milliseconds * (Convert.ToDouble(strMult));
                var roundedSecs = Math.Round(srcDate.TimeOfDay.TotalSeconds, Convert.ToInt32(prec));
                var dateOnly = srcDate.Date;

                return dateOnly.AddSeconds(roundedSecs);
                //return dateOnly.AddMilliseconds(milliseconds);
            }

            public override int GetHashCode() => RawDateTime.GetHashCode();

            public override bool Equals(object obj)
            {
                switch (obj)
                {
                    case DateTime dt:
                        return GetRoundedDateTimeNearestMs(dt, Precision) == RoundedDateTime;

                    case SqlDateTime sqlDt:
                        return sqlDt.Equals(AsSqlDateTime);

                    case SqlDateTime2 sqlDt2:
                        return RawDateTime == sqlDt2.RawDateTime &&
                            Precision == sqlDt2.Precision;
                    default:
                        return base.Equals(obj);
                }                
            }
        }

        public ConvertibleValue(object newVal) => RawValue = newVal;

        public object RawValue { get; }

        public string StringValue => ToString();

        public bool TryGetBool(out bool value)
        {
            switch (RawValue)
            {
                case null:
                    value = false;
                    return false;
                case bool b:
                    value = b;
                    return true;
                default:
                    switch (RawValue.ToString())
                    {
                        case "Y":
                            value = true;
                            return true;
                        case "N":
                            value = false;
                            return true;
                        default:
                            return bool.TryParse(RawValue.ToString(), out value);
                    }
            }
        }

        public bool TryGetDateTime(out DateTime value)
        {
            switch (RawValue)
            {
                case null:
                    value = DateTime.Now;
                    return false;
                case DateTime dt:
                    value = dt;
                    return true;
                case SqlDateTime dt:
                    value = dt.Value;
                    return true;
                case SqlDateTime2 dt:
                    value = dt.RoundedDateTime;
                    return true;

                default:
                    return DateTime.TryParse(RawValue.ToString(), out value);
            }
        }

        public bool TryGetInt(out int value)
        {
            switch (RawValue)
            {
                case null:
                    value = 0;
                    return false;
                case double d:
                    value = Convert.ToInt32(d);
                    return true;
                case int i:
                    value = i;
                    return true;
                default:
                    return int.TryParse(RawValue.ToString(), out value);
            }
        }

        public bool TryGetDouble(out double value)
        {
            switch (RawValue)
            {
                case null:
                    value = 0;
                    return false;
                case double d:
                    value = d;
                    return true;
                case int i:
                    value = Convert.ToDouble(i);
                    return true;
                default:
                    return double.TryParse(RawValue.ToString(), out value);
            }
        }

        public override string ToString()
        {
            if (RawValue is DateTime)
            {
                string myDate = ((DateTime)RawValue).ToString("yyy-MM-dd HH:mm:ss.fff");
                return myDate;
            }
            else if (RawValue is SqlDateTime)
            {
                string myDate = ((SqlDateTime)RawValue).Value.ToString("yyy-MM-dd HH:mm:ss.fff");
                return myDate;
            }
            else if (RawValue is SqlDateTime2)
            {
                string myDate = ((SqlDateTime2)RawValue).RoundedDateTime.ToString("yyy-MM-dd HH:mm:ss.fff");
                return myDate;
            }
            else
            {
                return RawValue?.ToString() ?? "";
            }            
        }

        public DateTime ToDateTime()
        {
            if (IsClrDateTime)
            {
                if (RawValue is DateTime dt)
                    return dt;                
            }
            else if (IsSqlDateTime)
            {
                return ((SqlDateTime)RawValue).Value;
            }
            else if (IsSqlDateTime2)
            {
                return ((SqlDateTime2)RawValue).RoundedDateTime;
            }
            throw new InvalidCastException("Raw Value is not a DateTime and can't be parsed");
        }
            // returns if .net or sql datetime object
        public bool IsDateTime => RawValue is DateTime || RawValue is SqlDateTime || RawValue is SqlDateTime2 || DateTime.TryParse(RawValue.ToString(), out _);

        public bool IsClrDateTime => RawValue is DateTime/* || DateTime.TryParse(RawValue.ToString(), out _)*/;

        public bool IsSqlDateTime => RawValue is SqlDateTime;

        public bool IsSqlDateTime2 => RawValue is SqlDateTime2;

        public DateTime ToSqlDateTime => new SqlDateTime(ToDateTime()).Value;

        public TypeCode GetTypeCode() => Convert.GetTypeCode(RawValue);

        public bool ToBoolean(IFormatProvider provider) => Convert.ToBoolean(RawValue, provider);

        public char ToChar(IFormatProvider provider) => Convert.ToChar(RawValue, provider);

        public sbyte ToSByte(IFormatProvider provider) => Convert.ToSByte(RawValue, provider);

        public byte ToByte(IFormatProvider provider) => Convert.ToByte(RawValue, provider);

        public short ToInt16(IFormatProvider provider) => Convert.ToInt16(RawValue, provider);

        public ushort ToUInt16(IFormatProvider provider) => Convert.ToUInt16(RawValue, provider);

        public int ToInt32(IFormatProvider provider) => Convert.ToInt32(RawValue, provider);

        public uint ToUInt32(IFormatProvider provider) => Convert.ToUInt32(RawValue, provider);

        public long ToInt64(IFormatProvider provider) => Convert.ToInt64(RawValue, provider);

        public ulong ToUInt64(IFormatProvider provider) => Convert.ToUInt64(RawValue, provider);

        public float ToSingle(IFormatProvider provider) => Convert.ToSingle(RawValue, provider);

        public double ToDouble(IFormatProvider provider) => Convert.ToDouble(RawValue);

        public decimal ToDecimal(IFormatProvider provider) => Convert.ToDecimal(RawValue, provider);

        public DateTime ToDateTime(IFormatProvider provider) => Convert.ToDateTime(RawValue);

        public string ToString(IFormatProvider provider) => RawValue?.ToString() ?? "";

        public object ToType(Type conversionType, IFormatProvider provider) => ChangeType(RawValue, conversionType, provider);

        public override int GetHashCode() => RawValue == null ? base.GetHashCode() : RawValue.GetHashCode();

        public override bool Equals(object obj)
        {
            if (RawValue == null)
            {
                if (obj == null)
                {
                    return true;
                }
                return false;
            }
            if(obj == null)
            {
                return false;
            }
            if (!(obj is IConvertibleValue ic)) return false;            

            if (RawValue is DateTime dt && ic.RawValue is DateTime compDt)
            {             
                var compReturn =  dt == compDt;

                return compReturn;
            }
            if(RawValue is DateTime dtr && ic.IsSqlDateTime)
            {
                var tempSqlDateTime = new SqlDateTime(dtr);
                var compReturn = ((SqlDateTime)ic.RawValue).Value.Equals(tempSqlDateTime.Value);
            
                return compReturn;
            }
            if(RawValue is SqlDateTime dtrt && !ic.IsSqlDateTime && !ic.IsSqlDateTime2)
            {
                var tempSqlDateTime = new SqlDateTime((DateTime)ic.RawValue);
                var compReturn = tempSqlDateTime.Value.Equals(dtrt.Value);
            
                return compReturn;
            }
            if(RawValue is SqlDateTime sdt && ic.IsSqlDateTime2)
            {                 
                var compReturn = ((SqlDateTime2)ic.RawValue).Equals(sdt);
                
                return compReturn;
            }
            if(RawValue is SqlDateTime sqlDt && ic.IsSqlDateTime)
            {
                var compReturn = ((SqlDateTime)RawValue).Value.Equals(((SqlDateTime)ic.RawValue).Value);
       
                return compReturn;
            }
            if (RawValue is SqlDateTime2 sdt2)
            {               
                return sdt2.Equals(ic.RawValue);
            }

            var myTypeCode = GetTypeCode();
            var compTypeCode = ic.GetTypeCode();

            myTypeCode = myTypeCode == TypeCode.DBNull ? TypeCode.Empty : myTypeCode;
            compTypeCode = compTypeCode == TypeCode.DBNull ? TypeCode.Empty : compTypeCode;

            if (myTypeCode == compTypeCode)
            {
                return myTypeCode == TypeCode.Empty
                       || RawValue.Equals(ic.RawValue)
                       || RawValue.ToString().Trim() == ic.RawValue.ToString().Trim();
            }
            if (RawValue is char || ic.RawValue is char)
            {
                return RawValue.ToString() == ic.RawValue.ToString();
            }

            if (double.TryParse(RawValue.ToString().Trim(), out double myDbl) && !double.IsNaN(myDbl))
            {
                if (double.TryParse(ic.RawValue.ToString().Trim(), out double compDbl) && !double.IsNaN(compDbl))
                {
                    return Math.Abs(myDbl - compDbl) < .00000001;
                }
            }
            
            return false;
        }
    }

}