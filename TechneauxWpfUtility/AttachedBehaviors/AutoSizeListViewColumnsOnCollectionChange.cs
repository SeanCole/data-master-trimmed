﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TechneauxWpfUtility.AttachedBehaviors
{
    public class AutoSizeListViewColumns : DependencyObject
    {
        [AttachedPropertyBrowsableForChildren]
        public static bool AutoSizeColumns(DependencyObject obj)
        {
            return (bool)obj.GetValue(AutoSizeColumnsProperty);
        }

        [AttachedPropertyBrowsableForChildren]
        public static void AutoSizeColumns(DependencyObject obj, bool value)
        {
            obj.SetValue(AutoSizeColumnsProperty, value);
        }

        // Using a DependencyProperty as the backing store for SetMinWidthToAuto.  
        // This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AutoSizeColumnsProperty =
            DependencyProperty.RegisterAttached("AutoSizeColumns",
                typeof(bool),
                typeof(AutoSizeListViewColumns),
                new UIPropertyMetadata(false, HookUpCollectionChangedEvent));

        public static void HookUpCollectionChangedEvent(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var lv = (ListView)d;

            var doIt = (bool)e.NewValue;

            if (doIt)
            {
                lv.SourceUpdated += ResizeColumns;
            }
        }

        public static void ResizeColumns(object source, EventArgs e)
        {            
            if (source is GridView gridView)
            {
                foreach (var column in gridView.Columns)
                {
                    if (double.IsNaN(column.Width))
                    {
                        column.Width = column.ActualWidth;
                    }
                    column.Width = double.NaN;
                }
            }
        }
    }
}
