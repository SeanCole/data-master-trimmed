using CygNet.Data.Core;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Serilog.Context;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;

namespace CygNetRuleModel.Resolvers.History
{
    public static class NormalizedPointHistory
    {
        // Static methods

        public static async Task<List<CygNetHistoryEntry>> GetPointHistory(ICachedCygNetPoint srcPoint,
            DateTime earliestDate,
            DateTime latestDate,
            int? maxEntries,
            PointHistoryGeneralOptions genHistOpts,
            bool includeNumericOnly,
            IProgress<int> progress,
            CancellationToken ct)
        {
            // Read history and return
            try
            {
                List<CygNetHistoryEntry> cygHistory;

                //Decide what type of filtering should be used
                if (genHistOpts.EnableFloor && genHistOpts.EnableCeiling)
                {
                    cygHistory = maxEntries.HasValue ?
                    await srcPoint.GetLatestHistoryInIntervalFilterRange(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterCeiling, genHistOpts.FilterFloor) :
                    await srcPoint.GetHistoryFilterRange(earliestDate, latestDate, progress, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterCeiling, genHistOpts.FilterFloor);
                }
                else if (genHistOpts.EnableFloor && ! genHistOpts.EnableCeiling)
                {
                    cygHistory = maxEntries.HasValue ?
                    await srcPoint.GetLatestHistoryInIntervalFilterFloor(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterFloor) :
                    await srcPoint.GetHistoryFilterFloor(earliestDate, latestDate, progress, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterFloor);
                }
                else if (! genHistOpts.EnableFloor && genHistOpts.EnableCeiling)
                {
                    cygHistory = maxEntries.HasValue ?
                    await srcPoint.GetLatestHistoryInIntervalFilterCeiling(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterCeiling) :
                    await srcPoint.GetHistoryFilterCeiling(earliestDate, latestDate, progress, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterCeiling);
                }
                else
                {
                    cygHistory = maxEntries.HasValue ?
                    await srcPoint.GetLatestHistoryInInterval(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct) :
                    await srcPoint.GetHistory(earliestDate, latestDate, progress, genHistOpts.IncludeUnreliable, includeNumericOnly, ct);
                }
          

                if (cygHistory == null)
                    return new List<CygNetHistoryEntry>();

                //if (!includeUnreliable)
                //{
                //    cygHistory = cygHistory.Where(entry => !entry.BaseStatus.HasFlag(BaseStatusFlags.Unreliable)).ToList();
                //}
                //if(includeNumericOnly)
                //{
                //    cygHistory = cygHistory.Where(entry => entry.IsNumeric).ToList();
                //}                

                return cygHistory;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception trying to get point history from normalization methods");

                throw;
            }
        }


        public static async Task<List<CygNetHistoryEntry>> GetPointHistoryExtended(
          ICachedCygNetPoint srcPoint,
          DateTime earliestDate,
          DateTime latestDate,
          bool includeUnreliable,
          bool includeNumericOnly,
          int numEntries,
          CancellationToken ct,
          PointHistoryGeneralOptions genHistOpts)
        {
            // Read history and return
            try
            {
                List<CygNetHistoryEntry> cygHistory;
                if(genHistOpts == null)
                {
                    cygHistory = (await
                    srcPoint.GetLatestHistoryInInterval(
                        earliestDate,
                        latestDate,
                        numEntries,
                        includeUnreliable: includeUnreliable,
                        includeNumericOnly: includeNumericOnly,
                        ct));
                }
                else
                {
                    cygHistory = (await
                    GetCorrectHistory(
                    srcPoint,
                    earliestDate,
                    latestDate,
                    includeUnreliable,
                    includeNumericOnly,
                    numEntries,
                    ct,
                    genHistOpts));
                }
                

                if (cygHistory == null)
                    return new List<CygNetHistoryEntry>();
                if (!includeUnreliable)
                {
                    cygHistory = cygHistory.Where(entry => !entry.BaseStatus.HasFlag(BaseStatusFlags.Unreliable)).ToList();
                }
                return cygHistory;
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception trying to get point history from normalization methods for reverse extended.");

                throw;
            }
        }


        public static async Task<List<CygNetHistoryEntry>> GetCorrectHistory(
          ICachedCygNetPoint srcPoint,
          DateTime earliestDate,
          DateTime latestDate,
          bool includeUnreliable,
          bool includeNumericOnly,
          int maxEntries,
          CancellationToken ct,
          PointHistoryGeneralOptions genHistOpts)
        {
            

            if (genHistOpts.EnableFloor && genHistOpts.EnableCeiling)
            {
                return await srcPoint.GetLatestHistoryInIntervalFilterRange(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterCeiling, genHistOpts.FilterFloor);
            }
            else if (genHistOpts.EnableFloor && !genHistOpts.EnableCeiling)
            {
                return await srcPoint.GetLatestHistoryInIntervalFilterFloor(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterFloor);
            }
            else if (!genHistOpts.EnableFloor && genHistOpts.EnableCeiling)
            {
                return await srcPoint.GetLatestHistoryInIntervalFilterCeiling(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct, genHistOpts.FilterCeiling);
            }
            else
            {
                return await srcPoint.GetLatestHistoryInInterval(earliestDate, latestDate, maxEntries, genHistOpts.IncludeUnreliable, includeNumericOnly, ct);
            }
        }
        //public static async Task<List<CygNetHistoryEntry>> GetNormalizedHistoryValuesAsync(
        //    List<CygNetHistoryEntry> rawHist,
        //    DateTime earliestDate,
        //    DateTime latestDate,
        //    PointHistoryNormalizationOptions normalizationOpts
        //    )
        //{
        //    List<CygNetHistoryEntry> normHist;
        //    if (rawHist.IsAny() && normalizationOpts.EnableNormalization != false)
        //    {
        //        var normalizer = new HistoryNormalization(normalizationOpts, earliestDate, latestDate);
        //        normHist = (await normalizer.Normalize(rawHist)).Where(hist => hist.IsValid && !hist.IsValueUncertain).Select(hist => hist.NormalizedHistoryValue).ToList();
        //    }
        //    else
        //    {
        //        normHist = null;
        //    }

        //    return normHist;
        //}
    }
}

// get on demand here



