﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace DataMasterClient.Utility
{
    internal static class ExtensionMethods
    {      
        public static bool IsAny<T>(this IEnumerable<T> srcEnum)
        {
            return (srcEnum != null && srcEnum.Any());
        }

        // Enum methods
        public static string GetDescription(this Enum value)
        {
            var field = value.GetType().GetField(value.ToString());

            return !(Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute)
                ? value.ToString()
                : attribute.Description;
        }

     

    }
}
