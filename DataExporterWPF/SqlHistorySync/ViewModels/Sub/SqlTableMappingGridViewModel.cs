using System.Windows.Input;
using DataMasterClient.Forms;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.SqlHistorySync.ViewModels.Sub
{
    public class SqlTableMappingGridViewModel : NotifyDynamicBase<SqlTableMapping>
    {    
        public SqlTableMappingGridViewModel(SqlTableMapping srcModel) : base(srcModel)
        {           
            ConfiguredRuleView = new SqlTableMappingRuleViewModel(srcModel);
        }

        public ICommand OpenFieldIdWindow => new DelegateCommand(OpenSelectedFieldIdWindow);

        public void OpenSelectedFieldIdWindow(object inputObject)
        {
            var windowInstance = SqlFieldIdWindow.Instance();
          
            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;

            if (result.Value != null)
            {
                MyDataModel.SqlTableFieldName = result.Value as string;
            }
        }

        public SqlTableMappingRuleViewModel ConfiguredRuleView
        {
            get => GetPropertyValue<SqlTableMappingRuleViewModel>();
            set => SetPropertyValue(value);
        }    
    }
}
