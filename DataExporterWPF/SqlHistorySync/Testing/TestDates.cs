﻿using System;

namespace DataMasterClient.Testing
{
    public static class TestDates
    {
        static TestDates()
        {
                
        }

        public static DateTime End = DateTime.Now.AddHours(-1); // this either
        public static DateTime EarliestHist = DateTime.Now.AddDays(-5);
        public static DateTime LatestHist = (DateTime.Now.AddHours(-1));
        public static DateTime Beginning = DateTime.Now.AddDays(-5);

        public static DateTime TestBeg = new DateTime(1, 2, 2, 23, 2,2);
        public static DateTime TestEnd = new DateTime(1,2,3,18,8,8);
    }
}
