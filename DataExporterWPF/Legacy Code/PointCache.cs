﻿
using System;
using System.Collections.Concurrent;
using System.Linq;
using static CygNetRuleModel.Rules.PointHistoryGeneralOptions;
using static DataExporter.DataModel.ReportElm;

namespace DataExporter
{
    public static partial class PointHistory
    {
        private static class PointCache
        {
            private static ConcurrentDictionary<string, PointValue> PointValueCache = new ConcurrentDictionary<string, PointValue>();
            private static ConcurrentDictionary<DateTime, string> PointTimestampCache = new ConcurrentDictionary<DateTime, string>();

            public static void AddPointToCache(
                string cacheKey,
                PointValue value)
            {
                if (!PointValueCache.ContainsKey(cacheKey))
                {
                    PointValueCache.TryAdd(cacheKey, value);
                    PointTimestampCache.TryAdd(DateTime.Now, cacheKey);

                    if (PointTimestampCache.Count > 20)
                    {
                        DateTime OldestTimestamp = PointTimestampCache.Keys.Min();

                        string KeyToRemove;

                        if (PointTimestampCache.TryGetValue(OldestTimestamp, out KeyToRemove))
                        {
                            PointValue ThisVal;
                            PointValueCache.TryRemove(KeyToRemove, out ThisVal);

                            string ThisKey;
                            PointTimestampCache.TryRemove(OldestTimestamp, out ThisKey);
                        }
                    }
                }
            }

            public static bool TryGetPointFromCache(
                 string cacheKey,
                 out PointValue thisValue)
            {
                return PointValueCache.TryGetValue(cacheKey, out thisValue);
            }

            public static string GetCacheKey(
                 string uisService,
                 string facTag,
                 string udc,
                 string vhsService,
                 bool allowUnreliable,
                 HistoryTypes rollupType,
                 DateTime earliestDate,
                 DateTime latestDate)
            {
                return $"uis={uisService}|ft={facTag}|udc={udc}|vhs={vhsService}|unr={allowUnreliable}|roll={rollupType}|early={earliestDate}|late={latestDate}";
            }

        }
    }
}