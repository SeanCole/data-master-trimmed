﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using DataMasterClient.SqlHistorySync.ViewModels;
using DataMasterClient.Utility;
using TechneauxWpfControls.DataConverters;

namespace DataMasterClient.Controls
{
    /// <summary>
    /// Interaction logic for PointSelectionPreview.xaml
    /// </summary>
    public partial class PointSelectionPreview : UserControl   //, INotifyPropertyChanged
    {
        public PointSelectionPreview()
        {
            InitializeComponent();
            Loaded += UserControl1_Loaded;

            DataContextChanged += PointSelectionPreview_DataContextChanged;
        }

        private void PointSelectionPreview_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            AddGridColumns();
        }

        void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            if(DataContext is INotifyPropertyChanged)
            {
                var myVm = DataContext as INotifyPropertyChanged;
                myVm.PropertyChanged += MyVM_PropertyChanged;
                AddGridColumns();
            }
            //AddGridColumns();
        }

        private void MyVM_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "UdcList")
            {
                AddGridColumns();
            }
        }
        
        protected void AddGridColumns()
        {

            if (DataContext is PointSelectionViewModel)
            {
                var myVm = DataContext as PointSelectionViewModel;
                //myVm.IsPointPreviewBusy = true;
                var udcList = myVm.UdcList;
                if (!udcList.IsAny())
                {
                    if (PointPreviewGridView.Columns.Count > 1)
                    {
                        for (var k = PointPreviewGridView.Columns.Count - 1; k > 0; k--)
                        {
                            PointPreviewGridView.Columns.RemoveAt(k);
                        }
                    }
                    return;
                }
                    
                PointPreviewGridView.AllowsColumnReorder = true;
                PointPreviewGridView.ColumnHeaderToolTip = "UDC";
                if (PointPreviewGridView.Columns.Count > 1 + udcList.Count)
                {
                    for (var k = PointPreviewGridView.Columns.Count - 1; k > 0; k--)
                    {
                        PointPreviewGridView.Columns.RemoveAt(k);
                    }
                }
                for (var i = PointPreviewGridView.Columns.Count; i < 1 + udcList.Count; i++)
                {
                    PointPreviewGridView.Columns.Add(new GridViewColumn());
                }

                for (var i = 1; i < PointPreviewGridView.Columns.Count; i++)
                {
                    var udc = udcList[i - 1];
                    
                    //GridViewColumn newColumn = new GridViewColumn();
                    var hasUdcConverter = new BoolXOConverter();
                    var columnBinding = new Binding
                    {
                        Converter = hasUdcConverter,
                        ConverterParameter = udc
                    };

                    var thisCol = PointPreviewGridView.Columns[i];
     
                    thisCol.DisplayMemberBinding = columnBinding;
                    thisCol.Header = udc;

                    thisCol.Width = thisCol.ActualWidth;
                    thisCol.Width = double.NaN;
                    //PointPreviewGridView.Columns.Add(newColumn);                    
                }
                var refreshBinding = new Binding("FacilityUdcGrid");
                PointPreview.ItemsSource = null;
                BindingOperations.SetBinding(PointPreview, ItemsControl.ItemsSourceProperty, refreshBinding);
                //myVm.IsPointPreviewBusy = false;
            }
            else
            {
                for (var k = PointPreviewGridView.Columns.Count - 1; k > 0; k--)
                {
                    PointPreviewGridView.Columns.RemoveAt(k);
                }
            }
        }

    }
}
