﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;

namespace DataMasterClient.Controls
{

    /// <summary>
    /// Interaction logic for RuleNormalizationPicker.xaml
    /// </summary>
    public partial class HistoryPointPollingPicker : UserControl
    {
        public HistoryPointPollingPicker()
        {
            InitializeComponent();
        }
        private void NumericUpDown_GotFocus(object sender, RoutedEventArgs e)
        {
            var focused = sender as NumericUpDown;
            focused.HideUpDownButtons = false;
        }

        private void NumericUpDown_LostFocus(object sender, RoutedEventArgs e)
        {
            var lostFocused = sender as NumericUpDown;
            lostFocused.HideUpDownButtons = true;
        }


    }
}

