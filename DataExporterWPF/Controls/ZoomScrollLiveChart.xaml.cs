﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DataMasterClient.ViewModels;
using LiveCharts.Events;
using LiveCharts.Wpf;

namespace DataMasterClient.Controls
{
    /// <summary>
    /// Interaction logic for ZoomScrollLiveChart.xaml
    /// </summary>
    public partial class ZoomScrollLiveChart : UserControl
    {
        public ZoomScrollLiveChart()
        {
            InitializeComponent();
        }

        private void Axis_OnRangeChanged(RangeChangedEventArgs eventargs)
        {
            var vm = (HistoryTrendViewModel)DataContext;

            var currentRange = eventargs.Range;

            //if (currentRange < TimeSpan.TicksPerHour)
            //    (eventargs.Axis as Axis).SetRange(TimeSpan.TicksPerHour);

            if (currentRange < TimeSpan.TicksPerDay * 2)
            {
                vm.XFormatter = x => new DateTime((long)x).ToString("t");
                return;
            }

            if (currentRange < TimeSpan.TicksPerDay * 60)
            {
                vm.XFormatter = x => new DateTime((long)x).ToString("dd MMM yy");
                return;
            }

            if (currentRange < TimeSpan.TicksPerDay * 540)
            {
                vm.XFormatter = x => new DateTime((long)x).ToString("MMM yy");
                return;
            }

            vm.XFormatter = x => new DateTime((long)x).ToString("yyyy");
        }
    }
}
