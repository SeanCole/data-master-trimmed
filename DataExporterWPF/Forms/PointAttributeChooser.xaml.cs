﻿using System;
using System.Windows;
using System.Windows.Input;
using GenericRuleModel.Helper;
using MahApps.Metro.Controls;
using Techneaux.CygNetWrapper.Points;
using TechneauxWpfControls.DataConverters;

namespace DataMasterClient.Forms
{
    /// <summary>
    /// Interaction logic for PointLinkingAttributeWindow.xaml
    /// </summary>
    public partial class PointAttributeChooser : MetroWindow
    {
        private static object _staticDataContext = null;
        public static object StaticDataContext
        {
            get => _staticDataContext;
            set
            {
                _staticDataContext = null;
                _staticDataContext = value;
            }
        }

        public PointAttributeChooser()
        {
            InitializeComponent();

            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
            AttributeListView.MouseDoubleClick += AttributeListView_MouseDoubleClick;


            //throw new NotImplementedException("Add event listener for bound list changes by reading the context object at runtime");
        }

        private void AttributeListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!(AttributeListView.SelectedItem is PointAttribute selectedAttributeItem))
            {
                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.Id;
            _thisChooserResult.Description = selectedAttributeItem.Description;

            _wasCancelled = false;

            Close();
        }
   
        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Close();
        }
        public static PointAttributeChooser Instance()
        {
            if (_staticDataContext == null)
            {
                throw new InvalidOperationException("Static Data Context must not be null");
            }

            var instance = new PointAttributeChooser
            {
                DataContext = _staticDataContext
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            Owner = Application.Current.MainWindow;
            ShowDialog();

            result = _thisChooserResult;
            return !_wasCancelled;
        }


        private bool _wasCancelled = false;
        private readonly ValueDescription _thisChooserResult = new ValueDescription();

        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            if (!(AttributeListView.SelectedItem is PointAttribute selectedAttributeItem))
            {

                return;
            }

            _thisChooserResult.Value = selectedAttributeItem.Id;
            _thisChooserResult.Description = selectedAttributeItem.Description;

            _wasCancelled = false;

            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            _wasCancelled = true;

            Close();
        }
    }
}
