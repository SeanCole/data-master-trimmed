﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using DataMasterClient.ViewModels;
using MahApps.Metro.Controls;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;
using TechneauxUtility;

namespace DataMasterClient.Forms
{
    /// <summary>
    /// Interaction logic for AllPointPopupWindowSync.xaml
    /// </summary>
    public partial class LicenseForm : MetroWindow
    {
        public LicenseForm()
        {
            InitializeComponent();
        }
        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public bool IsTrialExpired()
        {
            if (TrialPeriod.LicenseStatus.approvalType == TrialPeriod.TrialStatuses.TrialExpired)
            {
                LicenseMessage.Text =
                    "Your trial period has expired, please contact Techneaux.";

                DayCount.Text = "";
                SecondMessage.Text = "";
            }
            else
            {
                DayCount.Text = TrialPeriod.DaysTillExpiration.ToString();
            }
            
            ShowDialog();

            if (TrialPeriod.LicenseStatus.approvalType == TrialPeriod.TrialStatuses.TrialExpired)
            {
                return true;
            }

            return false;
        }
    }
}
