using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class PollingRetentionViewModel : NotifyDynamicBase<PointHistoryPollingRetentionOptions>
    {
        public PollingRetentionViewModel(PointHistoryPollingRetentionOptions srcModel) : base(srcModel)
        {
     
        }
    }
}