using System.Windows.Input;
using DataMasterClient.Forms;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SqlHistory.Enumerable;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class RuleViewModel : NotifyDynamicBase<SqlTableMapping>
    {    
        public RuleViewModel(SqlTableMapping srcModel) : base(srcModel)
        {
            var tempRule = (MyDataModel.CygNetElementRule);
            ConfiguredRuleView = new CygNetRuleView(tempRule);
        }

        public ICommand OpenFieldIdWindow => new DelegateCommand(OpenSelectedFieldIdWindow);

        public void OpenSelectedFieldIdWindow(object inputObject)
        {      
            var windowInstance = SqlFieldIdWindow.Instance();
          
            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.SqlTableFieldName = result.Value as string;
                }
                //if (result.Description != null)
                //{
                //    MyDataModel.AttributeID.Desc = result.Description as string;

                //}
            }
        }

        public CygNetRuleView ConfiguredRuleView
        {
            get => GetPropertyValue<CygNetRuleView>();
            set => SetPropertyValue(value);
        }    
    }
}
