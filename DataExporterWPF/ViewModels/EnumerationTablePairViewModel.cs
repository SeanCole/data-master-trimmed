using GenericRuleModel.Rules;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class EnumerationTablePairViewModel : NotifyDynamicBase<EnumerationTable.EnumerationPair>
    {
        public EnumerationTablePairViewModel(EnumerationTable.EnumerationPair srcModel) : base(srcModel)
        {
        }  
    }
}

