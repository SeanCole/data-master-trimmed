using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DataMasterClient.ViewModels.Chart;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Wpf;
using TechneauxHistorySynchronization.Models;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class PointPreviewCompareViewModel : NotifyDynamicBase<CygNetSqlDataCompareModel>
    {
        public PointPreviewCompareViewModel(CygNetSqlDataCompareModel srcModel) : base(srcModel)
        { 
            var dayConfig = Mappers.Xy<DateModel>()
                .X(dayModel => (double)dayModel.DateTime.Ticks / TimeSpan.FromHours(1).Ticks)
                .Y(dayModel => dayModel.Value);

            Series = new SeriesCollection(dayConfig)
            {
                new LineSeries()
                {
                    Values = new ChartValues<DateModel>(),
                    Title = "CygNet History"
                },
                new LineSeries()
                {
                    Values = new ChartValues<DateModel>(),
                    Title = "Normalized"
                }
            };
            CancellationToken ct;
        
            //RawHist = MyDataModel.RawHistory.Select(item => new HistoryEntryViewModel(item)).ToList();

            //NormHist = MyDataModel.NormalizedHistory.Select(item => new HistoryEntryViewModel(item)).ToList();
            //SqlCompareList = MyDataModel.ComparedRows.Select(item => new SqlRowViewModel(item)).ToList();
            Formatter = value => new DateTime((long)(value * TimeSpan.FromHours(1).Ticks)).ToString("t");
        }

        //private void SrcModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        //{
        //    RawHist = MyDataModel.RawHistory.Select(item => new HistoryEntryViewModel(item)).ToList();
            
        //    NormHist = MyDataModel.NormalizedHistory.Select(item => new HistoryEntryViewModel(item)).ToList();
        //    SqlCompareList = MyDataModel.ComparedRows.Select(item => new SqlRowViewModel(item)).ToList();
        //}

        public Func<double, string> Formatter { get; set; }
        public SeriesCollection Series
        {
            get => GetPropertyValue<SeriesCollection>();
            set => SetPropertyValue(value);
        }

        public List<SqlRowViewModel> SqlCompareList
        {
            get => GetPropertyValue<List<SqlRowViewModel>>();
            set => SetPropertyValue(value);
        }
        public List<HistoryEntryViewModel> RawHist
        {
            get => GetPropertyValue<List<HistoryEntryViewModel>>();
            set => SetPropertyValue(value);
        }

        public string Udc
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }
        public string ShortId
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public List<HistoryEntryViewModel> NormHist
        {
            get => GetPropertyValue<List<HistoryEntryViewModel>>();
            set => SetPropertyValue(value);
        }
    }
}
