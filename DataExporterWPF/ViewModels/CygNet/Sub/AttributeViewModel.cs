using System.Windows.Input;
using DataMasterClient.Forms;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxWpfControls.DataConverters;

namespace DataMasterClient.ViewModels.CygNet.Sub
{
    public class AttributeViewModel : XmlDataModelUtility.NotifyDynamicBase<FacilityPointAttributeOptions>
    {
        public AttributeViewModel(FacilityPointAttributeOptions srcModel) : base(srcModel)
        {
        }

        public void ClearControl()
        {
            MyDataModel.AttributeKey.Id = "";
            MyDataModel.AttributeKey.Desc = "";
        }
        public ICommand OpenRuleFacilityAttributeWindow => new DelegateCommand(OpenSelectRuleFacilityAttributeWindow);

        public void OpenSelectRuleFacilityAttributeWindow(object inputObject)
        {
            var windowInstance = FacilityAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value != null)
            {
                MyDataModel.AttributeKey.Id = result.Value.ToString();

            }
            if (result.Description != null)
            {
                MyDataModel.AttributeKey.Desc = result.Description;
            }
        }
        public ICommand OpenRulePointAttributeWindow => new DelegateCommand(OpenSelectRulePointAttributeWindow);

        public void OpenSelectRulePointAttributeWindow(object inputObject)
        {
            var windowInstance = PointAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;

            if (result.Value != null)
            {
                MyDataModel.AttributeKey.Id = result.Value as string;
            }

            if (result.Description != null)
            {
                MyDataModel.AttributeKey.Desc = result.Description;
            }
        }

    }
}