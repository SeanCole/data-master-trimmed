using System.ComponentModel;
using TechneauxReportingDataModel.Helper;

namespace DataMasterClient.ViewModels
{
    public class SinglePointCompareSummaryViewModel// : ReactiveUI.ReactiveObject*/
    {
        [Browsable(false)]
        public CachedCygNetPointWithRule MyDataModel { get; }

        public SinglePointCompareSummaryViewModel(CachedCygNetPointWithRule srcModel)
        {
            //Udc = srcModel.Tag.UDC;
            MyDataModel = srcModel;

            //FacilityTagUdc = srcModel.Tag.GetTagFacilityUDC();

            //ShortId = srcModel.ShortId;



        }

        //ObservableAsPropertyHelper<>

        [DisplayName("SITE")]
        public string Site => MyDataModel.Tag.Site;

        [DisplayName("SERVICE")]
        public string Service => MyDataModel.Tag.Service;

        [DisplayName("FACILITY")]
        public string FacilityId => MyDataModel.Tag.FacilityId;

        [DisplayName("UDC")]
        public string Udc => MyDataModel.Tag.UDC;

        [DisplayName("POINT ID")]
        public string ShortId => MyDataModel.Tag.PointId;

        

        //public int TotalRaw { get; private set; }

        //public int TotalNorm { get; private set; }

        //public double HistDataInSql { get; private set; }
    }
}
