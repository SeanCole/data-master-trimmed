using DataMasterClient.Forms;
using System.Windows;
using System.Windows.Input;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.CygNet.Rules;
using XmlDataModelUtility;
using static TechneauxReportingDataModel.CygNet.StatusBitMetadata;

namespace DataMasterClient.ViewModels
{
    public class PointHistoryViewModel : NotifyDynamicBase<PointHistoryGeneralOptions>
    {
        public PointHistoryViewModel(PointHistoryGeneralOptions srcModel) : base(srcModel)
        {
            IsExcludeBlanksVisible = Visibility.Visible;
            IsDefVisible = Visibility.Visible;
            IsCeilingVisible = Visibility.Visible;
            IsFloorVisible = Visibility.Visible;

        }
        #region Range Window

        [AffectedByOtherPropertyChange(nameof(PointHistoryGeneralOptions.EnableCeiling))]
        public Visibility IsCeilingVisible
        {
            get
            {

                if (MyDataModel.EnableCeiling == true)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(PointHistoryGeneralOptions.EnableFloor))]
        public Visibility IsFloorVisible
        {
            get
            {

                if (MyDataModel.EnableFloor == true)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        #endregion

        [AffectedByOtherPropertyChange(nameof(PointHistoryGeneralOptions.BlankValueHandlingType))]
        public Visibility IsDefVisible
        {
            get
            {
                var tempRule = (PointHistoryGeneralOptions)MyDataModel;
                if (tempRule.BlankValueHandlingType == PointHistoryGeneralOptions.BlankValueHandlingTypes.ReplaceWithDefault)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        public Visibility IsExcludeBlanksVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public ICommand OpenAlarmBitChooserWindow => new DelegateCommand(OpenSelectAlarmBitChooserWindow);

        public void OpenSelectAlarmBitChooserWindow(object inputObject)
        {
            var windowInstance = AlarmBitChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;

            if (result.Value != null)
            {
                MyDataModel.SelectedBit = result.Value as BitMetadata;
            }

            //if (result.Description != null)
            //{
            //    MyDataModel.AttributeKey.Desc = result.Description;
            //}
        }

        public Visibility IsOnlyNumericVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
    }
}