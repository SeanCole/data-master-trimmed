using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using DataMasterClient.Forms;
using DataMasterClient.ViewModels.CygNet.Sub;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.ViewModels
{
    public class FacilityFilteringViewModel : NotifyDynamicBase<FacilityFilteringOptions>
    {
        public FacilityFilteringViewModel(FacilityFilteringOptions srcModel) : base(srcModel)
        {
            ConditionList = new BindingList<CygNetFacilityFilterConditionViewModel>();
            for (var i = 0; i < srcModel.BaseRules.Count; i++)
            {
                var model = new CygNetFacilityFilterConditionViewModel(MyDataModel.BaseRules[i]);
                ConditionList.Add(model);
            }

            ChildGroupsViewModels = new BindingList<ChildFacilityGroupViewModel>();
            for (var i = 0; i < srcModel.ChildGroups.Count(); i++)
            {
                var childGroupModel = new ChildFacilityGroupViewModel(srcModel.ChildGroups[i]);
                ChildGroupsViewModels.Add(childGroupModel);
            }

            CygNetChildFacilityWindow.StaticDataContext = this;
            SelectedChildGroupCondition = null;
        }

        public ChildFacilityGroupViewModel SelectedChildGroup
        {
            get => GetPropertyValue<ChildFacilityGroupViewModel>();
            set => SetPropertyValue(value);
        }

        public BindingList<ChildFacilityGroupViewModel> ChildGroupsViewModels
        {
            get => GetPropertyValue<BindingList<ChildFacilityGroupViewModel>>();
            set => SetPropertyValue(value);
        }

        public CygNetFacilityFilterConditionViewModel SelectedChildGroupCondition
        {
            get => GetPropertyValue<CygNetFacilityFilterConditionViewModel>();
            set => SetPropertyValue(value);
        }

        public CygNetChildFacilityWindow ChildFacilityWindow
        {

            get => GetPropertyValue<CygNetChildFacilityWindow>();
            set => SetPropertyValue(value);
        }

        public BindingList<CygNetFacilityFilterConditionViewModel> ConditionList
        {
            get => GetPropertyValue<BindingList<CygNetFacilityFilterConditionViewModel>>();
            set => SetPropertyValue(value);
        }

        public ICommand AddRow => new DelegateCommand(AddNewRow);

        public void DeleteSelectedChildGroupRow(object inputObject)
        {

            var index = ChildGroupsViewModels.IndexOf(SelectedChildGroup);
            ChildGroupsViewModels.Remove(SelectedChildGroup);

            if (ChildGroupsViewModels.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= ChildGroupsViewModels.Count)
                index = ChildGroupsViewModels.Count - 1;

            SelectedChildGroup = ChildGroupsViewModels[index];       
        }

        public void AddSelectedChildGroupRow(object inputObject)
        {
            var newChildGroup = new ChildFacilityGroup();
            var newChildGroupView = new ChildFacilityGroupViewModel(newChildGroup);
            if (SelectedChildGroup == null)
            {

                MyDataModel.ChildGroups.Add(newChildGroup);

                ChildGroupsViewModels.Add(newChildGroupView);
                SelectedChildGroup = ChildGroupsViewModels[ChildGroupsViewModels.Count - 1];
                return;
            }

            var index = ChildGroupsViewModels.IndexOf(SelectedChildGroup);
            if (index < 0)
                index = 0;
            else if (index >= ChildGroupsViewModels.Count)
                index = ChildGroupsViewModels.Count - 1;

            MyDataModel.ChildGroups.Insert(index, newChildGroup);

            ChildGroupsViewModels.Insert(index, newChildGroupView);

            if (index < 0)
                index = 0;
            else if (index >= ChildGroupsViewModels.Count)
                index = ChildGroupsViewModels.Count - 1;

            SelectedChildGroup = ChildGroupsViewModels[index];

        }

        public CygNetFacilityFilterConditionViewModel SelectedCondition
        {
            get => GetPropertyValue<CygNetFacilityFilterConditionViewModel>();
            set => SetPropertyValue(value);
        }

        public void AddNewRow(object inputObject)
        {
            if (SelectedCondition == null)
            {
                var newConditionView = new CygNetFacilityFilterConditionViewModel(MyDataModel.AddNewCondition(ConditionList.Count));
                ConditionList.Add(newConditionView);
                SelectedCondition = ConditionList[ConditionList.Count - 1];
                return;
            }

            var index = ConditionList.IndexOf(SelectedCondition);
            if (index < 0)
                index = 0;
            else if (index >= ConditionList.Count)
                index = ConditionList.Count - 1;


            var ruleView = new CygNetFacilityFilterConditionViewModel(MyDataModel.AddNewCondition(index));
            ConditionList.Insert(index, ruleView);


            if (index < 0)
                index = 0;
            else if (index >= ConditionList.Count)
                index = ConditionList.Count - 1;

            SelectedCondition = ConditionList[index];

        }

        public ICommand DeleteRow => new DelegateCommand(DeleteSelectedRow);


        public ICommand OpenChildReferenceAttributeWindow => new DelegateCommand(OpenSelectedChildReferenceAttributeWindow);

        public ICommand OpenChildOrdinalAttributeWindow => new DelegateCommand(OpenSelectedChildOrdinalAttributeWindow);

        public void OpenSelectedChildOrdinalAttributeWindow(object inputObject)
        {
            var windowInstance = FacilityAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value != null)
            {
                MyDataModel.ChildOrdinalAttrKey.Id = result.Value as string;
            }
            if (result.Description != null)
            {
                MyDataModel.ChildOrdinalAttrKey.Desc = result.Description as string;
            }
        }

        public void OpenSelectedChildReferenceAttributeWindow(object inputObject)
        {
            var windowInstance = FacilityAttributeChooser.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.ChildReferenceAttrKey.Id = result.Value as string;

                }
                if (result.Description != null)
                {
                    MyDataModel.ChildReferenceAttrKey.Desc = result.Description as string;
                }
            }
        }

        public void DeleteSelectedRow(object inputObject)
        {

            var index = ConditionList.IndexOf(SelectedCondition);
            ConditionList.Remove(SelectedCondition);
            MyDataModel.DeleteCondition(index);
            if (ConditionList.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= ConditionList.Count)
                index = ConditionList.Count - 1;

            SelectedCondition = ConditionList[index];

            return;

        }
        public ICommand DeleteGroupFilterRow => new DelegateCommand(DeleteSelectedGroupFilterRow);

        public ICommand AddGroupFilterRow => new DelegateCommand(AddSelectedGroupFilterRow);

        public void DeleteSelectedGroupFilterRow(object inputObject)
        {
            if (SelectedChildGroup == null)
                return;

            var index = SelectedChildGroup.ChildGroupConditionsViewModels.IndexOf(SelectedChildGroupCondition);
            SelectedChildGroup.ChildGroupConditionsViewModels.Remove(SelectedChildGroupCondition);
            SelectedChildGroup.DeleteCondition(index);

            if (SelectedChildGroup.ChildGroupConditionsViewModels.FirstOrDefault() == null)
                return;

            if (index < 0)
                index = 0;
            else if (index >= SelectedChildGroup.ChildGroupConditionsViewModels.Count)
                index = SelectedChildGroup.ChildGroupConditionsViewModels.Count - 1;

            SelectedChildGroupCondition = SelectedChildGroup.ChildGroupConditionsViewModels[index];

            return;

        }
        public void AddSelectedGroupFilterRow(object inputObject)
        {
            if (SelectedChildGroup == null)
                return;
            FacilityAttributeFilterCondition newCondition;
            CygNetFacilityFilterConditionViewModel newConditionView;
            if (SelectedChildGroupCondition == null)
            {
                newCondition = SelectedChildGroup.AddNewCondition(SelectedChildGroup.ChildGroupConditionsViewModels.Count);

                newConditionView = new CygNetFacilityFilterConditionViewModel(newCondition);
                SelectedChildGroup.ChildGroupConditionsViewModels.Add(newConditionView);
                SelectedChildGroupCondition = SelectedChildGroup.ChildGroupConditionsViewModels[SelectedChildGroup.ChildGroupConditionsViewModels.Count - 1];
                return;
            }


            var index = SelectedChildGroup.ChildGroupConditionsViewModels.IndexOf(SelectedChildGroupCondition);
            if (index < 0)
                index = 0;
            else if (index >= SelectedChildGroup.ChildGroupConditionsViewModels.Count)
                index = SelectedChildGroup.ChildGroupConditionsViewModels.Count - 1;

            newCondition = SelectedChildGroup.AddNewCondition(index);
            newConditionView = new CygNetFacilityFilterConditionViewModel(newCondition);
            SelectedChildGroup.ChildGroupConditionsViewModels.Insert(index, newConditionView);


            if (index < 0)
                index = 0;
            else if (index >= SelectedChildGroup.ChildGroupConditionsViewModels.Count)
                index = SelectedChildGroup.ChildGroupConditionsViewModels.Count - 1;

            SelectedChildGroupCondition = SelectedChildGroup.ChildGroupConditionsViewModels[index];

        }
        public ICommand AddChildGroupRow => new DelegateCommand(AddSelectedChildGroupRow);

        public ICommand DeleteChildGroupRow => new DelegateCommand(DeleteSelectedChildGroupRow);
    }
}

