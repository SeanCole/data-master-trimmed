﻿using DataMasterClient.SmartSheetSync.View;
using DataMasterClient.ViewModels;
using DataMasterClient.ViewModels.General;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Serilog.Context;
using TechneauxReportingDataModel.CygNet.Utility;
using TechneauxSmartSheetSync.Resolver;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace DataMasterClient
{
    public static class AutoOperation
    {

        public enum AppExitCodes
        {
            CannotCreateLogSubfolder = 11,
            CannotWriteLogFile = 12,

            AutoConfigPathInvalid = 21,
            AutoBatchFolderInvalid = 22,
            NoConfigFilesInBatchFolder = 23
        }

        public enum AutoCmdType
        {
            Invalid,
            Folder,
            SingleFile
        }

        public static async Task RunAutoOperationAsync(DateTime runDate, List<string> commandLineParams, ConfigModelFileService configFileSvc, CancellationToken ct)
        {
            commandLineParams = (from arg in commandLineParams select arg.Replace("\"", "")).ToList();

            var newLevel = (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), "Information");
            var logLevelSwitch = new LoggingLevelSwitch(newLevel);

            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .WriteTo
                .RollingFile($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Logs\SmartSheet Auto Run Log.txt", shared: true)
                .CreateLogger();
            try
            {
                // Init log 
                string logPath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Logs";

                if (!Directory.Exists(logPath))
                {
                    //Attempt to create log folder
                    try
                    {
                        Directory.CreateDirectory(logPath);
                    }
                    catch (UnauthorizedAccessException)
                    {
                        logPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, "Failure creating log folder");

                        //use serilog here
                        // Log..WriteEvent($"Techneaux Data Extractor - Error trying to create Log subfolder [Ex: {ex.ToString()}]",
                        //     System.Diagnostics.EventLogEntryType.Error);
                        Environment.ExitCode = 2;
                    }
                }

                string cmdLineConfigPath = "", cmdLineBatchFolder = "", logAutoTypeString = "";

                AutoCmdType cmdType;
                if (commandLineParams.Exists(item => item.ToLower().StartsWith(@"/configpath=") && item.ToLower().EndsWith(@".xml")))
                {
                    cmdLineConfigPath = commandLineParams.First(item => item.ToLower().StartsWith(@"/configpath=") && item.ToLower().EndsWith(@".xml"));
                    cmdLineConfigPath = cmdLineConfigPath.Replace(@"/configpath=", "");

                    cmdType = AutoCmdType.SingleFile;

                    try
                    {
                        logAutoTypeString = Path.GetFileNameWithoutExtension(cmdLineConfigPath);
                    }
                    catch (ArgumentException)
                    {
                        logAutoTypeString = cmdLineConfigPath;
                    }

                }
                else if (commandLineParams.Exists(item => item.ToLower().StartsWith(@"/batchfolder=")))
                {
                    // Extract the folder name and determine if the folder exists
                    cmdLineBatchFolder = commandLineParams.First(item => item.ToLower().StartsWith(@"/batchfolder="));
                    cmdLineBatchFolder = cmdLineBatchFolder.Replace(@"/batchfolder=", "");
                    cmdType = AutoCmdType.Folder;

                    try
                    {
                        logAutoTypeString = new DirectoryInfo(cmdLineBatchFolder).Name;
                    }
                    catch (Exception ex) when (ex is ArgumentException || ex is SecurityException || ex is ArgumentNullException || ex is PathTooLongException)
                    {
                        logAutoTypeString = cmdLineBatchFolder;
                    }
                }
                else
                {
                    cmdType = AutoCmdType.Invalid;
                }

                string logFileFullPath = $@"{logPath}\Report Log (Autorun - {logAutoTypeString}) {DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss-fff")}";
                string logFileExt = ".txt";

                int suffixNumber = 1;
                string baseLogFileFullPath = logFileFullPath;
                while (File.Exists(logFileFullPath))
                {
                    logFileFullPath = $"{baseLogFileFullPath}_{suffixNumber}";
                    suffixNumber++;
                }

                Log.Logger = new LoggerConfiguration().MinimumLevel.Information().
                    WriteTo.File(logFileFullPath + logFileExt).CreateLogger();

                Log.Information($"Techneaux DataMaster SmartSheet Log - {DateTime.Now}");
                Log.Information($"Current User = {Environment.UserName}");

                // File List var
                List<string> configFilePaths = new List<string>();

                if (cmdType == AutoCmdType.SingleFile)
                // Single config file mode
                {
                    // Determine if the file exists
                    Log.Information($@"- Single Config Mode with file path={cmdLineConfigPath}");

                    if (File.Exists(cmdLineConfigPath))
                    {
                        configFilePaths.Add(cmdLineConfigPath);
                    }
                    else
                    {
                        // Path is not valid
                        Log.Error("File does not exist or path is invalid. Exiting application.");
                        Environment.ExitCode = (int)AppExitCodes.AutoConfigPathInvalid;
                        return;
                    }
                }

                // Batch folder mode
                else if (cmdType == AutoCmdType.Folder)
                {
                    Log.Information($@"- Batch Folder Mode with folder path={cmdLineBatchFolder}");

                    if (!Directory.Exists(cmdLineBatchFolder))
                    {
                        Log.Error("Batch folder does not exist or is invalid. Exiting application");
                        Environment.ExitCode = (int)AppExitCodes.AutoBatchFolderInvalid;
                        return;
                    }

                    configFilePaths = Directory.GetFiles(cmdLineBatchFolder, "*.xml").ToList();

                    if (!configFilePaths.Any())
                    {
                        Log.Error("No config files found. Exiting application");
                        Environment.ExitCode = (int)AppExitCodes.NoConfigFilesInBatchFolder;
                        return;
                    }
                }
                else
                {
                    // Invalid command line parameters
                    Log.Error("No valid command line parameters were found. Exiting application.");
                }

                foreach (string configPath in configFilePaths)
                {
                    Log.Information($@" == {DateTime.Now} : Beginning analysis of config file: {configPath} ==");
                    //MainViewModel myViewModel;
                    if (configFileSvc.OpenConfigFilePath(configPath))
                    {
                        //myViewModel = new MainViewModel(configFileSvc.ThisConfigModel);
                    }
                    var currentConfig = configFileSvc.ThisConfigModel;
                    //myViewModel = new MainViewModel(configFileSvc.ThisConfigModel);
                    if (currentConfig == null)
                    {
                        Log.Error("Config file is not valid. Skipping this file.");
                        continue;
                    }
                    Log.Information("Config file is valid");
                    try
                    {
                        RollupPeriod _defaultRollupPeriod = new RollupPeriod(
                                                            DateTime.Now.Date.AddHours(8).AddDays(-1),
                                                            DateTime.Now.Date.AddHours(8));

                        var results = await WriteReportToSmartSheet.UploadReport(configFileSvc.ThisConfigModel.CygNetGeneral, configFileSvc.ThisConfigModel.SmartSheetConfigModel, _defaultRollupPeriod, CancellationToken.None);
                        //myViewModel.SmartSheetViewModel.WriteToSmartSheetError = results.Details;
                        Log.Warning($"Write to smartsheet success = {results.WasSuccessful} with result details: {results.Details}");
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, "General failure during automatic operation on file {file}", configPath);

                        throw;
                    }

                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Fatal error during automatic operation");
                
                //WindowsEventLogs.WriteEvent($"Techneaux Data Extractor - Unrecoverable error during automatic run [Ex: {ex.ToString()}]",
                //           System.Diagnostics.EventLogEntryType.Error);
                Environment.ExitCode = 3;
            }
            finally
            {

            }
        }

    }
}

