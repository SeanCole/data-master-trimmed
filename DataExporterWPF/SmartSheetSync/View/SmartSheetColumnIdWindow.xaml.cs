﻿using DataMasterClient.ViewModels;
using MahApps.Metro.Controls;
using System;
using System.Windows;
using System.Linq;
using System.Windows.Input;
using TechneauxWpfControls.DataConverters;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using System.ComponentModel;
using System.Collections.Generic;
using TechneauxReportingDataModel.SmartSheet.Enumerable;
using GenericRuleModel.Helper;

namespace DataMasterClient.SmartSheetSync.View
{
    /// <summary>
    /// Interaction logic for SmartSheetColumnIdWindow.xaml
    /// </summary>
    public partial class SmartSheetColumnIdWindow : MetroWindow
    {
        public static BindingList<SmartSheetReportElm> SheetIdList { get; set; }
        public static object StaticDataContext { get; set; }
        public SmartSheetColumnIdWindow()
        {
            InitializeComponent();
            FieldIdListView.MouseDoubleClick += FieldIdListView_MouseDoubleClick;
        }


        private void FieldIdListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!(FieldIdListView.SelectedItem is SmartSheetItemId selectedRowItem))
            {
                return;
            }
            if (SheetIdList.Any(x => x.SmartSheetColumn == selectedRowItem.Name))
            {
                System.Windows.MessageBox.Show("This Id is already in use, please choose another one");
                return;
            }

            ThisChooserResult.Value = selectedRowItem;
                
                //.Name;
            //ThisChooserResult.Description = selectedRowItem.Id;

            WasCancelled = false;

            Close();
        }

        private void OnCloseExecuted(object sender, ExecutedRoutedEventArgs e)
        {
            Hide();
        }

        public static SmartSheetColumnIdWindow Instance()
        {
            if (StaticDataContext == null)
            {
                var instanceTemp = new SmartSheetColumnIdWindow
                {
                    DataContext = StaticDataContext,
                    Owner = Application.Current.MainWindow
                };
                return instanceTemp;
            }

            var instance = new SmartSheetColumnIdWindow
            {
                DataContext = StaticDataContext,
                Owner = Application.Current.MainWindow
            };

            return instance;
        }

        public bool TryGetKeyDescId(out ValueDescription result)
        {
            ShowDialog();

            result = ThisChooserResult;
            return !WasCancelled;
        }

        public bool WasCancelled;
        public ValueDescription ThisChooserResult { get; private set; } = new ValueDescription();


        private void SelectButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedRowItem = FieldIdListView.SelectedItem as SmartSheetItemId;
            if (selectedRowItem == null)
            {
                return;
            }
            if (SheetIdList.Any(x => x.SmartSheetColumn == selectedRowItem.Name))
            {
                System.Windows.MessageBox.Show("This Id is already in use, please choose another one");
                return;
            }
            ThisChooserResult.Value = selectedRowItem;
            WasCancelled = false;
            Close();
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {

            WasCancelled = true;
            Close();
        }
    }
}


