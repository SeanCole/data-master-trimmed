﻿using System;
using System.Windows;
using System.Windows.Input;
using DataMasterClient.Forms;
using DataMasterClient.SmartSheetSync.View;
using DataMasterClient.ViewModels;
using DataMasterClient.ViewModels.CygNet.Sub;
using TechneauxReportingDataModel.CygNet.Rules;
using TechneauxReportingDataModel.SmartSheet.Enumerable;
using TechneauxReportingDataModel.SmartSheet.SubOptions;
using TechneauxWpfControls.DataConverters;
using XmlDataModelUtility;

namespace DataMasterClient.SmartSheetSync.ViewModels
{
    public interface ISmartSheetReportElmViewModel
    {
        ICommand OpenFieldIdWindow { get; }
        Visibility IsDataVisible { get; set; }
        Visibility IsFacilityTypeVisible { get; set; }
        Visibility IsPointAttributeSelectionVisible { get; set; }
        Visibility IsFixedTextVisible { get; set; }
        FmsOptionsViewModel FmsOptionsConfig { get; set; }
        AttributeViewModel AttributeView { get; set; }
        FormatViewModel FormatConfig { get; set; }
        Visibility IsAttributeTypeVisible { get; set; }
        Visibility IsHistoryTypeVisible { get; set; }
        Visibility IsNoteHistoryTypeVisible { get; set; }
        Visibility IsFacilityNoteSelectionVisible { get; set; }
        Visibility IsPointNoteSelectionVisible { get; set; }
        Visibility IsNoteValueTypeVisible { get; set; }
        Visibility IsUdcVisible { get; set; }
        Visibility IsChildOrdVisible { get; set; }
        Visibility IsChildGroupVisible { get; set; }
        Visibility IsHistoryOptionsVisible { get; set; }
        ICommand OpenChildGroupWindow { get; }
        PollingRetentionViewModel Polling { get; set; }
        PointHistoryViewModel PointHistory { get; set; }
        Visibility IsIdVisible { get; set; }
        Visibility IsFmsOptionsVisible { get; set; }
        Visibility IsNoteTypeVisible { get; set; }
        Visibility IsPointHistoryTypeVisible { get; set; }
        Visibility IsPointValueTypeVisible { get; set; }
     
        Visibility IsFacilityAttributeSelectionVisible { get; set; }
        Guid Id { get; }
    }

    public class SmartSheetReportElmViewModel : NotifyDynamicBase<SmartSheetReportElm>, ISmartSheetReportElmViewModel
    {

        public SmartSheetReportElmViewModel(SmartSheetReportElm srcModel) : base(srcModel)
        {
      
            FormatConfig = new FormatViewModel(srcModel.FormatConfig);
            AttributeView = new AttributeViewModel(srcModel.AttributeOptions);
            PointHistory = new PointHistoryViewModel(srcModel.PointHistoryOptions);
            //Polling = new PollingRetentionViewModel(srcModel.PollingOptions);
            FmsOptionsConfig = new FmsOptionsViewModel(srcModel.FmsPropertyOptions);
            IsUdcVisible = Visibility.Visible;

            IsChildOrdVisible = Visibility.Collapsed;
            IsFixedTextVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsChildGroupVisible = Visibility.Collapsed;
            IsNoteValueTypeVisible = Visibility.Collapsed;
            IsUdcVisible = Visibility.Visible;
            IsFacilityTypeVisible = Visibility.Visible;
            //IsChildOrdVisible = System.Windows.Visibility.Visible;
            IsFacilityAttributeSelectionVisible = Visibility.Visible;
            IsHistoryTypeVisible = Visibility.Visible;
            IsNoteHistoryTypeVisible = Visibility.Collapsed;
            IsNoteTypeVisible = Visibility.Collapsed;
            IsAttributeTypeVisible = Visibility.Visible;
            IsPointValueTypeVisible = Visibility.Visible;
            //IsChildGroupVisible = Visibility.Visible;
            IsNoteValueTypeVisible = Visibility.Collapsed;
            IsPointHistoryTypeVisible = Visibility.Collapsed;
            IsHistoryOptionsVisible = Visibility.Collapsed;
            IsPointAttributeSelectionVisible = Visibility.Visible;
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsIdVisible = Visibility.Visible;
            IsPointNoteSelectionVisible = Visibility.Collapsed;
            IsFmsOptionsVisible = Visibility.Collapsed;
            IsFacilityNoteSelectionVisible = Visibility.Collapsed;
            IsDataVisible = Visibility.Visible;
            IsPointNoteSelectionVisible = Visibility.Collapsed;
        }


        public ICommand OpenFieldIdWindow => new DelegateCommand(OpenSelectedFieldIdWindow);
        public void OpenSelectedFieldIdWindow(object inputObject)
        {
            var windowInstance = SmartSheetColumnIdWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (!success) return;
            if (result.Value != null)
            {
                MyDataModel.SmartSheetColumn = (result.Value as SmartSheetItemId).Name;
            }
        }
        public Visibility IsDataVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFacilityTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointAttributeSelectionVisible
        {
            get
            {
                SetPropertyValue(MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute
                    ? Visibility.Visible
                    : Visibility.Collapsed);

                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFixedTextVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        public FmsOptionsViewModel FmsOptionsConfig
        {
            get => GetPropertyValue<FmsOptionsViewModel>();
            set => SetPropertyValue(value);
        }

        public AttributeViewModel AttributeView
        {  
            get => GetPropertyValue<AttributeViewModel>();
            set => SetPropertyValue(value);
        }
        public FormatViewModel FormatConfig
        {
            get => GetPropertyValue<FormatViewModel>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsAttributeTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute ||
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsHistoryTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsNoteHistoryTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }


        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsFacilityNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsPointNoteSelectionVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsNoteValueTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsUdcVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
                set => SetPropertyValue(value);
        }



        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection))]
        public Visibility IsChildOrdVisible
        {
            get
            {
                if (MyDataModel.FacilityPointOptions.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }


        [AffectedByOtherPropertyChange(nameof(FacilityPointSelection))]
        public Visibility IsChildGroupVisible
        {
            get
            {
                if (MyDataModel.FacilityPointOptions.FacilityChoice.Equals(FacilitySelection.FacilityChoices.ChildFacility))
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

        [AffectedByOtherPropertyChange(nameof(CygNetRollupRuleBase.DataElement))]
        public Visibility IsHistoryOptionsVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

       
       
        public ICommand OpenChildGroupWindow => new DelegateCommand(OpenSelectChildGroupWindow);


        public void OpenSelectChildGroupWindow(object inputObject)
        {
            var windowInstance = CygNetChildFacilityWindow.Instance();

            var success = windowInstance.TryGetKeyDescId(out var result);
            if (success)
            {
                if (result.Value != null)
                {
                    MyDataModel.FacilityPointOptions.ChildGroupKey.Id = result.Value as string;

                }
                if (result.Description != null)
                {
                    MyDataModel.FacilityPointOptions.ChildGroupKey.Desc = result.Description;
                }
            }
        }

        public PollingRetentionViewModel Polling
        {
            get => GetPropertyValue<PollingRetentionViewModel>();
            set => SetPropertyValue(value);
        }



        public PointHistoryViewModel PointHistory
        {
            get => GetPropertyValue<PointHistoryViewModel>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsIdVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointAttribute ||
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFmsOptionsVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.FmsProperty)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }



        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsNoteTypeVisible
        {
            get => GetPropertyValue<Visibility>();
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointHistoryTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsPointValueTypeVisible
        {
            get
            {
                if (MyDataModel.DataElement == CygNetRuleBase.DataElements.PointHistory || MyDataModel.DataElement == CygNetRuleBase.DataElements.PointCurrentValue)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }
      

        [AffectedByOtherPropertyChange(nameof(CygNetRuleBase.DataElement))]
        public Visibility IsFacilityAttributeSelectionVisible
        {
            get
            {
                if (
                    MyDataModel.DataElement == CygNetRuleBase.DataElements.FacilityAttribute)
                {
                    SetPropertyValue(Visibility.Visible);
                }
                else
                {
                    SetPropertyValue(Visibility.Collapsed);
                }
                return GetPropertyValue<Visibility>();
            }
            set => SetPropertyValue(value);
        }

    }
}
