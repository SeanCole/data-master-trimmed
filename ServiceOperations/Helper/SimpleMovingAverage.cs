﻿using System.Collections.Generic;
using System.Linq;

namespace TechneauxDataSyncService.Helper
{
    public class SimpleMovingAverage
    {
        private List<double> _internalList = new List<double>();

        public double DefaultValue { get; private set; }
        public double MaxCount { get; set; }


        public SimpleMovingAverage(int numItems, double defaultValue)
        {
            DefaultValue = defaultValue;
            MaxCount = numItems;
        }

        public void AddNew(double newVal)
        {
            _internalList.Insert(0, newVal);

            _internalList = _internalList.Take(10).ToList();
        }

        public double AverageValue => _internalList.Average();
    }
}
