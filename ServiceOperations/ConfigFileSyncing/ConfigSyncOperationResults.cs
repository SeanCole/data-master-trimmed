﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Serilog.Core;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.ConfigFileSyncing
{
    public partial class SyncFileUtility
    {
        public class ConfigSyncOperationResults : NotifyModelBase
        {
            public ConfigSyncOperationResults(
                int totalPointCount)
            {
                OperationStopwatch.Start();

                NumTotalPoints = totalPointCount;

                StartTime = DateTime.Now;
            }

            private SemaphoreSlim recordResultsSem = new SemaphoreSlim(1, 1);


            private Stopwatch swUi = new Stopwatch();
            public void StartNewDataMaintenance()
            {
                NewDataStopwatch.Reset();
                NewDataStopwatch.Start();

                UpdateValues();

                swUi.Start();
            }

            private int _NumMaintPointsProcessed = 0;
            private int _NumMaintRowsInserted = 0;
            private int _NumMaintRowsDeleted = 0;
            public async Task RecordMaintenancePointProc(
                int numRowsIns,
                int numRowsDel)
            {
                try
                {
                    // await recordResultsSem.WaitAsync();

                    Interlocked.Increment(ref _NumMaintPointsProcessed);
                    Interlocked.Add(ref _NumMaintRowsInserted, numRowsIns);
                    Interlocked.Add(ref _NumMaintRowsDeleted, numRowsDel);

                    if (swUi.Elapsed.TotalSeconds > 1)
                    {
                        NumMaintPointsProcessed = _NumMaintPointsProcessed;
                        NumMaintRowsInserted = _NumMaintRowsInserted;
                        NumMaintRowsDeleted = _NumMaintRowsDeleted;

                        swUi.Restart();

                        if (numRowsIns > 0)
                            NumPointsNewData += 1;

                        UpdateValues();
                    }
                }
                catch(Exception ex)
                {
                    Log.Error(ex, "Exception while recording maintenance point proc.");
                }
                finally
                {
                    //recordResultsSem.Release();
                }
            }

            public void EndNewData()
            {
                NewDataStopwatch.Stop();

                UpdateValues();
            }

            public void StartBackfill(
                int numPointsInvolved)
            {
                NumPointsBackfillNeeded = numPointsInvolved;

                NewDataStopwatch.Stop();
                BackfillStopwatch.Start();
            }

            private HashSet<string> BackfillPointTags = new HashSet<string>();

            public async Task RecordBackfillOperation(
                string tag,
                int rowsInserted)
            {
                try
                {
                    await recordResultsSem.WaitAsync();

                    NumBackfillPasses += 1;

                    NumBackfillRowsInserted += rowsInserted;

                    BackfillPointTags.Add(tag);
                    NumBackfillPointsProcessed = BackfillPointTags.Count;
                }
                finally
                {
                    recordResultsSem.Release();
                }
            }

            public void EndBackfill()
            {
                BackfillStopwatch.Stop();

                UpdateValues();
            }


            public void EndOperation()
            {
                OperationStopwatch.Stop();
                EndTime = DateTime.Now;

                UpdateValues();
            }

            public Stopwatch BackfillStopwatch { get; } = new Stopwatch();

            public Stopwatch OperationStopwatch { get; } = new Stopwatch();

            public Stopwatch NewDataStopwatch { get; } = new Stopwatch();

            public int NumPointsNoNewData
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumPointsNewData
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumMaintRowsInserted
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumMaintRowsDeleted
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumPointsBackfillNeeded
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumBackfillPointsProcessed
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumBackfillRowsInserted
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumBackfillPasses
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumPointsBackfillCompleted
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public DateTime StartTime
            {
                get => GetPropertyValue<DateTime>();
                private set => SetPropertyValue(value);
            }

            public DateTime EndTime
            {
                get => GetPropertyValue<DateTime>();
                private set => SetPropertyValue(value);
            }

            public int NumTotalPoints
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public int NumMaintPointsProcessed
            {
                get => GetPropertyValue<int>();
                private set => SetPropertyValue(value);
            }

            public double NumMaintPointsProcessedPerSecond
            {
                get => GetPropertyValue<double>();
                private set => SetPropertyValue(value);
            }

            private void UpdateValues()
            {
                TotalDuration = OperationStopwatch.Elapsed;
                MaintenanceDuration = NewDataStopwatch.Elapsed;
                BackfillDuration = BackfillStopwatch.Elapsed;

                NumPointsNoNewData = NumTotalPoints - NumPointsNewData;

                if (NumTotalPoints > 0 && NumPointsNewData > 0)
                {
                    PercentOfPointsWithNewData = Math.Round(NumPointsNewData /
                                                        (double)NumTotalPoints * 100, 1);
                }

                if (NumPointsBackfillNeeded > 0 && NumBackfillPointsProcessed > 0)
                {
                    PercentBackfillPointsProcessed = Math.Round(NumBackfillPointsProcessed /
                                                        (double)NumPointsBackfillNeeded * 100, 1);
                }

                if (MaintenanceDuration.TotalMinutes > 0)
                {
                    NumMaintenanceRowsPerMinute = Math.Round((NumMaintRowsInserted + NumMaintRowsDeleted) / MaintenanceDuration.TotalMinutes);
                }

                if (BackfillDuration.TotalMinutes > 0)
                {
                    NumBackfillRowsPerMinute = Math.Round((NumBackfillRowsInserted) / BackfillDuration.TotalMinutes);
                }

                if (MaintenanceDuration.TotalSeconds > 0)
                    NumMaintPointsProcessedPerSecond = NumMaintPointsProcessed / MaintenanceDuration.TotalSeconds;
            }

            public double PercentOfPointsWithNewData
            {
                get => GetPropertyValue<double>();
                private set => SetPropertyValue(value);
            }

            public double NumMaintenanceRowsPerMinute
            {
                get => GetPropertyValue<double>();
                private set => SetPropertyValue(value);
            }

            public double PercentBackfillPointsProcessed
            {
                get => GetPropertyValue<double>();
                private set => SetPropertyValue(value);
            }

            public double NumBackfillRowsPerMinute
            {
                get => GetPropertyValue<double>();
                private set => SetPropertyValue(value);
            }

            public TimeSpan MaintenanceDuration
            {
                get => GetPropertyValue<TimeSpan>();
                private set => SetPropertyValue(value);
            }

            public TimeSpan BackfillDuration
            {
                get => GetPropertyValue<TimeSpan>();
                private set => SetPropertyValue(value);
            }

            public TimeSpan TotalDuration
            {
                get => GetPropertyValue<TimeSpan>();
                private set => SetPropertyValue(value);
            }
        }
    }
}
