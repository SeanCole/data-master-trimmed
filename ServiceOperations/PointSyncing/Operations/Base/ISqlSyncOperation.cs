﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CygNetRuleModel.Resolvers;
using CygNetRuleModel.Resolvers.History;
using Techneaux.CygNetWrapper.Services.VHS;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.SqlServerHistory;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public  interface ISqlSyncOperation
    {
        SimpleSqlSyncResults Results { get; } 

        Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress, 
            CancellationToken ct);

        ISqlSyncDetailedResults DetailedHistoryResults { get; }
    }
}
