﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public enum OperationTypes
    {
        FirstCheck,
        KeepCurrent,
        FastAudit,
        FullSync
    }
}
