﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechneauxDataSyncService.PointSyncing.Operations.Sub;
using TechneauxHistorySynchronization.Models;
using TechneauxReportingDataModel.Helper;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    class FullResync : TimePeriodAudit
    {
        public FullResync(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule newSrcPoint)
            : base(newSchema, newSrcPoint, true)
        {
            EarliestDate = DateTime.Now.AddDays(-RetentionDays);

            LatestDate = DateTime.Now;

            Results = new SimpleSqlSyncResults(nameof(FullResync));
        }
    }
}
