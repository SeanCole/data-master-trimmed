﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;
using TechneauxUtility;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryDeleteAll : TimedOperation, ISqlSyncOperation
    {
        public HistoryDeleteAll(
            SimpleCombinedTableSchema newSchema, 
            CachedCygNetPointWithRule srcPnt)
        {
            Schema = newSchema;

            if (Schema == null || Schema.IsEmptySqlSchema)
                throw new ArgumentNullException(nameof(newSchema), "Schema must not be null or empty");
            if (srcPnt == null)
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
                throw new ArgumentException("Src point rule must be non-null and valid");
            SrcPoint = srcPnt;
            
        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        #endregion

        
        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryDeleteAll));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();
        

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress, 
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();
                
                StartSubtask(nameof(sqlOp.DeleteSqlRows));

                // -- Get sql rows
                var latestSqlRows = await GetRecentSqlHistoryRows(sqlOp, ct);

                Results.NumTotalSqlRows = latestSqlRows?.Count;

                // Is there any sql data at all?
                if (!latestSqlRows.IsAny())
                {
                    // No history is a Pass condition
                    Results.OpResult = OpStatuses.Pass;
                    Results.MostRecentRawHistTimestamp = DateTime.Now.AddHours(-1);
                    Results.MostRecentNormTimestamp = DateTime.Now.AddHours(-1);
                    Results.StatusMessage = "No history to delete";
                    return;
                }
                
                Results.NumSqlRowsDeleted =
                    await sqlOp.DeleteSqlRows(SrcPoint, ct);
                //Results.NumSqlRowsDeleted = await SqlRowResolver.DeleteExpiredSqlForPoint(Schema, SrcPoint, SrcPoint.SrcRule.PollingOptions.RetentionDays,ct);
            
                ct.ThrowIfCancellationRequested();
                Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.DeleteSqlRows)).ToString());
                Results.LogAuditMessage($"Sql all rows delete finished with {Results.NumSqlRowsDeleted} rows deletd.");
      
                Results.StatusMessage = "Finished all history delete.";
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - All History Delete";
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }

        private async Task<List<SqlHistoryRow>> GetRecentSqlHistoryRows(
           SqlOperation sqlOp,
           CancellationToken ct)
        {
            StartSubtask(nameof(sqlOp.GetLatestXRowsFromDb));

            // Collect recent SQL history
            var latestSqlRows = await sqlOp.GetLatestXRowsFromDb(SrcPoint, ct);

            Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.GetLatestXRowsFromDb)).ToString());
            Results.NumTotalSqlRows = latestSqlRows.Count;

            return latestSqlRows;
        }
    }
}
