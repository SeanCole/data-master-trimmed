﻿using Serilog.Context;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TechneauxDataSyncService.PointSyncing.Operations;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;

namespace TechneauxDataSyncService.PointSyncing
{
    public partial class PointSync
    {
        public bool NeedsBackfill
        {
            get => GetPropertyValue<bool>();
            private set => SetPropertyValue(value);
        }

        public double FillPercent
        {
            get => GetPropertyValue<double>();
            private set => SetPropertyValue(value);
        }

        private bool AllSqlIsDeleted { get; set; } = false;

        private async Task<(bool continueProcessingNextState, int numRowsInserted, int numRowsDeleted)>
            ExecuteSingleOperation(
                SqlOperation sqlOp,
                SimpleCombinedTableSchema tableSchema,
                CancellationToken ct)
        {
            ISqlSyncOperation thisOp = null;

            _log.Debug("Starting sync operation with state machine");

            try
            {
                using (LogContext.PushProperty("Sync State", SyncState.ToString()))
                {

                    // -- Check state
                    switch (SyncState)
                    {
                        case SyncStates.HistoryCheck:

                            // Run a hist check op
                            thisOp = new HistoryCheck(SrcPoint);
                            var histCheckOp = (HistoryCheck)thisOp;

                            await histCheckOp.DoOperationAsync(sqlOp, null, ct);

                            switch (histCheckOp.Results.OpResult)
                            {
                                // Pass means history was found in the retention period
                                case OpStatuses.Pass:
                                    // If history was found, analyze what is available in SQL with next state
                                    SyncState = SyncStates.InitialSetup;

                                    // Now that we are moving forward into setup and maintenance, we assume there could be sql data
                                    AllSqlIsDeleted = false;

                                    // Move to next state
                                    return (
                                        continueProcessingNextState: true,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // Fail means no history was found in retention period in CygNet, so delete all SQL history
                                case OpStatuses.Fail:

                                    if (AllSqlIsDeleted)
                                    {
                                        // If a full deletion was already done, and no history has been found since, just go back to hist check
                                        SyncState = SyncStates.HistoryCheck;
                                        return (
                                            continueProcessingNextState: false,
                                            numRowsInserted: 0,
                                            numRowsDeleted: 0);
                                    }
                                    else
                                    {
                                        // Delete history next state
                                        SyncState = SyncStates.DeleteAllSQLHistory;
                                        return (
                                            continueProcessingNextState: true,
                                            numRowsInserted: 0,
                                            numRowsDeleted: 0);
                                    }

                                // Exception means we don't know if history or not, because the operation had an exception we couldn't ignore
                                case OpStatuses.Exception:
                                    // Try another history check next time, since state is unknown, we can't proceed any other way
                                    SyncState = SyncStates.HistoryCheck;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                        //case SyncStates.EarlyGapCheck:
                        //    // Do first check
                        //    thisOp = new EarlyGapCheck(tableSchema, SrcPoint);
                        //    var earlyGapCheckOp = (EarlyGapCheck)thisOp;

                        //    await earlyGapCheckOp.DoOperationAsync(sqlOp, null, ct);

                        //    switch (earlyGapCheckOp.Results.OpResult)
                        //    {
                        //        // Pass means no backfill is needed
                        //        case OpStatuses.Pass:
                        //            // If no rows were found, must not be any sql history at all
                        //            if (earlyGapCheckOp.Results.NumMatchingSqlRows == 0)
                        //            {
                        //                SyncState = SyncStates.MaintenanceSync;
                        //                LastCygNetTimestamp = earlyGapCheckOp.Results.MostRecentRawHistTimestamp;
                        //                LastNormalizedTimestamp = earlyGapCheckOp.Results.MostRecentRawHistTimestamp;
                        //                NeedsBackfill = true;
                        //                return (true, 0, 0);
                        //            }
                        //            else
                        //            {
                        //                SyncState = SyncStates.InitialSetup;
                        //                NeedsBackfill = false;
                        //                return (true, 0, 0);
                        //            }

                        //        case OpStatuses.Fail:
                        //            NeedsBackfill = true;
                        //            SyncState = SyncStates.InitialSetup;
                        //            return (true, 0, 0);

                        //        // Unknown exception, result not known. Assume no backfill is needed for now.
                        //        case OpStatuses.Exception:
                        //            //NextOperationScheduledTime = DateTime.Now.AddHours(.25);
                        //            return (false, 0, 0);
                        //        default:
                        //            throw new ArgumentOutOfRangeException();
                        //    }

                        case SyncStates.InitialSetup:
                            // Do first check
                            thisOp = new InitialSetup(tableSchema, SrcPoint, FirstCheckRowCount);
                            var firstCheckOp = (InitialSetup)thisOp;

                            await firstCheckOp.DoOperationAsync(sqlOp, null, ct);

                            switch (firstCheckOp.Results.OpResult)
                            {
                                // Pass means either no data in SQL at all, or it matches CygNet
                                //  Maintenance sync can continue, but maintenance processing timestamps need to be properly set
                                case OpStatuses.Pass:
                                    LastCygNetTimestamp = firstCheckOp.Results.MostRecentRawHistTimestamp;
                                    LastNormalizedTimestamp = firstCheckOp.Results.MostRecentNormTimestamp;

                                    CachedColVals = firstCheckOp.DetailedHistoryResults.FixedCygNetValues;

                                    SyncState = SyncStates.MaintenanceSync;
                                   
                                    // Set backfill status (determined by the operation)
                                    NeedsBackfill = firstCheckOp.IsBackfillNeeded;

                                    if (NeedsBackfill)
                                    {
                                        EarliestCygNetRawBackfillTimestamp = firstCheckOp.EarliestCommonBackfillTime;
                                        EarliestCygNetNormalizedBackfillTimestamp = firstCheckOp.EarliestCommonBackfillTime;
                                    }

                                    return (
                                        continueProcessingNextState: true,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // Fail means there was data and it isn't valid, so delete all history next
                                case OpStatuses.Fail:
                                    SyncState = SyncStates.DeleteAllSQLHistory;
                                  
                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // We can't just continue forward if the state of SQL is not known, nor is there much point in 
                                //   restarting the state machine, just retry
                                case OpStatuses.Exception:
                                    SyncState = SyncStates.InitialSetup;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                        case SyncStates.Backfill:
                            thisOp = new MaintenanceBackfill(
                                tableSchema,
                                SrcPoint,
                                CachedColVals,
                                EarliestCygNetRawBackfillTimestamp,
                                EarliestCygNetNormalizedBackfillTimestamp);

                            var backfillOp = (MaintenanceBackfill)thisOp;

                            await backfillOp.DoOperationAsync(sqlOp, null, ct);

                            switch (backfillOp.Results.OpResult)
                            {
                                // Pass means history was either inserted in SQL, or no values were found to insert, either way,
                                //   move to next earliest backfill period
                                case OpStatuses.Pass:
                                    SyncState = SyncStates.MaintenanceSync;

                                    EarliestCygNetRawBackfillTimestamp = backfillOp.Results.EarliestRawHistTimestamp;
                                    EarliestCygNetNormalizedBackfillTimestamp = backfillOp.Results.EarliestNormTimestamp;

                                    NeedsBackfill = backfillOp.IsBackfillStillNeeded;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: backfillOp.Results.NumSqlRowsInserted ?? 0,
                                        numRowsDeleted: backfillOp.Results.NumSqlRowsDeleted ?? 0);

                                // Fail means that fixed values (non primary keys) were found to be changed, invalidating all SQL data
                                case OpStatuses.Fail:
                                    // Delete and start fresh.
                                    SyncState = SyncStates.DeleteAllSQLHistory;

                                    return (
                                        continueProcessingNextState: true,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // If backfill hits exception, there are a few possibilities:
                                //   1) No history made it to SQL => a retry is safe
                                //   2) Some, not all, history made it into SQL => retry will lead to key collisions on insert, but history is incomplete
                                //         so we either move on to earlier timeframe and ignore the gap, do a compare (too expensive), or delete history
                                //         in the attempted range and retry
                                //   3) All the history made it in => similar to (2), but how do we know which happened, partial or full?
                                //  
                                // For now, until better code is written, we will do a retry
                                case OpStatuses.Exception:
                                    SyncState = SyncStates.MaintenanceSync;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                        case SyncStates.MaintenanceSync:
                            thisOp = new MaintenanceSync(tableSchema, SrcPoint, CachedColVals,
                                    LastCygNetTimestamp,
                                    LastNormalizedTimestamp);
                            var maintSyncOp = (MaintenanceSync)thisOp;

                            await maintSyncOp.DoOperationAsync(sqlOp, null, ct);

                            switch (maintSyncOp.Results.OpResult)
                            {
                                // New data, if available, was successfully inserted into SQL, continue with new maintenance
                                case OpStatuses.Pass:
                                    LastCygNetTimestamp = maintSyncOp.Results.MostRecentRawHistTimestamp;
                                    LastNormalizedTimestamp = maintSyncOp.Results.MostRecentNormTimestamp;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: maintSyncOp.Results.NumSqlRowsInserted ?? 0,
                                        numRowsDeleted: maintSyncOp.Results.NumSqlRowsDeleted ?? 0);

                                // Fail means fixed values (non-primary keys) were found to have changed, invalidating all SQL data
                                case OpStatuses.Fail:
                                    SyncState = SyncStates.DeleteAllSQLHistory;
                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // An exception leaves the state unclear, 3 Options are possible:
                                //    1) It was temporary fluke and a retry will succeed
                                //    2) It was a critical error in code or data, and it will simply recur on the next attempt
                                //    3) It was a context based exception, and deleting SQL history and starting fresh may lead to a new context that works
                                // I think (1) or (2) is by far the likeliest, with the hope that it's just (1), since (2) is un-fixable
                                //   Hence, attempt maintenance again. In future, may wish to limit retries and then eventually reset.
                                case OpStatuses.Exception:
                                    SyncState = SyncStates.MaintenanceSync;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                        case SyncStates.FastAudit:
                            thisOp = new FastAudit(tableSchema, SrcPoint);
                            var fastAuditOp = (FastAudit)thisOp;

                            await fastAuditOp.DoOperationAsync(sqlOp, null, ct);

                            switch (fastAuditOp.Results.OpResult)
                            {
                                // Pass means no problems were found, return to maintenance
                                case OpStatuses.Pass:
                                    SyncState = SyncStates.MaintenanceSync;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // Fail means an inner gap was found, or old data failed to match in values or resolution
                                case OpStatuses.Fail:
                                    // If any SQL history is found to be invalid, delete all and start fresh
                                    SyncState = SyncStates.DeleteAllSQLHistory;

                                    return (
                                        continueProcessingNextState: true,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                // Exception gives an uncertain result, but doesn't justify further action, return to maintenance
                                case OpStatuses.Exception:

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                default:
                                    throw new ArgumentOutOfRangeException();
                            }


                        case SyncStates.DeleteAllSQLHistory:
                            thisOp = new HistoryDeleteAll(tableSchema, SrcPoint);
                            var historyDeleteAllOp = (HistoryDeleteAll)thisOp;
                            await historyDeleteAllOp.DoOperationAsync(sqlOp, null, ct);

                            switch (historyDeleteAllOp.Results.OpResult)
                            {
                                // Pass means delete all succeeded, return history check
                                case OpStatuses.Pass:
                                    SyncState = SyncStates.HistoryCheck;
                                    AllSqlIsDeleted = true;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: historyDeleteAllOp.Results.NumSqlRowsDeleted ?? 0);

                                // Fail is undetermined for this operation at present, treat as exception
                                case OpStatuses.Fail:

                                // Exception implies deleting history failed, we can re-attempt state machine from scratch
                                //   because we will reach this same point in short order if bad history remains
                                case OpStatuses.Exception:
                                    SyncState = SyncStates.HistoryCheck;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                        case SyncStates.DeleteOldHistory:
                            thisOp = new HistoryDeleteOld(tableSchema, SrcPoint);
                            var deleteHistOldOp = (HistoryDeleteOld)thisOp;
                            await deleteHistOldOp.DoOperationAsync(sqlOp, null, ct);

                            switch (deleteHistOldOp.Results.OpResult)
                            {
                                // Pass means delete succeeded, return to maintenance
                                case OpStatuses.Pass:
                                    SyncState = SyncStates.MaintenanceSync;

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: deleteHistOldOp.Results.NumSqlRowsDeleted ?? 0);

                                // Fail is undetermined for this operation at present, treat as exception
                                case OpStatuses.Fail:

                                // Exception implies deleting old history failed, but doesn't justify further action, return to maintenance
                                case OpStatuses.Exception:

                                    return (
                                        continueProcessingNextState: false,
                                        numRowsInserted: 0,
                                        numRowsDeleted: 0);

                                default:
                                    throw new ArgumentOutOfRangeException();
                            }

                        default:
                            throw new ArgumentOutOfRangeException(nameof(SyncState), "State unknown");
                    }
                }
            }

            catch (OperationCanceledException)
            {
                return (
                    continueProcessingNextState: false,
                    numRowsInserted: 0,
                    numRowsDeleted: 0);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _log.Error(e, $"General exception during point sync for [{SrcPoint.Tag.GetTagPointIdFull()}] ");

                return (
                    continueProcessingNextState: false,
                    numRowsInserted: 0,
                    numRowsDeleted: 0);
            }
            finally
            {
                FillPercent = !NeedsBackfill || EarliestCygNetRawBackfillTimestamp < DateTime.Now.AddDays(-RetentionDays) ?
                    100 :
                    Math.Round((DateTime.Now - EarliestCygNetRawBackfillTimestamp).TotalDays / RetentionDays * 100, 1);

                if (thisOp == null)
                    throw new InvalidOperationException("Op var must not be null");

                if (ServiceOps.LogAllOpResults)
                {
                    OpsRecords.Add(thisOp.Results);
                    OpsRecordsHarness = OpsRecords.ToList();
                }

                if (thisOp.Results.OpResult == OpStatuses.Exception)
                {
                    _log.Error(thisOp.Results.FailedException,
                        $"Operation type [{thisOp.GetType()}] failed for point [{SrcPoint.Tag.GetTagPointIdFull()}] with results [{thisOp.Results}]");
                }
                else
                {
                    _log.Debug(
                        $"Operation type [{thisOp.GetType()}] ran for point [{SrcPoint.Tag.GetTagPointIdFull()}] with results [{thisOp.Results}]");
                }

                if (SyncState == SyncStates.MaintenanceSync && DateTime.Now > _nextFastAudit)
                {
                    //SyncState = SyncStates.FastAudit;
                    _nextFastAudit = _nextFastAudit.AddDays(1);
                }
                else if (SyncState == SyncStates.MaintenanceSync && DateTime.Now > _nextHistoryDelete)
                {
                    SyncState = SyncStates.DeleteOldHistory;
                    _nextHistoryDelete = _nextHistoryDelete.AddDays(1);
                }
            }
        }
    }
}
