﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TechneauxDataSyncService
{
    public static class ServiceOps
    {
        public static bool LogAllOpResults { get; set; } = false;

        public static double DeleteTimeout { get; set; } = 300;

        public static double InsertTimeout { get; set; } = 300;

        public static double SelectTimeout { get; set; } = 900;

        public static bool RunningFromHarness { get; set; } = false;

        public static double CriticalTimestampTimeoutMins { get; set; } = 30;

        public static bool LogPointSetOperationStatistics { get; set; } = false;

        public static int MaxRowsPerBackfillOperation { get; set; } = 105000;                
    }


    public static class HarnessForcedTestingOptions
    {
        private const bool IsDeleteAllTestingEnabled = false;
        private const bool PerformDeleteAllTests = false;
        private const int MinsDelayOnFirstDeleteAll = 0;
        private const int NumPointsDeleteAll = 2000;
        private const int HoursBetweenTests = 2;

        private static DateTime? LastDeleteAllDate = null;
        private static DateTime ClassInstanceTime = DateTime.Now;
        private static int NumPointsThisDeleteAll = 0;
        private static bool IsInCurrentDeleteAllTest { get; set; } = false;

        public static bool IsDeleteAllTestForced
        {
            get
            {
                if(IsDeleteAllTestingEnabled)
                {
                    if (IsInCurrentDeleteAllTest)
                    {
                        if (NumPointsThisDeleteAll >= NumPointsDeleteAll)
                        {
                            IsInCurrentDeleteAllTest = false;
                            LastDeleteAllDate = DateTime.Now;
                            return false;
                        }

                       Interlocked.Increment(ref NumPointsThisDeleteAll);
                        return true;
                    }

                    if (LastDeleteAllDate == null)
                    {
                        if((DateTime.Now - ClassInstanceTime).TotalMinutes > MinsDelayOnFirstDeleteAll)
                        {
                            IsInCurrentDeleteAllTest = true;
                            NumPointsThisDeleteAll += 1;
                            return true;
                        }
                    }
                    else
                    {
                        if ((DateTime.Now - LastDeleteAllDate.Value).TotalHours > HoursBetweenTests)
                        {
                            IsInCurrentDeleteAllTest = true;
                            NumPointsThisDeleteAll += 1;
                            return true;
                        }
                    }
                }

                return false;
            }
        }
    }
}
