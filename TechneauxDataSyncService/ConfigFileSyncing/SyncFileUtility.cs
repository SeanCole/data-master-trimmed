﻿using CygNetRuleModel.Resolvers;
using ReactiveUI;
using Serilog;
using Serilog.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Serilog.Context;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxHistorySynchronization.SqlServerHistory.DatabaseOperations;
using TechneauxReportingDataModel.General;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.ConfigFileSyncing
{
    public partial class SyncFileUtility : NotifyModelBase, ICancellable
    {

        public BindingList<ConfigSyncOperationResults> PointSyncResults { get; set; } =
            new BindingList<ConfigSyncOperationResults>();

        public OperationChartViewModel ChartViewModel { get; }

        private const int MinsBetweenPointCheck = 10;
        private double _minsBetweenPointSync = 5; // Comes from App.Config with default of 5 minutes (SEAN), no less than 30 secs, no more than 1 hour.
        private double _minsBackfillGuaranteed = 0; //Comes from app.config with default of 0  (SEAN), no less than 30 seconds no more than 1 hour. 
        private double _sqlDeleteTimeoutSeconds = 300; // app config
        private double _sqlInsertTimeoutSeconds = 300; // app config
        private double _sqlSelectTimeoutSeconds = 900; // app config
        private double _criticalTimestampTimeoutSeconds = 60; // app config

        public ReportConfigModel SrcDataModel
        { get; set; }
        //{
        //    get => GetPropertyValue<ReportConfigModel>();
        //    set => SetPropertyValue(value);
        //}

        [XmlIgnore()]
        public string FilePath
        {
            get => GetPropertyValue<string>();
            private set
            {
                FileName = Path.GetFileName(value);
                SetPropertyValue(value);
            }
        }

        private int TestOpCount = 0;

        [XmlIgnore()]
        public string FileName
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public string Status
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public string CurrentActivity
        {
            get => GetPropertyValue<string>();
            private set => SetPropertyValue(value);
        }

        private bool UsingTestHarness = false;

        public DateTime LastModifiedTimestamp
        {
            get => GetPropertyValue<DateTime>();
            set => SetPropertyValue(value);
        }

        private int MaxPoints { get; set; } = -1;


        public SyncFileUtility()
        {
            var sAttr = ConfigurationManager.AppSettings.Get("MaxPoints");

            if (int.TryParse(sAttr, out int num))
            {
                MaxPoints = num;
            }
        }

        private CancellationTokenSource Cts { get; }
        private ILogger _configFileLog;

        //private IDisposable _checkForNewPointsTimer;
        //private IDisposable _checkForNewPointDataTimer;

        public SyncFileUtility(
            string newFileName,
            ReportConfigModel newConfigModel,
            DateTime lastModDate,
            CancellationToken parentCt,
            bool usingTestHarness = false)
        {
            FilePath = newFileName;
            SrcDataModel = newConfigModel;
            LastModifiedTimestamp = lastModDate;
            Cts = CancellationTokenSource.CreateLinkedTokenSource(parentCt);

            InitLogging();
            UsingTestHarness = usingTestHarness;
            _configFileLog.Information("================================================");
            _configFileLog.Information($"Initializing new sync file with path=[{FilePath}]");

            LoadSyncSettings();
            if (usingTestHarness)
            {
                ChartViewModel = new OperationChartViewModel();
            }

            _configFileLog.Information($"Binding timer events: Point check every [{MinsBetweenPointCheck}] mins, Point sync every [{_minsBetweenPointSync}] mins");

            MyPoints = new List<PointSync>();

            //if (usingTestHarness)
            //{
            Observable.FromAsync(UpdatePointList)
                .RepeatAfterDelay(TimeSpan.FromMinutes(MinsBetweenPointCheck), RxApp.MainThreadScheduler)
                .Subscribe();

            //Observable.FromAsync(CheckForNewPointData)
            //    .RepeatAfterDelay(TimeSpan.FromMinutes(_minsBetweenPointSync), RxApp.MainThreadScheduler)
            //    .Subscribe();

            var CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromSeconds(2), RxApp.MainThreadScheduler)
                .Where(_ => !IsBusy && (DateTime.Now - LastSyncStartTimestamp).TotalMinutes > _minsBetweenPointSync)
                //.ObserveOnDispatcher()
                          .Subscribe(evt =>
                          {
                              CheckForNewPointData();
                          });

            //  CheckForNewPointsTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointCheck))
            //.ObserveOnDispatcher()
            //.Subscribe(evt =>
            //{
            //    UpdatePointList();
            //});

            //CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointSync))
            //                .ObserveOnDispatcher()
            //              .Subscribe(evt =>
            //              {
            //                  CheckForNewPointData();
            //              });
            //}
            //else
            //{
            //    CheckForNewPointsTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointCheck))
            //  .Subscribe(evt =>
            //  {
            //      UpdatePointList();
            //  });

            //    CheckForNewPointDataTimer = Observable.Interval(TimeSpan.FromMinutes(MinsBetweenPointSync))
            //                  .Subscribe(evt =>
            //                  {
            //                      CheckForNewPointData();
            //                  });
            //}


        }

        private void InitLogging()
        {
            var loggingLevelAttr = ConfigurationManager.AppSettings.Get("LogLevelConfig");
            Serilog.Events.LogEventLevel newLevel =
                (Serilog.Events.LogEventLevel)Enum.Parse(typeof(Serilog.Events.LogEventLevel), loggingLevelAttr);

            var logLevelSwitch = new LoggingLevelSwitch(newLevel);

            _configFileLog = new LoggerConfiguration()
                .MinimumLevel.ControlledBy(logLevelSwitch)
                .Enrich.FromLogContext()
                .WriteTo
                .File($@"{AppDomain.CurrentDomain.BaseDirectory}\Logs\Config Log [{Path.GetFileNameWithoutExtension(FilePath)}] {
                            DateTime.Now:MM-dd-yyyy hh-mm-ss}.txt")//, 
                                                                   //shared: true,
                                                                   //buffered: true)
                .CreateLogger();
        }

        private void LoadSyncSettings()
        {
            //CurrentValueService.MaxCVRefreshRate = Convert.ToDouble(ConfigurationManager.AppSettings.Get("MaxCVRefreshRate"));
            //if (CurrentValueService.MaxCVRefreshRate > 60)
            //    CurrentValueService.MaxCVRefreshRate = 60;
            //if (CurrentValueService.MaxCVRefreshRate < 1)
            //{
            //    CurrentValueService.MaxCVRefreshRate = 1;
            //}
            string MinsBetweenPointSyncError;
            string MinsBackfillGuaranteedError;
            string MinsDeleteSecondsError;
            string MinsSelectSecondsError;
            string MinsInsertSecondsError;
            string CriticalTimestampTimeoutSecondsError;
            
            if(AppSettingsHelper.TryGetDoubleFromAppSettings("MinsBetweenPointSync", 1, 60, 5, out _minsBetweenPointSync, out MinsBetweenPointSyncError))
            {

            }
            else
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("Error getting config item: " + MinsBetweenPointSyncError);
            }
            if(AppSettingsHelper.TryGetDoubleFromAppSettings("ExtendedBackfillMins", 0, 60, 0, out _minsBackfillGuaranteed, out MinsBackfillGuaranteedError))
            {
                
            }            
            else
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("Error getting config item: " + MinsBackfillGuaranteedError);                
            }
            if(AppSettingsHelper.TryGetDoubleFromAppSettings("SqlDeleteTimeoutSeconds", 300, 10000, 300, out _sqlDeleteTimeoutSeconds, out MinsDeleteSecondsError))
            {
                ServiceOps.DeleteTimeout = _sqlDeleteTimeoutSeconds;
            }
            else
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("Error getting config item: " + MinsDeleteSecondsError);
            }
            if(AppSettingsHelper.TryGetDoubleFromAppSettings("SqlInsertTimeoutSeconds", 300, 10000, 300, out _sqlInsertTimeoutSeconds, out MinsInsertSecondsError))
            {
                ServiceOps.InsertTimeout = _sqlInsertTimeoutSeconds;
            }
            else
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("Error getting config item: " + MinsInsertSecondsError);
            }
            if (AppSettingsHelper.TryGetDoubleFromAppSettings("SqlSelectTimeoutSeconds", 900, 10000, 900, out _sqlSelectTimeoutSeconds, out MinsSelectSecondsError))
            {
                ServiceOps.SelectTimeout = _sqlSelectTimeoutSeconds;
            }
            else
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("Error getting config item: " + MinsSelectSecondsError);
            }
            if(AppSettingsHelper.TryGetDoubleFromAppSettings("CriticalTimestampTimeoutMinutes", 1, 10000, 60, out _criticalTimestampTimeoutSeconds, out CriticalTimestampTimeoutSecondsError))
            {
                ServiceOps.CriticalTimestampTimeoutMins = _criticalTimestampTimeoutSeconds;
            }
            else
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error("Error getting config item: " + CriticalTimestampTimeoutSecondsError);
            }
            CurrentValueService.MaxCVRefreshRate = _minsBetweenPointSync - .25;
        
        }

        public void CancelAll()
        {
            _configFileLog.Debug("Cancel all.");

            //_checkForNewPointsTimer.Dispose();
            //_checkForNewPointDataTimer.Dispose();

            Cts.Cancel();
        }

        private readonly SemaphoreSlim _lockPointCheck = new SemaphoreSlim(1, 1);

        private readonly SemaphoreSlim _schemaLock = new SemaphoreSlim(1, 1);
        private SimpleCombinedTableSchema _mySchema;
        private DateTime _lastSchemaRefreshTime;

        private async Task<SimpleCombinedTableSchema> GetTableSchema()
        {
            try
            {
                await _schemaLock.WaitAsync();

                _configFileLog.Debug($"Trying to read table schmema");

                if (_mySchema == null || (DateTime.Now - _lastSchemaRefreshTime).TotalMinutes > 15)
                {
                    _configFileLog.Debug("Schema needed to be refreshed, reading from table");

                    _mySchema = await Task.Run(() => SqlServerConnectionUtility.GetTableSchema(
                            SrcDataModel.SqlConfigModel.SqlGeneralOpts,
                            SrcDataModel.SqlConfigModel.TableMappingRules.ToList(),
                            Cts.Token),
                        Cts.Token);

                    _lastSchemaRefreshTime = DateTime.Now;
                }

                return _mySchema;
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, "Failed to get schema with exception:");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Unhandled exception getting schema. Sync will not proceed.");

                return null;
            }
            finally
            {
                _schemaLock.Release();
            }
        }

        public int NumberOfPointUpdates { get; private set; }

        private async Task<(bool isValid, SimpleCombinedTableSchema schema)> GetValidatedSchema()
        {
            CurrentActivity = "Getting table schema";

            var thisSchema = await GetTableSchema();
            Status = "Schema Valid";

            if (thisSchema != null && CygNetSqlDataCompareModel.ValidateRulesDataOnly(thisSchema).IsValid)
            {
                Status = "Config valid";
                return (true, thisSchema);
            }
            else
            {
                Status = "Config is NOT valid";
                return (false, null);
            }
        }

        public async Task UpdatePointList()
        {
            var swPoint = new Stopwatch();

            try
            {
                await _lockPointCheck.WaitAsync();

                // Start timer
                swPoint.Start();

                // Record num point checks
                _configFileLog.Debug($"Cancellation at start of updatepointlist is {Cts.IsCancellationRequested}");
                NumberOfPointUpdates += 1;
                _configFileLog.Information("Starting point update check #[{NumberOfPointUpdates}]", NumberOfPointUpdates);

                // Check if schema is valid
                var (configIsValid, thisSchema) = await GetValidatedSchema();
                if (!configIsValid)
                {
                    _configFileLog.Warning("Failed Validation in UpdatePointsList, removing all pointsync objects");
                    Status = "Failed Validation";

                    // Validation failed, cancel all point ops
                    foreach (var pnt in MyPoints)
                    {
                        pnt?.CancelAll();
                    }

                    MyPoints.Clear();
                }

                _configFileLog.Debug($@"Starting UpdatePointList");
                CurrentActivity = "Updating PointsList";

                var pointsFound = await FetchCygNetPoints();

                var pointsFoundSet = new HashSet<CachedCygNetPointWithRule>(pointsFound);



                Cts.Token.ThrowIfCancellationRequested();
                _configFileLog.Debug("Cancellation");

                var newPointSyncObjs = new List<PointSync>();

                if (pointsFoundSet.IsAny())
                {
                    _configFileLog.Debug("Point search successful with [{FoundCount}] points found",
                        pointsFoundSet.Count);

                    HashSet<ICachedCygNetPoint> existingPoints =
                        new HashSet<ICachedCygNetPoint>(MyPoints.Select(ps => ps.SrcPoint));

                    var newPoints = pointsFoundSet.Where(pt => !existingPoints.Contains(pt)).ToList();

                    foreach (var newPoint in newPoints)
                    {
                        var newPointSync = new PointSync(this, newPoint, _configFileLog, Cts.Token);
                        newPointSyncObjs.Add(newPointSync);
                        MyPoints.Add(newPointSync);
                    }

                    var pointsNoLongerAvailable = MyPoints.Where(ps => !pointsFoundSet.Contains(ps.SrcPoint)).ToList();

                    if (pointsNoLongerAvailable.Any())
                    {
                        _configFileLog.Warning(
                            "Removing [{UnavailablePointCount}] points which are no longer available",
                            pointsNoLongerAvailable.Count);

                        foreach (var pnt in pointsNoLongerAvailable)
                        {
                            pnt?.CancelAll();
                            MyPoints.Remove(pnt);
                        }
                    }

                    //if (newPointSyncObjs.Any())
                    //{
                    //    await CheckForNewPointData();
                    //}
                }
                else
                {
                    _configFileLog.Warning(
                        $"No points were found, clearing all existing points and stopping all syncs");

                    MyPoints.ToList().ForEach(pnt => pnt?.CancelAll());

                    MyPoints.Clear();
                }

                Status = "Point check completed";

            }
            catch (OperationCanceledException cx)
            {
                _configFileLog.Error(cx, $"Cancellation during point check.");
                Status = "Cancellation during point check";

                //using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                //    Log.Error(ex, "Unhandled exception during new/updated point check for config {cfg})", FileName);
            }
            catch (Exception ex)
            {
                _configFileLog.Error(ex, $"Exception during point check");
                Status = "Exception during point check";

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Unhandled exception during new/updated point check for config {cfg})", FileName);
            }
            finally
            {
                CurrentActivity = "";

                SqlConnection.ClearAllPools();
                //OdbcConnection.ReleaseObjectPool();
                GC.Collect();
                GC.WaitForPendingFinalizers();

                _configFileLog.Debug("Finished Point update check #[{NumberOfPointUpdates}] with duration [{dur}]", _numNewDataCheck, swPoint.Elapsed);

                _lockPointCheck.Release();
            }
        }

        private async Task<List<PointSync>> GetCurrentPoints(List<CachedCygNetPointWithRule> existingPointsToExclude)
        {
            var pointsFound = await FetchCygNetPoints();
            var pointsFoundSet = new HashSet<CachedCygNetPointWithRule>(pointsFound);

            Cts.Token.ThrowIfCancellationRequested();
            _configFileLog.Debug("Cancellation");

            if (pointsFoundSet.IsAny())
            {
                _configFileLog.Debug("Point search successful with [{FoundCount}] points found",
                    pointsFoundSet.Count);

                //var existingPoints =
                //    new HashSet<ICachedCygNetPoint>(MyPoints.Select(ps => ps.SrcPoint));

                var newPoints = pointsFoundSet   //.Where(pt => !existingPoints.Contains(pt))
                    .Select(pnt => new PointSync(this, pnt, _configFileLog, Cts.Token)).ToList();

                return newPoints;
            }

            return new List<PointSync>();
        }

        private async Task<List<CachedCygNetPointWithRule>> FetchCygNetPoints()
        {
            var resolver = new CachedFacilityResolver(SrcDataModel.CygNetGeneral);
            _configFileLog.Debug(
                $"CachedFacilityResolver made with {SrcDataModel.CygNetGeneral.FacSiteService} and {SrcDataModel.CygNetGeneral.FixedDomainId}");

            // Get base facilities
            var facResults = await Task.Run(() => resolver.FindFacilities(Cts.Token));

            if (facResults.Excp != null)
            {
                Status = "Facility search exception";
                throw facResults.Excp;
            }
            var pointsFound = new List<CachedCygNetPointWithRule>();

            if (facResults.SearchSucceeded && facResults.FacsFound.IsAny())
            {
                var (points, _, _) = await Task.Run(() =>
                    CygNetPointResolver.GetPointsAsync(
                        SrcDataModel.SqlConfigModel.SourcePointRules.ToList(),
                        facResults.FacsFound,
                        SrcDataModel.CygNetGeneral.FacilityFilteringRules.ChildGroups.ToList(),
                        facResults.FacService,
                        Cts.Token));

                if (points.IsAny())
                {
                    pointsFound = MaxPoints >= 0 ? points.Take(MaxPoints).ToList() : points.ToList();
                }
                _configFileLog.Debug(
                    $"Point Search Succeeded = {points.IsAny()} and CancellationRequested is {Cts.IsCancellationRequested}");
            }

            return pointsFound;
        }

        private bool _validation = true;

        int _numNewDataCheck = 0;

        private TimeSpan optMaxTimePerMaintenanceOperation = TimeSpan.FromMinutes(4.75);
        private TimeSpan optGuaranteedBackfillTime = TimeSpan.FromMinutes(0);
        private int _taskNum = 0;

        private bool IsBusy { get; set; }
        private DateTime LastSyncStartTimestamp { get; set; }

        private async Task CheckForNewPointData()
        {
            Interlocked.Increment(ref _taskNum);

            var swNewData = new Stopwatch();

            CurrentActivity = "Beginning Sync Op";

            try
            {
                await _lockPointCheck.WaitAsync();

                IsBusy = true;
                LastSyncStartTimestamp = DateTime.Now;

                swNewData.Start();
                optMaxTimePerMaintenanceOperation = TimeSpan.FromMinutes(_minsBetweenPointSync - .25);
                optGuaranteedBackfillTime = TimeSpan.FromMinutes(_minsBackfillGuaranteed);
                _numNewDataCheck += 1;
                _configFileLog.Warning("Starting point new data check #[{_numNewDataCheck}]", _numNewDataCheck);

                _validation = CygNetSqlDataCompareModel.ValidateRulesDataOnly((await GetTableSchema())).IsValid;
                if (_validation)
                {
                    _configFileLog.Warning($"Validation succeeded, looking for points due for checking");
                    Status = "Checking for New point Data";

                    Cts.Token.ThrowIfCancellationRequested();
                    var pointsDueForCheck = MyPoints
                        .Where(pnt => !pnt.IsBusy)
                        .ToList();

                    //var pointsDueForCheck = MyPoints
                    //    .Where(pnt => !pnt.IsBusy)
                    //    .Where(item => item.NextOperationScheduledTime <= DateTime.Now).ToList();

                    if (pointsDueForCheck.Any())
                    {
                        var thisResult = new ConfigSyncOperationResults(
                            totalPointCount: pointsDueForCheck.Count);

                        if (ServiceOps.RunningFromHarness && ServiceOps.LogPointSetOperationStatistics)
                        {
                            PointSyncResults.Add(thisResult);
                            //if (PointSyncResults.Count > 60 / 5 * 24)
                            //{
                            //    PointSyncResults.RemoveAt(0);
                            //}
                        }

                        Interlocked.Increment(ref _taskNum);
                        _configFileLog.Warning("Starting sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                            pointsDueForCheck.Count, _taskNum, Process.GetCurrentProcess().WorkingSet64);

                        var thisSchema = await GetTableSchema();
                        if (thisSchema == null)
                        {
                            _configFileLog.Warning($"Failed to get SqL table schema, stopping sync");
                            return;
                        }

                        var thisSqlOp = new SqlOperation(thisSchema, _sqlDeleteTimeoutSeconds, _sqlSelectTimeoutSeconds, _sqlInsertTimeoutSeconds, Cts.Token);

                        CurrentActivity = "Getting bulk copier";

                        //using (thisSqlOp.GetConnection(Cts.Token))
                        using (thisSqlOp.GetBulkCopier())
                        {
                            thisResult.MaintenanceOpStats.Start(pointsDueForCheck.Where(x => !x.NeedsDeleteAll).ToList().Count);

                            CurrentActivity = "Maintenance syncs";

                            // ============= MAINTENANCE ========================
                            await Task.Run(() => pointsDueForCheck.Where(x => !x.NeedsDeleteAll).ForEachAsync(20, async pntTask =>
                            {
                                //Interlocked.Increment(ref TestOpCount);
                                //Console.WriteLine("testOpCount1: " + TestOpCount);
                                Cts.Token.ThrowIfCancellationRequested();
                                var (rowsInserted, rowsDeleted) = await Task.Run(() => pntTask.SyncData(thisSqlOp, thisSchema, false, false, false, Cts.Token));

                                await thisResult.MaintenanceOpStats.RecordPointProcessed(
                                    tag: pntTask.SrcPoint.Tag.ToString(),
                                    numRowsInserted: rowsInserted,
                                    numRowsDeleted: rowsDeleted);

                                //Interlocked.Decrement(ref TestOpCount);
                                //Console.WriteLine("testOpCount2: " + TestOpCount);
                            }));

                            thisResult.MaintenanceOpStats.End();

                            _configFileLog.Warning(
                                "Finishing sync tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}.",
                                pointsDueForCheck.Where(x => !x.NeedsDeleteAll).ToList().Count,
                                _taskNum,
                                Process.GetCurrentProcess().WorkingSet64);

                            // ------------- END MAINTENANCE ---------------------
                            GC.Collect();
                            GC.WaitForPendingFinalizers();
                            SqlConnection.ClearAllPools();

                            // ============= DELETE ALL ========================
                            List<PointSync> pointDueForDeleteAll = pointsDueForCheck.Where(pnt => pnt.NeedsDeleteAll).OrderByDescending(x => x.MinsOnDeleteAllHold).ToList();

                            if (thisResult.ElapsedTime < optMaxTimePerMaintenanceOperation && pointDueForDeleteAll.IsAny())
                            {
                                CurrentActivity = $"Delete All";

                                _configFileLog.Warning("thisResult.OperationStopwatch.Elapsed:" + thisResult.ElapsedTime);
                                _configFileLog.Warning("optMaxTimePerMaintenanceOperation: " + optMaxTimePerMaintenanceOperation);

                                thisResult.DeleteAllOpStats.Start(pointDueForDeleteAll.Count);

                                _configFileLog.Warning(
                                    "Starting DeleteAll for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                                    pointDueForDeleteAll.Count, _taskNum, Process.GetCurrentProcess().WorkingSet64);

                                await Task.Run(() => pointDueForDeleteAll.ForEachAsync(10, thisResult.OperationStopwatch,
                                    optMaxTimePerMaintenanceOperation, async pntTask =>
                                    {
                                        Cts.Token.ThrowIfCancellationRequested();
                                        var (rowsInserted, rowsDeleted) = await Task.Run(() =>
                                            pntTask.SyncData(thisSqlOp, thisSchema, false, false, true, Cts.Token));

                                        await thisResult.DeleteAllOpStats.RecordPointProcessed(
                                            pntTask.SrcPoint.Tag.ToString(),
                                            rowsInserted,
                                            rowsDeleted);
                                    }));

                                _configFileLog.Warning(
                                    "Finishing DeleteAll tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}.",
                                    pointDueForDeleteAll.Count,
                                    _taskNum,
                                    Process.GetCurrentProcess().WorkingSet64);

                                thisResult.DeleteAllOpStats.End();
                            }
                            // ------------- END DELETE ALL ---------------------


                            // ============= BACKFILL ========================
                            List<PointSync> pointDueForBackfill = pointsDueForCheck.Where(pnt => pnt.NeedsBackfill).ToList();

                            if (pointDueForBackfill.Any() && thisResult.ElapsedTime < optMaxTimePerMaintenanceOperation + optGuaranteedBackfillTime)
                            {

                                thisResult.BackfillOpStats.Start(pointDueForBackfill.Count);

                                var passCount = 0;
                                while (thisResult.ElapsedTime < (optMaxTimePerMaintenanceOperation + optGuaranteedBackfillTime) && pointDueForBackfill.IsAny())
                                {
                                    passCount += 1;
                                    CurrentActivity = $"Backfill Pass #{passCount}";

                                    _configFileLog.Warning("thisResult.OperationStopwatch.Elapsed:" + thisResult.ElapsedTime);
                                    _configFileLog.Warning("optMaxTimePerMaintenanceOperation: " + optMaxTimePerMaintenanceOperation);

                                    _configFileLog.Warning(
                                        "Starting Backfill for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                                        pointDueForBackfill.Count, _taskNum, Process.GetCurrentProcess().WorkingSet64);

                                    await Task.Run(() => pointDueForBackfill.ForEachAsync(10, thisResult.OperationStopwatch,
                                        (optMaxTimePerMaintenanceOperation + optGuaranteedBackfillTime), async pntTask =>
                                        {
                                            Cts.Token.ThrowIfCancellationRequested();
                                            var (rowsInserted, rowsDeleted) = await Task.Run(() =>
                                                pntTask.SyncData(thisSqlOp, thisSchema, true, false, false, Cts.Token));

                                            await thisResult.BackfillOpStats.RecordPointProcessed(
                                                pntTask.SrcPoint.Tag.ToString(),
                                                rowsInserted,
                                                rowsDeleted);
                                        }));

                                    _configFileLog.Warning(
                                        "Finishing backfill tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}.",
                                        pointDueForBackfill.Count,
                                        _taskNum,
                                        Process.GetCurrentProcess().WorkingSet64);

                                    pointDueForBackfill = pointsDueForCheck.Where(pnt => pnt.NeedsBackfill).ToList();
                                }

                                thisResult.BackfillOpStats.End();
                            }
                            await Task.Run(() => thisSqlOp.FlushRowsToSql(Cts.Token));
                            // ------------- END BACKFILL ---------------------

                            // ============= DELETE OLD ========================
                            List<PointSync> pointDueForDeleteOld = pointsDueForCheck.Where(pnt => pnt.NeedsDeleteOld).OrderByDescending(x => x.MinsSinceLastClean).ToList();

                            if (thisResult.OperationStopwatch.Elapsed < optMaxTimePerMaintenanceOperation && pointDueForDeleteOld.IsAny())
                            {
                                CurrentActivity = "Delete Old";

                                _configFileLog.Warning("thisResult.OperationStopwatch.Elapsed:" + thisResult.OperationStopwatch.Elapsed);
                                _configFileLog.Warning("optMaxTimePerMaintenanceOperation: " + optMaxTimePerMaintenanceOperation);


                                thisResult.DeleteOldOpStats.Start(pointDueForDeleteOld.Count);

                                _configFileLog.Warning(
                                    "Starting DeleteOld for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}",
                                    pointDueForDeleteOld.Count, _taskNum, Process.GetCurrentProcess().WorkingSet64);

                                await pointDueForDeleteOld.ForEachAsync(10, thisResult.OperationStopwatch,
                                    optMaxTimePerMaintenanceOperation, async pntTask =>
                                    {
                                        Cts.Token.ThrowIfCancellationRequested();
                                        var (rowsInserted, rowsDeleted) = await Task.Run(() =>
                                            pntTask.SyncData(thisSqlOp, thisSchema, false, true, false, Cts.Token));

                                        await thisResult.DeleteOldOpStats.RecordPointProcessed(
                                            pntTask.SrcPoint.Tag.ToString(),
                                            rowsInserted,
                                            rowsDeleted);
                                    });

                                _configFileLog.Warning(
                                    "Finishing DeleteOld tasks for [{PointCount}] points that are due for a sync, Task #{TaskNum}. Current Memory: {mem}.",
                                    pointDueForDeleteOld.Count,
                                    _taskNum,
                                    Process.GetCurrentProcess().WorkingSet64);
                            }

                            thisResult.DeleteOldOpStats.End();
                            if (UsingTestHarness)
                            {
                                ChartViewModel.AddNewResults(thisResult);
                            }

                            // ------------- END DELETE OLD ---------------------

                            thisResult.EndOperation();

                            _configFileLog.Warning(
                            "Operation statistics for Task #{TaskNum} so far (after new data maintenance): {@opres}",
                            _taskNum,
                            thisResult);
                        }
                    }
                }
                else
                {
                    _configFileLog.Debug("Failed Validation in CheckForNewPointData");
                    Status = "Failed config validation";
                }
            }
            catch (Exception ex)
            {
                Status = $"Exception {ex.Message}";
                _configFileLog.Error(ex, "Failed new data check #[{numCheck}]", _numNewDataCheck);

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Failed new data check #[{numCheck}]", _numNewDataCheck);
            }
            finally
            {
                //SqlConnection.ClearAllPools();

                IsBusy = false;

                CurrentActivity = "";

                if (_validation)
                    Status = "Completed operation";

                _configFileLog.Debug(
                    "Finished point new data check #[{_numNewDataCheck}] with duration [{dur}]",
                    _numNewDataCheck,
                    swNewData.Elapsed);

                _lockPointCheck.Release();
            }
        }

        [XmlIgnore()]
        public List<PointSync> MyPoints
        {
            get => GetPropertyValue<List<PointSync>>();
            set => SetPropertyValue(value);
        }

        [XmlIgnore()]
        public PointSync SelectedPoint
        {
            get => GetPropertyValue<PointSync>();
            set => SetPropertyValue(value);
        }
    }
}
