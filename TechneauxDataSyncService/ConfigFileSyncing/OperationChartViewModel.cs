﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Windows.Media;
using LiveCharts;
using LiveCharts.Configurations;
using LiveCharts.Geared;
using LiveCharts.Wpf;
using LiveCharts.Wpf.Charts.Base;
using XmlDataModelUtility;

namespace TechneauxDataSyncService.ConfigFileSyncing
{
    public class OperationChartViewModel : NotifyModelBase
    {
        public OperationChartViewModel()
        {
            var mapper = Mappers.Xy<MeasureModel>()
                .X(model => model.DateTime.Ticks)   //use DateTime.Ticks as X
                .Y(model => model.Value);           //use the value property as Y

            //lets save the mapper globally.
            Charting.For<MeasureModel>(mapper);

            AxisX = new Axis()
            {
                Title = "Time",
                Separator = new Separator() { Step = TimeSpan.FromMinutes(5).Ticks },
                Unit = TimeSpan.TicksPerMinute,
                LabelFormatter = DateTimeFormatter
            };

            AxisStep = TimeSpan.FromHours(1).Ticks;
            AxisUnit = TimeSpan.TicksPerMinute;

            SetAxisLimits();


            // Counts
            PointCountSeriesCollection = new SeriesCollection
            {
                new GStackedAreaSeries
                {
                    Title = "Maintenance",
                    Values = MaintenancePointCountValues,
                    Stroke = Brushes.LimeGreen,
                    Fill = Brushes.LightGreen,
                    PointGeometry = DefaultGeometries.Circle
                },
                new GStackedAreaSeries
                {
                    Title = "Backfill",
                    Values = BackfillPointCountValues,
                    Stroke = Brushes.DeepSkyBlue,
                    Fill = Brushes.LightBlue,
                    PointGeometry = DefaultGeometries.Square
                },
                new GStackedAreaSeries
                {
                    Title = "Delete All",
                    Values = DeleteAllPointCountValues,
                    Stroke = Brushes.Red,
                    Fill = Brushes.LightCoral,
                    PointGeometry = DefaultGeometries.Diamond
                },
                new GStackedAreaSeries
                {
                    Title = "Delete Old",
                    Values = DeleteOldPointCountValues,
                   Stroke = Brushes.Gold,
                   Fill = Brushes.LightGoldenrodYellow,
                   PointGeometry = DefaultGeometries.Diamond
                }
            };
            //PointCountChart = new CartesianChart()
            //{
            //    AxisX = new AxesCollection() { AxisX },
            //    Series = PointCountSeriesCollection
            //};


            // Rates
            PointRateSeriesCollection = new SeriesCollection
            {
                new GLineSeries
                {
                    Title = "Maintenance",
                    Values = MaintenancePpmValues,
                    Stroke = Brushes.LimeGreen,
                    Fill = Brushes.LightGreen,
                    PointGeometry = DefaultGeometries.Circle
                },
                new GLineSeries
                {
                    Title = "Backfill",
                    Values = BackfillPpmValues,
                    Stroke = Brushes.DeepSkyBlue,
                    Fill = Brushes.LightBlue,
                    PointGeometry = DefaultGeometries.Square
                },
                new GLineSeries
                {
                    Title = "Delete All",
                    Values = DeleteAllPpmValues,
                    Stroke = Brushes.Red,
                    Fill = Brushes.LightCoral,
                    PointGeometry = DefaultGeometries.Diamond
                },
                new GLineSeries
                {
                    Title = "Delete Old",
                    Values = DeleteOldPpmValues,
                    Stroke = Brushes.Gold,
                    Fill = Brushes.LightGoldenrodYellow,
                    PointGeometry = DefaultGeometries.Diamond
                }
            };

            // Durations
            OpDurationSeriesCollection = new SeriesCollection
            {
                new GStackedAreaSeries
                {
                    Title = "Maintenance",
                    Values = MaintenanceDurationValues,
                    Stroke = Brushes.LimeGreen,
                    Fill = Brushes.LightGreen,
                    PointGeometry = DefaultGeometries.Circle
                },
                new GStackedAreaSeries
                {
                    Title = "Backfill",
                    Values = BackfillDurationValues,
                    Stroke = Brushes.DeepSkyBlue,
                    Fill = Brushes.LightBlue,
                    PointGeometry = DefaultGeometries.Square
                },
                new GStackedAreaSeries
                {
                    Title = "Delete All",
                    Values = DeleteAllDurationValues,
                    Stroke = Brushes.Red,
                    Fill = Brushes.LightCoral,
                    PointGeometry = DefaultGeometries.Diamond
                },
                new GStackedAreaSeries
                {
                    Title = "Delete Old",
                    Values = DeleteOldDurationValues,
                    Stroke = Brushes.Gold,
                    Fill = Brushes.LightGoldenrodYellow,
                    PointGeometry = DefaultGeometries.Diamond
                }
            };



            //PointRateChart = new CartesianChart()
            //{
            //    AxisX = new AxesCollection() { AxisX },
            //    Series = PointCountSeriesCollection
            //};




        }

        public Axis AxisX { get; }

        private DateTime InitTime = DateTime.Now;

        public SeriesCollection PointCountSeriesCollection { get; set; } = new SeriesCollection();
        public SeriesCollection PointRateSeriesCollection { get; set; } = new SeriesCollection();
        public SeriesCollection OpDurationSeriesCollection { get; set; } = new SeriesCollection();


        public Chart PointCountChart { get; set; }
        public Chart PointRateChart { get; set; }

        private GearedValues<MeasureModel> MaintenancePointCountValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> BackfillPointCountValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> DeleteAllPointCountValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> DeleteOldPointCountValues { get; } = new GearedValues<MeasureModel>();

        private GearedValues<MeasureModel> MaintenancePpmValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> BackfillPpmValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> DeleteAllPpmValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> DeleteOldPpmValues { get; } = new GearedValues<MeasureModel>();

        private GearedValues<MeasureModel> MaintenanceDurationValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> BackfillDurationValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> DeleteAllDurationValues { get; } = new GearedValues<MeasureModel>();
        private GearedValues<MeasureModel> DeleteOldDurationValues { get; } = new GearedValues<MeasureModel>();

        public Func<double, string> DateTimeFormatter { get; set; } = value => new DateTime((long)value).ToString("h:mm");

        private TimeSpan GetMinTimeSpan(TimeSpan t1, TimeSpan t2) => t1 <= t2 ? t1 : t2;
        private TimeSpan GetMaxTimeSpan(TimeSpan t1, TimeSpan t2) => t1 >= t2 ? t1 : t2;
        private TimeSpan GetAtLeast(TimeSpan min, TimeSpan compare) => GetMaxTimeSpan(min, compare);
        private TimeSpan GetAtMost(TimeSpan max, TimeSpan compare) => GetMinTimeSpan(max, compare);

        private TimeSpan GetTimeSpanWithin(TimeSpan min, TimeSpan max, TimeSpan compare) => GetAtMost(max, GetAtLeast(min, compare));

        private double ItemsPerMinute(int numItems, TimeSpan duration) =>
            duration <= TimeSpan.FromMilliseconds(100) ? double.NaN : Convert.ToDouble(numItems) / duration.TotalMinutes;

        public void AddNewResults(IConfigSyncOperationResults newResults)
        {
            MaintenancePointCountValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.MaintenanceOpStats.NumPointsProcessed });
            BackfillPointCountValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.BackfillOpStats.NumProcessingPasses });
            DeleteAllPointCountValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.DeleteAllOpStats.NumPointsProcessed });
            DeleteOldPointCountValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.DeleteOldOpStats.NumPointsProcessed });

            MaintenancePpmValues.Add(new MeasureModel
            {
                DateTime = newResults.StartTime,
                Value = ItemsPerMinute(newResults.MaintenanceOpStats.NumPointsProcessed, newResults.MaintenanceOpStats.OperationDuration)
            });
            BackfillPpmValues.Add(new MeasureModel
            {
                DateTime = newResults.StartTime,
                Value = ItemsPerMinute(newResults.BackfillOpStats.NumProcessingPasses, newResults.BackfillOpStats.OperationDuration)
            });
            DeleteAllPpmValues.Add(new MeasureModel
            {
                DateTime = newResults.StartTime,
                Value = ItemsPerMinute(newResults.DeleteAllOpStats.NumPointsProcessed, newResults.DeleteAllOpStats.OperationDuration)
            });
            DeleteOldPpmValues.Add(new MeasureModel
            {
                DateTime = newResults.StartTime,
                Value = ItemsPerMinute(newResults.DeleteOldOpStats.NumPointsProcessed, newResults.DeleteOldOpStats.OperationDuration)
            });

            MaintenanceDurationValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.MaintenanceOpStats.OperationDuration.TotalMinutes});
            BackfillDurationValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.BackfillOpStats.OperationDuration.TotalMinutes });
            DeleteAllDurationValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.DeleteAllOpStats.OperationDuration.TotalMinutes });
            DeleteOldDurationValues.Add(new MeasureModel { DateTime = newResults.StartTime, Value = newResults.DeleteOldOpStats.OperationDuration.TotalMinutes });


            SetAxisLimits();
        }


        private void SetAxisLimits()
        {
            AxisX.MaxValue = DateTime.Now.Ticks; // lets force the axis to be 1 second ahead
            AxisX.MinValue = DateTime.Now.Subtract(
                GetTimeSpanWithin(TimeSpan.FromMinutes(15), TimeSpan.FromHours(24), DateTime.Now - InitTime)).Ticks;

            AxisMax = DateTime.Now.Ticks;
            AxisMin = DateTime.Now.Subtract(
                GetTimeSpanWithin(TimeSpan.FromMinutes(15), TimeSpan.FromHours(24), DateTime.Now - InitTime)).Ticks;

        }

        public double AxisStep { get; set; }
        public double AxisUnit { get; set; }

        public double AxisMax
        {
            get => GetPropertyValue<double>();
            set => SetPropertyValue(value);
        }
        public double AxisMin
        {
            get => GetPropertyValue<double>();
            set => SetPropertyValue(value);
        }




    }
}
