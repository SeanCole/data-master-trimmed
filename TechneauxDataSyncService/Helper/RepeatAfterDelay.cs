﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxDataSyncService.Helper
{
    public static class ReactiveDelay
    {
        public static IObservable<T> RepeatAfterDelay<T>(
            this IObservable<T> source, 
            TimeSpan delay, 
            IScheduler scheduler)
        {
            var repeatSignal = Observable
                .Empty<T>()
                .Delay(delay, scheduler);

            // when source finishes, wait for the specified
            // delay, then repeat.
            return source.Concat(repeatSignal).Repeat();
        }
    }
}
