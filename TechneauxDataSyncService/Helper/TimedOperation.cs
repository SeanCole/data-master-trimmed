﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechneauxDataSyncService.Helper
{
    public class TimedOperation
    {
        private readonly Stopwatch _sw = new Stopwatch();

        internal void StartTiming()
        {
            _sw.Start();
        }

        internal TimeSpan GetTotalTimeTaken()
        {
            _sw.Stop();
            return _sw.Elapsed;
        }

        private readonly Dictionary<string, Stopwatch> _subTimers = new Dictionary<string, Stopwatch>();

        internal void StartSubtask(string name)
        {
            var subSw = new Stopwatch();
            _subTimers[name] = subSw;

            subSw.Start();
        }

        internal TimeSpan GetSubTaskDuration(string name)
        {
            if (!_subTimers.TryGetValue(name, out var subSw))
                throw new InvalidOperationException($"Timer with name [{name}] doesn't exist");

            subSw.Stop();
            return subSw.Elapsed;
        }
    }
}
