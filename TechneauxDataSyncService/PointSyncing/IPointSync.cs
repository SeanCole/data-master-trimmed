﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechneauxDataSyncService.PointSyncing.Operations;

namespace TechneauxDataSyncService.PointSyncing
{
    public interface IPointSync
    {
        string CurrentStatus { get; }

        string LastOperationType { get; }
        DateTime LastSyncRunTime { get; }

        //DateTime NextOperationScheduledTime { get; }

        double AveMinsDelayScheduleVsActual { get; }

        bool TrackAllOps { get; set; }

        List<ISqlSyncResults> OpsRecords { get; }

        //TimeSpan PredictionInterval { get; }

        bool LastSyncInException { get; }
    }


}







