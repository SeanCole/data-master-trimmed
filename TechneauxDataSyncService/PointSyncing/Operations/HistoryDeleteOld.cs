﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using XmlDataModelUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryDeleteOld : TimedOperation, ISqlSyncOperation
    {
        public HistoryDeleteOld(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule srcPnt)
        {
            Schema = newSchema;

            if (Schema == null || Schema.IsEmptySqlSchema)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteOld: Schema is null or empty. HistoryDeleteOld: {this.ToString()}");
                throw new ArgumentNullException(nameof(newSchema), "Schema must not be null or empty");
            }

            if (srcPnt == null)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteOld: Source point is null. HistoryDeleteOld: {this.ToString()}");
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");
            }


            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteOld: Source rule is null or invalid. HistoryDeleteOld: {this.ToString()}");

                throw new ArgumentException("Src point rule must be non-null and valid");
            }

            SrcPoint = srcPnt;

        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        #endregion


        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryDeleteOld));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public override string ToString()
        {
            return $"{nameof(RetentionDays)}=[{RetentionDays}], " +
                   $"{nameof(Results)}=[{Results}]";
        }

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();

                StartSubtask(nameof(sqlOp.DeleteSqlRows));

                Results.NumSqlRowsDeleted =
                    await sqlOp.DeleteSqlRows(SrcPoint, RetentionDays, ct);

                //Results.NumSqlRowsDeleted = await SqlRowResolver.DeleteExpiredSqlForPoint(Schema, SrcPoint, SrcPoint.SrcRule.PollingOptions.RetentionDays,ct);

                ct.ThrowIfCancellationRequested();
                Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.DeleteSqlRows)).ToString());
                Results.LogAuditMessage($"Sql expired rows delete finished with {Results.NumSqlRowsDeleted} rows deleted.");

                Results.StatusMessage = "Finished expired history delete.";
            }
            catch (SqlException exS)
            {
                Results.FailedException = exS;
                Results.OpResult = OpStatuses.Exception;

                if (exS.Number == -2)
                {
                    Results.StatusMessage = $"Sql timeout while running delete query for HistoryDeleteOld. Delete query timeout was set to {ServiceOps.DeleteTimeout} seconds.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteOld: Sql timeout. HistoryDeleteOld: {this.ToString()}");
                }
                else
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteOld: Sql query failed. HistoryDeleteOld: {this.ToString()}");
                    Results.StatusMessage = $"Sql Error - Delete query failed for HistoryDeleteOld.";

                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - Old History Delete";
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteOld: Unhandled exception. HistoryDeleteOld: {this.ToString()}");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Error(ex, "Unhandled Exception during old history deletion operation.");
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }
    }
}
