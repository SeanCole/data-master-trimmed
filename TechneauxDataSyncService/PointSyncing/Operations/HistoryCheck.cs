﻿using System;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.CygNet.FacilityPointOptions;
using TechneauxReportingDataModel.Helper;
using TechneauxUtility;
using XmlDataModelUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryCheck : TimedOperation, ISqlSyncOperation
    {
        public HistoryCheck(CachedCygNetPointWithRule srcPnt, bool verifiedNoHistoryInSql)
        {
            if (srcPnt == null)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: Source point is null. HistoryCheck: {this.ToString()}");
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");
            }

            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: Source rule is null or invalid. HistoryCheck: {this.ToString()}");
                throw new ArgumentException("Src point rule must be non-null and valid");
            }

            SrcPoint = srcPnt;

            _checkForExistingSqlHistory = !verifiedNoHistoryInSql;

            EarliestDate = DateTime.Now.AddDays(-RetentionDays);
        }

        #region LocalVars

        private CachedCygNetPointWithRule SrcPoint { get; }

        private DateTime EarliestDate { get; }
        private DateTime LatestDate { get; } = DateTime.Now;

        #endregion

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        private readonly bool _checkForExistingSqlHistory;

        private StringBuilder _pointSyncAuditTrailBuilder = new StringBuilder();

        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryCheck));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public override string ToString()
        {
            return $"{nameof(HasSqlHistoryToDelete)}=[{HasSqlHistoryToDelete}], " +
                   $"{nameof(EarliestDate)}=[{EarliestDate}], " +
                   $"{nameof(LatestDate)}=[{LatestDate}], " +
                   $"{nameof(RetentionDays)}=[{RetentionDays}], " +
                   $"{nameof(Results)}=[{Results}]";
        }

        public bool HasSqlHistoryToDelete { get; private set; } = false;

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();

                Results.OperationInterval = (EarliestDate, LatestDate);
                var includeNumericOnly = PointSync.CheckForNumeric(SrcPoint.SrcRule);
                PointHistoryGeneralOptions genHistOptions = SrcPoint.SrcRule.GeneralHistoryOptions;
                if (SrcPoint.SrcRule.HistoryNormalizationOptions.IsExtendedNormType)
                {
                    if (!(await SrcPoint.HasHistory(SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable, includeNumericOnly, genHistOptions.EnableCeiling, genHistOptions.EnableFloor, genHistOptions.FilterCeiling, genHistOptions.FilterFloor)))
                    {
                        Results.OpResult = OpStatuses.Fail;
                        Results.StatusMessage = "History does not exist that is usable by this normalization type.";

                        if (_checkForExistingSqlHistory)
                        {
                            HasSqlHistoryToDelete = await sqlOp.HasAnySqlHistory(SrcPoint, ct);
                        }
                        if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                            PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: No history exists. HistoryCheck: {this.ToString()}");
                        return;
                    }

                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage =
                        "This is an extended norm type and history does exist.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: Source rule is null or invalid. HistoryCheck: {this.ToString()}");
                    return;
                }

                if (await SrcPoint.HasHistoryInInterval(
                    EarliestDate,
                    LatestDate,
                    SrcPoint.SrcRule.GeneralHistoryOptions.IncludeUnreliable,
                    includeNumericOnly,
                    genHistOptions.EnableCeiling,
                    genHistOptions.EnableFloor,
                    genHistOptions.FilterCeiling,
                    genHistOptions.FilterFloor))
                {
                    Results.OpResult = OpStatuses.Pass;
                    Results.StatusMessage =
                        "There is history in the retention period.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: History exists in retention. HistoryCheck: {this.ToString()}");
                    return;
                }
                else
                {
                    Results.OpResult = OpStatuses.Fail;
                    Results.StatusMessage = "There is no history in the retention period that is usable by this normalization type or none exists at all.";

                    if (_checkForExistingSqlHistory)
                    {
                        HasSqlHistoryToDelete = await sqlOp.HasAnySqlHistory(SrcPoint, ct);
                    }
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: No history exists in retention. HistoryCheck: {this.ToString()}");
                    return;
                }
            }
            catch (SqlException exS)
            {
                Results.FailedException = exS;
                Results.OpResult = OpStatuses.Exception;

                if (exS.Number == -2)
                {
                    Results.StatusMessage = $"Sql timeout while running select query for HistoryCheck. Select query timeout was set to {ServiceOps.SelectTimeout} seconds.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: Sql timeout. HistoryCheck: {this.ToString()}");
                }
                else
                {
                    Results.StatusMessage = $"Sql Error - Select query failed for HistoryCheck.";
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: Query failed. HistoryCheck: {this.ToString()}");

                }
            }
            catch (Exception ex)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryCheck: Unhandled Exception. HistoryCheck: {this.ToString()}");
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - First Check";

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception during history check.");
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }

        }

    }
}
