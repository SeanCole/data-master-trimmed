﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;
using TechneauxDataSyncService.Helper;
using TechneauxDataSyncService.PointSyncing.Operations.Base;
using TechneauxHistorySynchronization;
using TechneauxHistorySynchronization.Models;
using TechneauxHistorySynchronization.SqlServerHistory;
using TechneauxReportingDataModel.Helper;
using TechneauxReportingDataModel.SqlHistory;
using TechneauxUtility;
using XmlDataModelUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxDataSyncService.PointSyncing.Operations
{
    public class HistoryDeleteAll : TimedOperation, ISqlSyncOperation
    {
        public HistoryDeleteAll(
            SimpleCombinedTableSchema newSchema,
            CachedCygNetPointWithRule srcPnt)
        {
            Schema = newSchema;

            if (Schema == null || Schema.IsEmptySqlSchema)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteAll: Schema is null or empty. HistoryDeleteAll: {this.ToString()}");
                throw new ArgumentNullException(nameof(newSchema), "Schema must not be null or empty");
            }

            if (srcPnt == null)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteAll: Src point is null. HistoryDeleteAll: {this.ToString()}");
                throw new ArgumentNullException(nameof(srcPnt), "Src Point must not be empty");
            }


            if (srcPnt.SrcRule == null || !srcPnt.SrcRule.IsRuleValid)
            {
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteAll: Source rule is null or invalid. HistoryDeleteAll: {this.ToString()}");
                throw new ArgumentException("Src point rule must be non-null and valid");
            }

            SrcPoint = srcPnt;

        }

        #region LocalVars

        private SimpleCombinedTableSchema Schema { get; }
        private CachedCygNetPointWithRule SrcPoint { get; }

        private double RetentionDays => SrcPoint.SrcRule.PollingOptions.RetentionDays;

        #endregion


        public SimpleSqlSyncResults Results { get; } = new SimpleSqlSyncResults(nameof(HistoryDeleteAll));
        public ISqlSyncDetailedResults DetailedHistoryResults { get; } = new SqlSyncDetailedResults();

        public override string ToString()
        {
            return $"{nameof(RetentionDays)}=[{RetentionDays}], " +
                   $"{nameof(Results)}=[{Results}]";
        }

        public async Task DoOperationAsync(
            SqlOperation sqlOp,
            IProgress<(int, string)> progress,
            CancellationToken ct)
        {
            try
            {
                // Start timing operation
                StartTiming();

                StartSubtask(nameof(sqlOp.DeleteSqlRows));

                Results.NumSqlRowsDeleted =
                    await sqlOp.DeleteSqlRows(SrcPoint, ct);
                //Results.NumSqlRowsDeleted = await SqlRowResolver.DeleteExpiredSqlForPoint(Schema, SrcPoint, SrcPoint.SrcRule.PollingOptions.RetentionDays,ct);

                ct.ThrowIfCancellationRequested();
                Results.LogAuditMessage(GetSubTaskDuration(nameof(sqlOp.DeleteSqlRows)).ToString());
                Results.LogAuditMessage($"Sql all rows delete finished with {Results.NumSqlRowsDeleted} rows deleted.");

                Results.StatusMessage = "Finished all history delete.";
            }
            catch (SqlException exS)
            {
                Results.FailedException = exS;
                Results.OpResult = OpStatuses.Exception;

                if (exS.Number == -2)
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteAll: Sql query failed. HistoryDeleteAll: {this.ToString()}");
                    Results.StatusMessage = $"Sql timeout while running delete query for HistoryDeleteAll. Delete query timeout was set to {ServiceOps.DeleteTimeout} seconds.";
                }
                else
                {
                    if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                        PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteAll: Sql query failed. HistoryDeleteAll: {this.ToString()}");
                    Results.StatusMessage = $"Sql Error - Delete query failed for HistoryDeleteAll.";

                }
            }
            catch (Exception ex)
            {
                Results.FailedException = ex;
                Results.OpResult = OpStatuses.Exception;
                Results.StatusMessage = "General operation failure - All History Delete";
                if (GlobalLoggers.CheckPoint(SrcPoint.LongId))
                    PointLogger.Here().Information($"{SrcPoint.LongId}: HistoryDeleteAll: Unhandled exception. HistoryDeleteAll: {this.ToString()}");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled Exception");
            }
            finally
            {
                Results.TimeTakenPerformingOperation = GetTotalTimeTaken();
            }
        }


    }
}
