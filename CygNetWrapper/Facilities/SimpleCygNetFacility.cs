﻿using CygNet.Data.Core;
using System.Collections.Generic;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;

namespace Techneaux.CygNetWrapper.Facilities
{
    public class SimpleCygnetFacility
    {
        public class StringPool
        {
            private readonly Dictionary<string, string> contents =
                new Dictionary<string, string>();

            public string Add(string item)
            {
                string ret;
                if (!contents.TryGetValue(item, out ret))
                {
                    contents[item] = item;
                    ret = item;
                }
                return ret;
            }
        }

        private StringPool AttrStringPool = new StringPool();



        public SimpleCygnetFacility(FacilityTag thisTag)
        {
            Tag = thisTag;
        }

        
        public Dictionary<string, string> AttributeValues { get; } = new Dictionary<string, string>();

        public Dictionary<string, FacilityAttributeValue> FullAttributeValues { get; } = new Dictionary<string, FacilityAttributeValue>();

        public FacilityTag Tag { get; set; }

        public void AddAttributeValue(FacilityAttributeValue value)
        {
            FullAttributeValues[value.AttributeId] = value;
            AttributeValues[value.AttributeId] = value.ToString();
        }
        
        public bool TryGetParentFac(
            Dictionary<FacilityTag, SimpleCygnetFacility> sourceList,
            string parentAttr,
            out SimpleCygnetFacility parentFac)
        {
            parentFac = null;

            var parentAttrValue = AttributeValues[parentAttr];

            if (string.IsNullOrWhiteSpace(parentAttrValue))
            {
                return false;
            }

            if (FacilityTag.TryParse(parentAttrValue, out var parentTag))
            {
                return sourceList.TryGetValue(parentTag, out parentFac);
            }

            return FacilityTag.TryParse($"{Tag.SiteService}::{parentAttrValue}", out parentTag) && sourceList.TryGetValue(parentTag, out parentFac);
        }

        public bool IsFacilityChild(string childAttr, SimpleCygnetFacility testFac)
        {
            var parentTag = testFac.Tag;
            if (AttributeValues[childAttr] == parentTag.GetFacilityTag())
            {
                return true;
            }
            else if (AttributeValues[childAttr] == parentTag.FacilityId &&
                AttributeValues["facility_site"] == parentTag.Site &&
                AttributeValues["facility_service"] == parentTag.Service)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public List<SimpleCygnetFacility> FindChildren(Dictionary<string, List<SimpleCygnetFacility>> sourceList,
            string parentRefAttr,
            List<IFacilityFilterCondition> childFilterList)
        {
            var childlist = new List<SimpleCygnetFacility>();
            FindChildrenRecursively(sourceList, childFilterList, ref childlist, Tag.ToString());
            return childlist;
        }

        private void FindChildrenRecursively(
            Dictionary<string, List<SimpleCygnetFacility>> sourceList,
            List<IFacilityFilterCondition> childFilterList,
            ref List<SimpleCygnetFacility> refChildList, string strParent)
        {
            if (sourceList.TryGetValue(strParent, out var outList))
            {
                foreach (var objFac in outList)
                {
                    if (refChildList.Contains(objFac))
                    {
                        return;
                    }
                    else
                    {
                        refChildList.Add(objFac);
                        FindChildrenRecursively(sourceList, childFilterList, ref refChildList, objFac.Tag.ToString());
                    }
                }
            }
        }

        public static Dictionary<string, List<SimpleCygnetFacility>> BuildReferenceDictionary(IEnumerable<SimpleCygnetFacility> sourceList, string refAttr)
        {
            var refDict = new Dictionary<string, List<SimpleCygnetFacility>>();

            foreach (var fac in sourceList)
            {
                //FacilityTag ThisTag;

                //if (FacilityTag.TryParse(fac.AttributeValues[refAttr], out ThisTag) ||
                //    (FacilityTag.TryParse($"{fac.Tag.SiteService.ToString()}::{fac.AttributeValues[refAttr]}", out ThisTag)))
                //{
                //    List<SimpleCygnetFacility> Children;
                //    if (!RefDict.TryGetValue(ThisTag, out Children))
                //    {
                //        Children = new List<SimpleCygnetFacility>();
                //        RefDict.Add(ThisTag, Children);
                //    }

                //    RefDict[ThisTag].Add(fac);
                //}
                //else
                //{
                //    continue;
                //}
                var refTag = fac.AttributeValues[refAttr];
                if (!string.IsNullOrWhiteSpace(refTag))
                {
                    if (!refTag.Contains("::"))
                        refTag = fac.Tag.SiteService + "::" + refTag;

                    if (!refDict.TryGetValue(refTag, out var children))
                    {
                        children = new List<SimpleCygnetFacility>();
                        refDict.Add(refTag, children);
                    }

                    refDict[refTag].Add(fac);
                }
            }

            return refDict;
        }

    }
}
