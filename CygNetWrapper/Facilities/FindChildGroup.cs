﻿using System.Collections.Generic;
using System.Linq;
using Techneaux.CygNetWrapper.Facilities.Filtering;

namespace Techneaux.CygNetWrapper.Facilities
{
    public class FindChildGroups
    {
        private readonly List<ICachedCygNetFacility> _childFacs;
        public FindChildGroups(List<ICachedCygNetFacility> childFacs)
        {
            _childFacs = childFacs;
        }

        public Dictionary<int, ICachedCygNetFacility> GetOrdinalizedChildrenInGroup(
            List<IFacilityFilterCondition> groupFilters,
            string ordinalAttr = null)
        {
            var facsInGroup = new Dictionary<int, ICachedCygNetFacility>();

            // If the ordinal attribute param is valid and not null, we will only accept facs with an ordinal, and we will add those to the dictionary
            if (!string.IsNullOrWhiteSpace(ordinalAttr))
            {
                foreach (var fac in _childFacs)
                {
                    if (FacilityFilterConditionBase.DoAttributesMatchFilters(fac, groupFilters))
                    {
                        if (fac.TryGetAttributeValue(ordinalAttr, out var thisVal) && thisVal.TryGetInt(out int thisOrd))
                        {
                            facsInGroup[thisOrd] = fac;
                        }
                    }
                }
            }

            // If the ordinal attribute param is null or invalid, we will alphabetize the facs by id, and add those to the dictionary
            else
            {
                var matchingFacs = _childFacs.Where(fac => FacilityFilterConditionBase.DoAttributesMatchFilters(fac, groupFilters));

                matchingFacs = matchingFacs.OrderBy(fac => fac.Id);

                var i = 1;
                foreach (var fac in matchingFacs)
                {
                    facsInGroup[i] = fac;
                    i++;
                }
            }

            return facsInGroup;
        }


    }
}
