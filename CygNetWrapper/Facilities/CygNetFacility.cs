﻿using CygNet.Data.Core;
using System;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.FAC;

namespace Techneaux.CygNetWrapper.Facilities
{
    public class CygNetFacility : BaseCygNetFacility
    {
        public CygNetFacility(
            FacilityService newFacService,
            CurrentValueService newCvsService,
            FacilityTag newTag)
            : base(newFacService, newCvsService, newTag)
        { }

        public CygNetFacility(
           FacilityService newFacService,
           FacilityTag newTag)
           : base(newFacService, newTag)
        { }
        public override string Description => GetAttributeValue("facility_desc").ToString();

        public override string Type => GetAttributeValue("facility_type").ToString();

        public override string Category => GetAttributeValue("facility_category").ToString();

        public override bool IsActive => GetAttributeValue("facility_is_active").StringValue == "Y";

        public override async Task<FacilityAttributeValue> GetAttributeValueAsync(string attributeName, bool refresh = false)
        {
            await GetFacRecord();

            if (HasAttribute(attributeName, refresh))
            {

                if (TryGetAttributeValue(attributeName, out var thisVal))
                {
                    return thisVal;
                }

                throw new ArgumentException("Attribute name is invalid or unavailable");
            }

            throw new ArgumentException("Attribute name is invalid or unavailable");
        }

        public override FacilityAttributeValue GetAttributeValue(string attributeId, bool refresh = false)
        {
            if (FacRecord == null || refresh)
            {
                FacRecord = FacService.GetFacilityRecord(Tag);
            }

            if (HasAttribute(attributeId))
            {

                if (TryGetAttributeValue(attributeId, out var thisVal))
                {
                    return thisVal;
                }

                throw new ArgumentException("Attribute name is invalid or unavailable");
            }

            throw new ArgumentException("Attribute name is invalid or unavailable");
        }

        private FacilityAttributeValue _GetAttributeValue(string attrId, bool refresh = false)
        {
            int index;

            var thisAttrDef = FacService.EnabledAttributes[attrId];

            if (attrId == "facility_category")
            {
                return new FacilityAttributeValue(thisAttrDef, FacRecord.Category);
            }

            if (attrId == "facility_description")
            {
                return new FacilityAttributeValue(thisAttrDef, FacRecord.Description);
            }

            if (attrId == "facility_is_active")
            {
                return new FacilityAttributeValue(thisAttrDef, FacRecord.IsActive);
            }

            if (attrId == "facility_security_app")
            {
                return new FacilityAttributeValue(thisAttrDef, FacRecord.SecurityApp);
            }

            if (attrId == "facility_type")
            {
                return new FacilityAttributeValue(thisAttrDef, FacRecord.Type);
            }

            if (attrId.StartsWith("facility_info"))
            {
                index = Convert.ToInt16(attrId.Replace("facility_info", ""));
                if (index >= 0 && index <= 1)
                    return new FacilityAttributeValue(thisAttrDef, FacRecord.Info[index]);
                return null;
            }

            if (attrId.StartsWith("facility_attr"))
            {
                index = Convert.ToInt16(attrId.Replace("facility_attr", ""));
                if (index >= 0 && index <= 29)
                    return new FacilityAttributeValue(thisAttrDef, FacRecord.Attribute[index]);
                return null;
            }

            if (attrId.StartsWith("facility_table"))
            {
                index = Convert.ToInt16(attrId.Replace("facility_table", ""));
                if (index >= 0 && index <= 29)
                    return new FacilityAttributeValue(thisAttrDef, FacRecord.Table[index]);
                return null;
            }

            if (attrId.StartsWith("facility_yes_no"))
            {
                index = Convert.ToInt16(attrId.Replace("facility_yes_no", ""));
                if (index >= 0 && index <= 29)
                    return new FacilityAttributeValue(thisAttrDef, FacRecord.YesNo[index]);
                return null;
            }

            return null;
        }

        public override bool TryGetAttributeValue(string attributeName, out FacilityAttributeValue attributeValue, bool refresh = false)
        {
            if (HasAttribute(attributeName, refresh) && FacRecord != null)
            {
                attributeValue = _GetAttributeValue(attributeName, refresh);
                return true;
            }

            attributeValue = null;
            return false;
        }

    }
}