﻿using CygNet.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Techneaux.CygNetWrapper.Points
{
    public class CachedUdc
    {
        public CachedUdc(string newUdc)
        {
            UdcExistsOnFacility = false;
            Udc = newUdc;
            ThisPoint = null;
        }

        public CachedUdc(ICachedCygNetPoint newPoint)
        {
            UdcExistsOnFacility = true;
            ThisPoint = newPoint;
            Udc = ThisPoint.Tag.UDC;

            var tagAttrs = PointAttribute.AllPointAttributes.Values.Where(attr => attr.Category == PointAttribute.Categories.Tag);
            foreach (var tagAttr in tagAttrs)
            {
                var tagVal = new PointAttributeValue(tagAttr, ThisPoint.Tag);
                AttrValueCache.Add(tagAttr.Id, tagVal);
            }
        }

        public bool UdcExistsOnFacility { get; }
        public string Udc { get; }
        public PointTag Tag => ThisPoint?.Tag;

        public ICachedCygNetPoint ThisPoint { get; }

        private Dictionary<string, PointAttributeValue> AttrValueCache { get; set; } = new Dictionary<string, PointAttributeValue>();


        private DateTime AttrsLastUpdated = DateTime.Now;
        

        public const double MinsBetweenPointUpdates = 15;

        public IDictionary<string, PointAttributeValue> AllCachedAttributes
        {
            get
            {
                if ((DateTime.Now - AttrsLastUpdated).TotalMinutes > MinsBetweenPointUpdates)
                {
                    AttrsLastUpdated = DateTime.Now;

                    var keyList = AllCachedAttributes.Keys.Where(key => !key.Contains("Tag")).ToList();

                    keyList.ForEach(key => AttrValueCache.Remove(key));
                }

                return AttrValueCache;
            }
        }

        public HashSet<string> AllCachedAttributeIds => new HashSet<string>(AllCachedAttributes.Keys);


        public void SetAttributeValue(string attrId, PointAttributeValue val)
        {
            AttrValueCache[attrId] = val;
        }
    }
}
