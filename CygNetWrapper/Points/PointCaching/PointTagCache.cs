﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Odbc;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using Serilog;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Points.PointCaching
{
    public class PointTagCache
    {
        private static TimeSpan RetentionTime { get; } = TimeSpan.FromMinutes(5);
        
        //private Dictionary<string, (DateTime dateAdded, Dictionary<string, PointTag> facs)> _cachedUdcs = 
        //    new Dictionary<string, (DateTime dateAdded, Dictionary<string, PointTag> facs)>();

        //private Dictionary<string, (DateTime dateAdded, Dictionary<string, PointTag> tags)> _fullyCachedFacIds =
        //    new Dictionary<string, (DateTime dateAdded, Dictionary<string, PointTag> tags)>();

        private ExpiringCache<string, Dictionary<string, PointTag>> _cachedUdcs;
        private ExpiringCache<string, Dictionary<string, PointTag>> _fullyCachedFacIds;

        private CurrentValueService ParentCvsService { get; }

        public PointTagCache(CurrentValueService srcCvs)
        {
            ParentCvsService = srcCvs;

            _cachedUdcs = new ExpiringCache<string, Dictionary<string, PointTag>>(CacheUdcForAllFacilities, TimeSpan.FromMinutes(5));
            _fullyCachedFacIds = new ExpiringCache<string, Dictionary<string, PointTag>>(CachePointsForSingleFacility, TimeSpan.FromMinutes(5));
        }
        
        public async Task<List<PointTag>> GetPointsForFacility(
            FacilityTag srcTag,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                // Check if fac service is correct
                if (srcTag.SiteService.ToString() != ParentCvsService.SiteService.ToString())
                    throw new InvalidOperationException($"Facility tag site.service [{srcTag.SiteService}] does not match the current value service [{ParentCvsService.SiteService}] in the cache");

                var pnts = await _fullyCachedFacIds.GetCachedItem(srcTag.FacilityId, ct);
                
                return pnts.Values.ToList();
            }
            catch (OperationCanceledException)
            {
                return null;
                // Cancellations ignored
            }
        }


        public async Task<PointTag> GetPointForFacility(
            FacilityTag srcTag,
            string udc,
            CancellationToken ct,
            bool forceRefresh = false)
        {
            try
            {
                // Check if fac service is correct
                if (srcTag.SiteService.ToString() != ParentCvsService.SiteService.ToString())
                    throw new InvalidOperationException($"Facility tag site.service [{srcTag.SiteService}] does not match the current value service [{ParentCvsService.SiteService}] in the cache");

                if (!CygNetPoint.IsUdcValid(udc))
                {
                    throw new ArgumentException("UDC is not valid", nameof(udc));
                }

                udc = CygNetPoint.FormatUdc(udc);

                var cachedPoints = await _cachedUdcs.GetCachedItem(udc, ct);
                
                if (cachedPoints.TryGetValue(srcTag.FacilityId, out var pnt))
                {
                    return pnt;
                }

                return null;
            }
            catch (OperationCanceledException)
            {
                return null;
                // Cancellations ignored
            }
        }
        
        private async Task<Dictionary<string, PointTag>> CacheUdcForAllFacilities(
            string udcToCache,
            CancellationToken ct)
        {
            udcToCache = CygNetPoint.FormatUdc(udcToCache);
            if(!CygNetPoint.IsUdcValid(udcToCache))
                throw new ArgumentException("Udc is not valid", nameof(udcToCache));
            
            var thisPointServ = await ParentCvsService.GetAssociatedPnt();

            var theseNewPoints = await thisPointServ.GetPointTagsFromOdbcByUdcAsync(
                ParentCvsService.SiteService,
                udcToCache,
                ct);

            var tagDict = new Dictionary<string, PointTag>();

            foreach (var pnt in theseNewPoints)
            {
                tagDict[pnt.FacilityId] = pnt;
            }

            return tagDict;
        }

        private async Task<Dictionary<string, PointTag>> CachePointsForSingleFacility(
            string facId,
            CancellationToken ct)
        {
            var thisPntServ = await ParentCvsService.GetAssociatedPnt();

            var allPointTags = await thisPntServ.GetPointTagsFromOdbcAsync(
                ParentCvsService.SiteService,
                facId,
                ct);

            if (allPointTags.IsAny())
            {
                var tagDict = new Dictionary<string, PointTag>();
                
                foreach (var pointTag in allPointTags)
                {
                    if(!tagDict.ContainsKey(pointTag.UDC))
                        tagDict.Add(pointTag.UDC, pointTag);
                }
                
                //var tagDict = allPointTags.ToDictionary(tag => tag.UDC, tag => tag);
                
                return tagDict;
            }

            return null;
        }
    }
}
