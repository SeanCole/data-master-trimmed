﻿using System;
using System.Collections.Generic;
using System.Threading;
using CygNet.Data.Core;
using CygNet.Data.Points;
using System.Threading.Tasks;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.VHS;
using CygNet.Data.Historian;
using Serilog;
using Serilog.Context;
using Techneaux.CygNetWrapper.Services.PNT;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Points
{
    public abstract class BaseCygNetPoint : ICygNetPoint
    {
        public BaseCygNetPoint(
            PointService newPntService,
            CurrentValueService newCvsService,
            ICygNetFacility newFacility,
            PointTag tag)
        {
            PntService = newPntService;
            CvsService = newCvsService;
            Facility = newFacility;
            Tag = tag;
        }

        public BaseCygNetPoint()
        { }

        public ICygNetFacility Facility { get; }

        public PointService PntService { get; }
        public CurrentValueService CvsService { get; }

        public PointTag Tag { get; }

        public string TagFacilityUdc => Tag.GetTagFacilityUDC();
        public string ShortId => Tag.PointId;
        public string LongId => Tag.LongId;

        //public string Description
        //{
        //    get { return GetAttributeValue("description").ValueString; }
        //}

        private PointConfigRecord _pointConfigRecord;

        public void RefreshPointConfigRecord()
        {
            _pointConfigRecord = null;
        }

        public async Task<PointConfigRecord> GetPointConfigRecord(bool refresh = false)
        {
            if (refresh || _pointConfigRecord == null)
            {
                await Task.Run(() => _pointConfigRecord = PntService.GetPointConfigRecord(Tag));
            }

            return _pointConfigRecord;
        }

        //public virtual async Task<PointAttributeValue> GetAttributeValueAsync(string attrId, bool refresh = false)
        //{
        //    throw new NotImplementedException();
        //}

        private PointAttributeValue _GetAttributeValue(string attributeName, bool refresh = false)
        {
            throw new NotImplementedException();
        }

        public bool TryGetAttributeValue(string attrId, out PointAttributeValue attributeValue, bool refresh = false)
        {
            var validPointAttrIds = PointAttribute.AllPointAttributes;

            if (validPointAttrIds.ContainsKey(attrId))
            {
                attributeValue = _GetAttributeValue(attrId, refresh);
                return true;
            }
            else
            {
                attributeValue = null;
                return false;
            }
        }

        public bool HasCurrentValue => CvsService.PointExists(Tag);

        public PointValue GetCurrentValue()
        {
            var pointValRec = CvsService.GetPointValue(Tag);
            return new PointValue(pointValRec);
        }

        public bool TryGetCurrentValue(out PointValue result)
        {
            try
            {
                result = GetCurrentValue();
                return true;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Warning(ex, "Unable to get current value for point with tag {tag}", Tag.ToString());

                result = null;
                return false;
            }
        }

        public void WriteValue(
           object newValue,
           DateTime? newTimestamp
           )
        {
            CvsService.WriteValue(Tag, newValue, newTimestamp);
        }

        public void WriteValue(
            object value,
            DateTime? timeStamp,
            BaseStatusFlags baseStatusMask,
            BaseStatusFlags baseStatus,
            UserStatusFlags userStatusMask,
            UserStatusFlags userStatus
            )
        {
            CvsService.WriteValue(Tag, value, timeStamp, baseStatusMask, baseStatus, userStatusMask, userStatus);
        }

        public async Task<bool> HasHistoryInInterval(
            DateTime earliestDate,
            DateTime latestDate,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.HasHistoryInInterval(Tag, earliestDate, latestDate, includeUnreliable, includeNumericOnly,enableCeiling, enableFloor, ceiling, floor);
        }


        NameStatistics _cachedStats;
        public async Task<NameStatistics> GetHistoryStatistics(bool refresh = false)
        {
            if (refresh || _cachedStats == null)
            {
                var vhs = await CvsService.GetAssociatedVhs();

                _cachedStats = vhs.GetHistStats(Tag);
            }

            return _cachedStats;
        }

        public async Task<bool> HasAnyHistory(bool enableCeiling,bool enableFloor, double ceiling,double floor, bool refresh = false)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.HasHistory(Tag, true, false, enableCeiling, enableFloor, ceiling, floor);
        }

        public async Task<bool> HasHistory(
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor,
            bool refresh = false)
        {

            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.HasHistory(Tag, includeUnreliable, includeNumericOnly, enableCeiling,enableFloor,ceiling,floor);
        }

        public async Task<DateTime?> GetEarliestHistoryEntryDate(
            bool refresh)
        {
            var myStats = await GetHistoryStatistics(refresh);

            if (myStats == null)
            {
                return null;
            }
            else
            {
                return myStats.EarliestActiveEntry;
            }
        }

        public async Task<DateTime?> GetLatestHistoryEntryDate(
            bool refresh)
        {
            var myStats = await GetHistoryStatistics(refresh);

            if (myStats == null)
            {
                return null;
            }
            else
            {
                return myStats.LatestActiveEntry;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj is BaseCygNetPoint compPoint)
            {
                return Tag.Equals(compPoint.Tag);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Tag.GetHashCode();
        }

        public override string ToString()
        {
            return Tag.ToString();
        }

        public async Task<List<CygNetHistoryEntry>> GetHistory(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistory(Tag, earliestDate, latestDate, progress, includeUnreliable,includeNumericOnly, ct);
        }
        public async Task<List<CygNetHistoryEntry>> GetHistory(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, CancellationToken ct)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistory(Tag, earliestDate, latestDate, progress, ct);
        }

        public async Task<List<CygNetHistoryEntry>> GetHistoryFilterRange(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double ceilingFilter, double floorFilter)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistoryFilterRange(Tag, earliestDate, latestDate, progress, includeUnreliable, includeNumericOnly, ct, ceilingFilter: ceilingFilter, floorFilter: floorFilter);
        }

        public async Task<List<CygNetHistoryEntry>> GetHistoryFilterCeiling(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double ceilingFilter)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistoryFilterCeiling(Tag, earliestDate, latestDate, progress, includeUnreliable, includeNumericOnly, ct, ceilingFilter : ceilingFilter);
        }

        public async Task<List<CygNetHistoryEntry>> GetHistoryFilterFloor(DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct ,double floorFilter)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            return vhs.GetHistoryFilterFloor(Tag, earliestDate, latestDate, progress, includeUnreliable, includeNumericOnly, ct, floorFilter: floorFilter);
        }




        public Task<List<CygNetHistoryEntry>> GetHistoryRollup(DateTime earliestDate, DateTime latestDate, CancellationToken ct)
        {
            throw new NotImplementedException();
        }

        public async Task<List<CygNetHistoryEntry>> GetEarliestHistoryInInterval(
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            int numEntries,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            var vhs = await CvsService.GetAssociatedVhs();
            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            var revHist = vhs.GetEarliestHistoryInInterval(
                Tag,
                earliestDate,
                latestDate,
                ct,
                numEntries,
                includeUnreliable,
                includeNumericOnly,
                enableCeiling,
                enableFloor,
                ceiling,
                floor);

            return revHist;
        }

        public async Task<List<CygNetHistoryEntry>> GetLatestHistoryInInterval(
            DateTime earliestDate,
            DateTime latestDate,
            int? numEntries,
            bool includeUnreliable,
            bool includeNumericOnly,
            CancellationToken ct)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            var revHist = vhs.GetLatestHistoryInInterval(
                Tag,
                earliestDate,
                latestDate,
                ct,
                numEntries,
                includeUnreliable,
                includeNumericOnly);

            return revHist;
        }


        public async Task<List<CygNetHistoryEntry>> GetLatestHistoryInIntervalFilterRange(DateTime earliestDate, DateTime latestDate, int? numEntries, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double ceilingFilter, double floorFilter)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            var revHist = vhs.GetLatestHistoryInIntervalWithFilter(
                Tag,
                earliestDate,
                latestDate,
                ct,
                numEntries,
                includeUnreliable,
                includeNumericOnly,
                floorEnabled: true,
                ceilingEnabled: true,
                ceiling: ceilingFilter,
                floor: floorFilter);

            return revHist;
        }

        public async Task<List<CygNetHistoryEntry>> GetLatestHistoryInIntervalFilterCeiling(DateTime earliestDate, DateTime latestDate, int? numEntries, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double ceilingFilter)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            var revHist = vhs.GetLatestHistoryInIntervalWithFilter(
                Tag,
                earliestDate,
                latestDate,
                ct,
                numEntries,
                includeUnreliable,
                includeNumericOnly,
                floorEnabled: false,
                ceilingEnabled: true,
                ceiling: ceilingFilter,
                floor: 0);

            return revHist;
        }

        public async Task<List<CygNetHistoryEntry>> GetLatestHistoryInIntervalFilterFloor(DateTime earliestDate, DateTime latestDate, int? numEntries, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double floorFilter)
        {
            var vhs = await CvsService.GetAssociatedVhs();

            if (vhs == null)
                throw new InvalidOperationException("Associated VHS service was not found");

            var revHist = vhs.GetLatestHistoryInIntervalWithFilter(
                Tag,
                earliestDate,
                latestDate,
                ct,
                numEntries,
                includeUnreliable,
                includeNumericOnly,
                floorEnabled: true,
                ceilingEnabled: false,
                ceiling: 0,
                floor: floorFilter);

            return revHist;
        }
    }
}