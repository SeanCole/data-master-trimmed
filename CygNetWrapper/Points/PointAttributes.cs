﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using CygNet.Data.Points;
using System.Reflection;
using CygNet.Data.Core;
using Serilog;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Points
{
    public class PointAttribute : IEquatable<PointAttribute>
    {
        public static Dictionary<PointAttribute, PointAttributeValue> GetTagAttributes(PointTag tag)
        {
            var dict = new Dictionary<PointAttribute, PointAttributeValue>
            {
                [AllPointAttributes["Tag-Site"]] = new PointAttributeValue(AllPointAttributes["Tag-Site"], tag.Site),
                [AllPointAttributes["Tag-Service"]] = new PointAttributeValue(AllPointAttributes["Tag-Service"], tag.Service),
                [AllPointAttributes["Tag-SiteService"]] = new PointAttributeValue(AllPointAttributes["Tag-SiteService"], tag.SiteService),
                [AllPointAttributes["Tag-PointId"]] = new PointAttributeValue(AllPointAttributes["Tag-PointId"], tag.PointId),
                [AllPointAttributes["Tag-SiteServPointId"]] = new PointAttributeValue(AllPointAttributes["Tag-SiteServPointId"], tag.GetTagPointId()),
                [AllPointAttributes["Tag-FacilityId"]] = new PointAttributeValue(AllPointAttributes["Tag-FacilityId"], tag.FacilityId),
                [AllPointAttributes["Tag-Udc"]] = new PointAttributeValue(AllPointAttributes["Tag-Udc"], tag.UDC),
                [AllPointAttributes["Tag-FacUdc"]] = new PointAttributeValue(AllPointAttributes["Tag-FacUdc"], $"{tag.FacilityId}_{tag.UDC}"),
                [AllPointAttributes["Tag-SiteServFacUdc"]] = new PointAttributeValue(AllPointAttributes["Tag-SiteServFacUdc"], tag.GetTagFacilityUDC()),
                [AllPointAttributes["Tag-PointIdLong"]] = new PointAttributeValue(AllPointAttributes["Tag-PointIdLong"], tag.LongId),
                [AllPointAttributes["Tag-SiteServPointIdLong"]] = new PointAttributeValue(AllPointAttributes["Tag-SiteServPointIdLong"], tag.GetTagPointIdLong()),
                [AllPointAttributes["Tag-Full"]] = new PointAttributeValue(AllPointAttributes["Tag-Full"], tag.GetTagPointIdFull())
            };
            return dict;
        }

        static PointAttribute()
        {
            AllPointAttributes = new Dictionary<string, PointAttribute>
                {
                    // Add tag attributes
                    { "Tag-Site", new PointAttribute("[n/a]", "Tag-Site", "[Site]", "Tag", "String", null, null, null, null) },
                    { "Tag-Service", new PointAttribute("[n/a]", "Tag-Service", "[Service]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteService", new PointAttribute("[n/a]", "Tag-SiteService", "[Site].[Service]", "Tag", "String", null, null, null, null) },
                    { "Tag-PointId", new PointAttribute("[n/a]", "Tag-PointId", "[PointId]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteServPointId", new PointAttribute("[n/a]", "Tag-SiteServPointId", "[Site].[Service].[PointId]", "Tag", "String", null, null, null, null) },
                    { "Tag-FacilityId", new PointAttribute("[n/a]", "Tag-FacilityId", "[FacilityId]", "Tag", "String", null, null, null, null) },
                    { "Tag-Udc", new PointAttribute("[n/a]", "Tag-Udc", "[UDC]", "Tag", "String", null, null, null, null) },
                    { "Tag-FacUdc", new PointAttribute("[n/a]", "Tag-FacUdc", "[FacilityId]_[UDC]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteServFacUdc", new PointAttribute("[n/a]", "Tag-SiteServFacUdc", "[Site].[Service]::[FacilityId].[UDC]", "Tag", "String", null, null, null, null) },
                    { "Tag-PointIdLong", new PointAttribute("[n/a]", "Tag-PointIdLong", "[PointIdLong]", "Tag", "String", null, null, null, null) },
                    { "Tag-SiteServPointIdLong", new PointAttribute("[n/a]", "Tag-SiteServPointIdLong", "[Site].[Service]:[PointIdLong]", "Tag", "String", null, null, null, null) },
                    { "Tag-Full", new PointAttribute("[n/a]", "Tag-Full", "[Site].[Service].[PointId]:[PointIdLong]", "Tag", "String", null, null, null, null) }
                };

            // Parse included tab-delimited list
            var assembly = Assembly.GetExecutingAssembly();

            var resourceName = "Techneaux.CygNetWrapper.Points.PointAttributes.tab";

            using (var stream = assembly.GetManifestResourceStream(resourceName))
            using (var sr = new StreamReader(stream))
            {
                // Skip header row
                sr.ReadLine();

                while (!sr.EndOfStream)
                {
                    var thisLine = sr.ReadLine();

                    var lineItems = thisLine.Split('\t');

                    try
                    {
                        if (Convert.ToBoolean(lineItems[(int)SourceColumnIndexes.Supported]))
                        {
                            int? thisIndex = null;
                            if (int.TryParse(lineItems[(int)SourceColumnIndexes.Index], out var testIndex))
                                thisIndex = testIndex;

                            var newAttr = new PointAttribute(
                                    lineItems[(int)SourceColumnIndexes.SqlColumnName],
                                    lineItems[(int)SourceColumnIndexes.ClassPropertyName],
                                    lineItems[(int)SourceColumnIndexes.Description],
                                    lineItems[(int)SourceColumnIndexes.Category],
                                    lineItems[(int)SourceColumnIndexes.DataType],
                                    lineItems[(int)SourceColumnIndexes.EnumName],
                                    thisIndex,
                                    lineItems[(int)SourceColumnIndexes.SqlRawValues],
                                    lineItems[(int)SourceColumnIndexes.EnumEquivalentIndices]);

                            // Validate that class property with the associated name exists
                            //Debug.Assert(TestPntRecord.HasProperty(NewAttr.ClassPropertyName));

                            // CODE HERE => Check for properties existing, getting null ref exception in the nested properties

                            //if (TestPntRecord.HasProperty(NewAttr.ClassPropertyName))
                            //{
                            AllPointAttributes.Add(newAttr.Id, newAttr);
                            //}
                        }
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Fatal(ex, "Fatal unhandled exception while loading internal point attribute definitions");
                    }
                }
            }
        }

        protected enum SourceColumnIndexes
        {
            Supported,
            SqlColumnName,
            ClassPropertyName,
            Category,
            Description,
            DataType,
            EnumName,
            Index,
            SqlRawValues,
            EnumEquivalentIndices,
            Notes
        }

        public static Dictionary<string, PointAttribute> AllPointAttributes { get; private set; }

        protected PointAttribute(
            string newSqlColumnName,
            string newClassPropertyName,
            string newAttrDesc,
            string newCategory,
            string newValueType,
            string newEnumName,
            int? newEnumIndex,
            string newSqlRawValues,
            string newEnumEquivalentInt)
        {
            Debug.Assert(!string.IsNullOrEmpty(newSqlColumnName), "Sql column name must not be blank");
            Debug.Assert(!string.IsNullOrEmpty(newClassPropertyName), "Class property name must not be blank");
            Debug.Assert(!string.IsNullOrEmpty(newAttrDesc), "Description must not be blank");
            Debug.Assert(!string.IsNullOrEmpty(newCategory), "Category must not be blank");
            Debug.Assert(!string.IsNullOrEmpty(newValueType), "Value type must not be blank");

            SqlColumnName = newSqlColumnName;
            ClassPropertyName = newClassPropertyName;
            Description = newAttrDesc;

            if (Enum.TryParse(newCategory, out Categories parsedCategory))
            {
                Category = parsedCategory;
            }
            else
            {
                Category = Categories.Uncategorized;
            }

            if (Enum.TryParse(newValueType, out AttrValueType parsedValType))
            {
                ValueType = parsedValType;
            }
            else
            {
                ValueType = AttrValueType.String;
            }

            if (!string.IsNullOrEmpty(newEnumName))
                EnumName = newEnumName;

            EnumClassIndex = newEnumIndex;

            if (!string.IsNullOrEmpty(newSqlRawValues) && !string.IsNullOrEmpty(newEnumEquivalentInt))
            {
                var sqlVals = newSqlRawValues.Split('|').ToList();
                var enumVals = newEnumEquivalentInt.Split('|').Select(index => Convert.ToInt32(index)).ToList();

                for (var i = 0; i < Math.Max(sqlVals.Count, enumVals.Count); i++)
                {
                    _sqlToEnumConversionDictionary.Add(sqlVals[i], enumVals[i]);
                    _enumToSqlConversionDictionary.Add(enumVals[i], sqlVals[i]);
                }
            }


            var indexNum = "";
            if (EnumClassIndex.HasValue)
                indexNum = "_" + EnumClassIndex;

            Id = $"{ClassPropertyName}{indexNum}";

            ExpressionId = $"pnt_{Id}".ToLower();
        }

        public enum AttrValueType
        {
            Enum,
            Bool,
            Byte,
            UnsignedShort,
            UnsignedInt,
            Double,
            String,
            TimeSpanFromSeconds,
            TimeSpanFromMinutes,
            DateTime
        }

        public enum Categories
        {
            Tag,
            General,
            Alarm,
            History,
            Scaling,
            Reference,
            Application,
            Script,
            Uncategorized
        }

        public AttrValueType ValueType { get; }
        public string SqlColumnName { get; }
        public string ClassPropertyName { get; }
        public string Description { get; }

        public Categories Category { get; }

        public string EnumName { get; }
        public int? EnumClassIndex { get; }

        private readonly Dictionary<string, int> _sqlToEnumConversionDictionary = new Dictionary<string, int>();
        private readonly Dictionary<int, string> _enumToSqlConversionDictionary = new Dictionary<int, string>();

        public string ExpressionId { get; }

        public string Id { get; }

        public static PointAttributeValue ConvertSqlToStandardValue(string sqlColName, object sqlValue)
        {
            var thisAttr = AllPointAttributes.Values.FirstOrDefault(attr => attr.SqlColumnName == sqlColName);
            if (thisAttr == null)
            {
                return null;
            }

            object convertedAttributeValue;

            switch (thisAttr.ValueType)
            {
                case AttrValueType.Enum:
                    // Must convert sql character to enum int using attribute dictionary
                    // -- Enum name must not be null
                    Debug.Assert(!string.IsNullOrEmpty(thisAttr.EnumName));

                    // -- SQL value must be in the conversion dictionary
                    Debug.Assert(thisAttr._sqlToEnumConversionDictionary.ContainsKey(sqlValue.ToString()));

                    if (!string.IsNullOrEmpty(thisAttr.EnumName) && thisAttr._sqlToEnumConversionDictionary.ContainsKey(sqlValue.ToString()))
                    {
                        var enumIndex = thisAttr._sqlToEnumConversionDictionary[sqlValue.ToString()];

                        switch (thisAttr.EnumName)
                        {
                            case "AllowedUpdates":
                                convertedAttributeValue = ((AllowedUpdates)enumIndex).ToString();
                                break;
                            case "CompressionType":
                                convertedAttributeValue = ((CompressionType)enumIndex).ToString();
                                break;
                            case "ForceHistoryAction":
                                convertedAttributeValue = ((ForceHistoryAction)enumIndex).ToString();
                                break;
                            case "HyperPointType":
                                convertedAttributeValue = ((HyperPointType)enumIndex).ToString();
                                break;
                            case "InitialValueType":
                                convertedAttributeValue = ((InitialValueType)enumIndex).ToString();
                                break;
                            case "ReportDelayUnits":
                                convertedAttributeValue = ((ReportDelayUnits)enumIndex).ToString();
                                break;
                            default:
                                // Enum table name not recognized
                                // Perform default action here, return null
                                Debug.Fail("Enum table name not recognized");

                                return new PointAttributeValue(thisAttr, AttributeValueResults.PointAttrEnumConversionFailed);
                        }
                    }
                    else
                    {
                        // Default action here in case of error
                        return new PointAttributeValue(thisAttr, AttributeValueResults.PointAttrEnumConversionFailed);
                    }
                    break;
                case AttrValueType.Bool:
                    // Must be able to parse string to character
                    char tempChar;
                    Debug.Assert(char.TryParse(sqlValue.ToString(), out tempChar));

                    if (char.TryParse(sqlValue.ToString(), out tempChar))
                    {
                        convertedAttributeValue = (tempChar == 'Y');
                    }
                    else
                    {
                        // Return value of false
                        convertedAttributeValue = false;
                    }
                    break;
                case AttrValueType.Byte:
                    // Must be able to convert to byte
                    byte tempByte;
                    Debug.Assert(byte.TryParse(sqlValue.ToString(), out tempByte));

                    if (byte.TryParse(sqlValue.ToString(), out tempByte))
                    {
                        convertedAttributeValue = tempByte;
                    }
                    else
                    {
                        convertedAttributeValue = byte.MinValue;
                    }
                    break;
                case AttrValueType.UnsignedShort:
                    // Must be able to convert to unsigned short
                    ushort tempUShort;
                    Debug.Assert(ushort.TryParse(sqlValue.ToString(), out tempUShort));

                    if (ushort.TryParse(sqlValue.ToString(), out tempUShort))
                    {
                        convertedAttributeValue = tempUShort;
                    }
                    else
                    {
                        convertedAttributeValue = ushort.MinValue;
                    }
                    break;
                case AttrValueType.UnsignedInt:
                    // Must be able to convert to unsigned integer
                    uint tempUInt;
                    Debug.Assert(uint.TryParse(sqlValue.ToString(), out tempUInt));

                    if (uint.TryParse(sqlValue.ToString(), out tempUInt))
                    {
                        convertedAttributeValue = tempUInt;
                    }
                    else
                    {
                        convertedAttributeValue = uint.MinValue;
                    }
                    break;
                case AttrValueType.Double:
                    // Must be able to convert to double
                    double tempDbl;
                    Debug.Assert(double.TryParse(sqlValue.ToString(), out tempDbl));

                    if (double.TryParse(sqlValue.ToString(), out tempDbl))
                    {
                        convertedAttributeValue = tempDbl;
                    }
                    else
                    {
                        convertedAttributeValue = double.NaN;
                    }
                    break;
                case AttrValueType.String:
                    convertedAttributeValue = sqlValue.ToString();
                    break;
                case AttrValueType.TimeSpanFromSeconds:
                    // Must be able to convert to unsigned integer
                    uint tempSecs;
                    Debug.Assert(uint.TryParse(sqlValue.ToString(), out tempSecs));

                    if (uint.TryParse(sqlValue.ToString(), out tempSecs))
                    {
                        convertedAttributeValue = TimeSpan.FromSeconds(tempSecs);
                    }
                    else
                    {
                        convertedAttributeValue = new TimeSpan(0);
                    }
                    break;
                case AttrValueType.TimeSpanFromMinutes:
                    // Must be able to convert to unsigned integer
                    uint tempMins;
                    Debug.Assert(uint.TryParse(sqlValue.ToString(), out tempMins));

                    if (uint.TryParse(sqlValue.ToString(), out tempMins))
                    {
                        convertedAttributeValue = TimeSpan.FromMinutes(tempMins);
                    }
                    else
                    {
                        convertedAttributeValue = new TimeSpan(0);
                    }
                    break;
                case AttrValueType.DateTime:
                    // Must be able to convert to unsigned integer
                    DateTime tempDateTime;
                    Debug.Assert(DateTime.TryParse(sqlValue.ToString(), out tempDateTime));

                    if (DateTime.TryParse(sqlValue.ToString(), out tempDateTime))
                    {
                        convertedAttributeValue = tempDateTime;
                    }
                    else
                    {
                        convertedAttributeValue = new DateTime();
                    }
                    break;
                default:
                    // Default, return null
                    return null;
            }

            return new PointAttributeValue(thisAttr, convertedAttributeValue);
        }

        public override bool Equals(object obj)
        {
            if (obj != null && obj is PointAttribute compAttr)
            {
                return Equals(compAttr);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return Id;
        }

        public bool Equals(PointAttribute other)
        {
            if (ReferenceEquals(this, other))
                return true;

            if (other != null)
                return Id == other.Id;
            else
                return false;
        }

        public static bool operator ==(PointAttribute attr1, PointAttribute attr2)
        {
            if (ReferenceEquals(attr1, attr2))
            {
                return true;
            }

            if (attr1 is null || attr2 is null)
            {
                return false;
            }

            return (attr1.Id == attr2.Id);
        }

        public static bool operator !=(PointAttribute attr1, PointAttribute attr2)
        {
            return !(attr1 == attr2);
        }

        //public static object ConvertStandardValueToSql(PointAttribute thisAttr, object standardValue)
        //{
        //}
    }
}
