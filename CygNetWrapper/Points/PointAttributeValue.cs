﻿using CygNet.Data.Core;
using System;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Points
{   
    public class PointAttributeValue : ConvertibleValue, IAttributeValue
    {
        public AttributeValueResults ValueAvailabilityStatus { get; }
        public Exception ThisValueException { get; }

        public PointAttribute ThisPointAttribute { get; }

        public string AttributeId => ThisPointAttribute?.Id;

        public string AttributeDesc => ThisPointAttribute?.Description;

        public PointAttributeValue(PointAttribute pntAttr, AttributeValueResults thisResult, Exception ex = null) : base(null)
        {
            ThisPointAttribute = pntAttr;

            if (thisResult == AttributeValueResults.ValueAvailable)
                throw new ArgumentException("This constructor cannot be called with a <Value Available> status. It is for all other types of statuses.");

            ValueAvailabilityStatus = thisResult;
            ThisValueException = ex;
        }

        public PointAttributeValue(PointAttribute pntAttr, PointTag tag) : base (GetTagValueString(pntAttr, tag))
        {
            // This is for building tag attribute values
            ThisPointAttribute = pntAttr;
        }

        private static string GetTagValueString(PointAttribute pntAttr, PointTag tag)
        {
            switch (pntAttr.Id)
            {
                case "Tag-Site":
                    return tag.Site;
                case "Tag-Service":
                    return tag.Service;
                case "Tag-SiteService":
                    return tag.SiteService.ToString();
                case "Tag-PointId":
                    return tag.PointId;
                case "Tag-SiteServPointId":
                    return tag.GetTagPointId();
                case "Tag-FacilityId":
                    return tag.FacilityId;
                case "Tag-Udc":
                    return tag.UDC;
                case "Tag-FacUdc":
                    return $"{tag.FacilityId}_{tag.UDC}";
                case "Tag-SiteServFacUdc":
                    return tag.GetTagFacilityUDC();
                case "Tag-PointIdLong":
                    return tag.LongId;
                case "Tag-SiteServPointIdLong":
                    return tag.GetTagPointIdLong();
                case "Tag-Full":
                    return tag.GetTagPointIdFull();
                default:
                    return tag.ToString();
            }
        }

        public PointAttributeValue(PointAttribute pntAttr, object newVal) : base(newVal)
        {
            ThisPointAttribute = pntAttr;

            ValueAvailabilityStatus = AttributeValueResults.ValueAvailable;
        }

        public override string ToString()
        {
            if (ValueAvailabilityStatus == AttributeValueResults.ValueAvailable)
            {
                if (RawValue is string)
                {
                    return (string)RawValue;
                }
                else
                {
                    return RawValue.ToString();
                }
            }
            else
            {
                return $"[{ValueAvailabilityStatus.ToString()}]";
            }
        }
    }

}