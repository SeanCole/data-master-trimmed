﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using CygNet.API.Points;
using CygNet.Data.Core;
using Techneaux.CygNetWrapper.Services;
using Techneaux.CygNetWrapper.Services.PNT;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Points
{
    public class PointServicePointTagCache
    {
        public PointService ParentPntService { get; private set; }

        private ConcurrentDictionary<FacilityTag, Dictionary<string, PointTag>> FacilityKeyedDictionary = new ConcurrentDictionary<FacilityTag, Dictionary<string, PointTag>>();
        private ConcurrentDictionary<string, Dictionary<FacilityTag, PointTag>> UdcKeyedDictionary = new ConcurrentDictionary<string, Dictionary<FacilityTag, PointTag>>();

        public DateTime TimeCacheLastUpdated { get; private set; }
        public TimeSpan TimeBetweenUpdateChecks { get; set; } = TimeSpan.FromMinutes(10);

        public PointServicePointTagCache(PointService cvs)
        {
            ParentPntService = cvs;

            TimeCacheLastUpdated = DateTime.Now.AddMinutes(-5);

            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = TimeBetweenUpdateChecks.TotalMilliseconds;
            aTimer.Enabled = true;

            UpdatePointCache();
        }

        private async void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            await UpdatePointCache();
        }

        private int numFullPointUpdates;


        public async Task<PointTag> GetPointTagAsync(FacilityTag facTag, string udc, CancellationToken ct)
        {
            if (FacilityKeyedDictionary.TryGetValue(facTag, out var pointDict))
            {
                return pointDict.TryGetValue(udc, out var tag) ? tag : null;
            }

            if (numFullPointUpdates > 0)
                return null;

            return await GetSingleUdc(facTag, udc, ct);
        }

        public async Task<List<PointTag>> GetPointTagsAsync(FacilityTag facTag, List<string> udcs, CancellationToken ct)
        {
            var pointsFound = new ConcurrentBag<PointTag>();

            if (numFullPointUpdates > 0)
            {
                if (FacilityKeyedDictionary.TryGetValue(facTag, out var pointDict))
                {
                    var tagList = new List<PointTag>();
                    foreach (var udc in udcs)
                    {
                        if (pointDict.TryGetValue(udc, out var tag))
                        {
                            tagList.Add(tag);
                        }
                    }

                    return tagList;
                }

                return new List<PointTag>();
            }

            await udcs.ForEachAsync<string>(10, async (udc) =>
            {
                var thisPoint = await GetPointTagAsync(facTag, udc, ct);
                if (thisPoint != null)
                    pointsFound.Add(thisPoint);
            });

            return pointsFound.ToList();
        }

        private async Task<PointTag> GetSingleUdc(FacilityTag facTag, string udc, CancellationToken ct)
        {
            // This method caches just one UDC

            try
            {
                var pntClient = new ConfigClient(ParentPntService.DomainSiteService);

                var cFilter = new CompoundFilterCriteria
                {

                    Operator = LogicalOperator.And,
                    Filters = {
                        new FilterCriteria
                        {
                            PropertyId = CygNetCoreProperties.CygNetProperty.UDC,
                            Value = "PRCASXIN"
                        }
                    },
                };

                var allPointTags = await Task.Run(() => pntClient.GetPointTagList(cFilter, ct), ct);

                var facKeyedDictTask = Task.Run(() => UpdateSingleToFacilityKeyedDictionary(allPointTags), ct);
                var udcKeyedDictTask = Task.Run(() => AddSingleToUdcKeyedDictionary(allPointTags), ct);

                ct.ThrowIfCancellationRequested();

                await Task.WhenAll(facKeyedDictTask, udcKeyedDictTask);

                return allPointTags.FirstOrDefault(pt => pt.GetFacilityTag() == facTag);
            }
            catch (OperationCanceledException)
            {
                return null;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);

                return null;
            }
        }

        private readonly SemaphoreSlim _lockCacheUpdates = new SemaphoreSlim(1, 1);
        private async Task UpdatePointCache()
        {
            try
            {
                await _lockCacheUpdates.WaitAsync();

                var pntClient = new ConfigClient(ParentPntService.DomainSiteService);
                var allPointTags = await Task.Run(() => pntClient.GetPointTagList("", CancellationToken.None));

                var facKeyedDictTask = Task.Run(() => UpdateFacilityKeyedDictionary(allPointTags));
                var udcKeyedDictTask = Task.Run(() => UpdateUdcKeyedDictionary(allPointTags));

                await Task.WhenAll(facKeyedDictTask, udcKeyedDictTask);

                Console.WriteLine($"********* Point Sync Completed ************ {allPointTags.Count}");

                TimeCacheLastUpdated = DateTime.Now;
                Interlocked.Increment(ref numFullPointUpdates);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _lockCacheUpdates.Release();
            }
        }

        private void UpdateSingleToFacilityKeyedDictionary(IList<PointTag> srcPointTags)
        {
            // Stage items in a holding dictionary
            foreach (var pointTag in srcPointTags)
            {
                var thisFacTag = pointTag.GetFacilityTag() ?? new FacilityTag(pointTag.SiteService, "NULL");
                var thisUdc = pointTag.UDC ?? "";
                
                if (!FacilityKeyedDictionary.TryGetValue(thisFacTag, out var udcDictionary))
                {
                    udcDictionary = new Dictionary<string, PointTag>();

                    FacilityKeyedDictionary[thisFacTag] = udcDictionary;

                    udcDictionary[thisUdc] = pointTag;
                }
            }
        }


        private void UpdateFacilityKeyedDictionary(IList<PointTag> srcPointTags)
        {
            var stagingFacKeyedDictionary = new Dictionary<FacilityTag, Dictionary<string, PointTag>>();

            // Stage items in a holding dictionary
            foreach (var pointTag in srcPointTags)
            {
                var thisFacTag = pointTag.GetFacilityTag() ?? new FacilityTag(pointTag.SiteService, "NULL");
                var thisUdc = pointTag.UDC ?? "";
                
                if (!stagingFacKeyedDictionary.TryGetValue(thisFacTag, out var udcDictionary))
                {
                    udcDictionary = new Dictionary<string, PointTag>();

                    stagingFacKeyedDictionary[thisFacTag] = udcDictionary;
                }

                if (!udcDictionary.ContainsKey(thisUdc))
                {
                    udcDictionary[thisUdc] = pointTag;
                }
            }

            // Update main dictionary
            var allCurrentFacTags = new HashSet<FacilityTag>(FacilityKeyedDictionary.Keys);

            // -- Replace all other internal dictionaries
            foreach (var key in stagingFacKeyedDictionary.Keys)
            {
                FacilityKeyedDictionary[key] = stagingFacKeyedDictionary[key];
            }

            var facTagsToRemove = allCurrentFacTags.Except(stagingFacKeyedDictionary.Keys);
            // -- Remove tags that no longer exist since last check
            foreach (var facilityTag in facTagsToRemove)
            {
                FacilityKeyedDictionary.TryRemove(facilityTag, out _);
            }

        }

        private void AddSingleToUdcKeyedDictionary(IList<PointTag> srcPointTags)
        {
            // Stage items in a holding dictionary
            foreach (var pointTag in srcPointTags)
            {
                var thisFacTag = pointTag.GetFacilityTag() ?? new FacilityTag(pointTag.SiteService, "NULL");
                var thisUdc = pointTag.UDC ?? "";

                if (!UdcKeyedDictionary.TryGetValue(thisUdc, out var facTagDictionary))
                {
                    facTagDictionary = new Dictionary<FacilityTag, PointTag>();

                    UdcKeyedDictionary[thisUdc] = facTagDictionary;

                    facTagDictionary[thisFacTag] = pointTag;
                }
            }
        }


        private void UpdateUdcKeyedDictionary(IList<PointTag> srcPointTags)
        {
            var stagingUdcKeyedDictionary = new Dictionary<string, Dictionary<FacilityTag, PointTag>>();

            // Stage items in a holding dictionary
            foreach (var pointTag in srcPointTags)
            {
                var thisFacTag = pointTag.GetFacilityTag() ?? new FacilityTag(pointTag.SiteService, "NULL");
                var thisUdc = pointTag.UDC ?? "";

                if (!stagingUdcKeyedDictionary.TryGetValue(thisUdc, out var facTagDictionary))
                {
                    facTagDictionary = new Dictionary<FacilityTag, PointTag>();

                    stagingUdcKeyedDictionary[thisUdc] = facTagDictionary;
                }

                if (!facTagDictionary.ContainsKey(thisFacTag))
                {
                    facTagDictionary[thisFacTag] = pointTag;
                }
            }

            // Update main dictionary
            var allCurrentUdcs = new HashSet<string>(UdcKeyedDictionary.Keys);

            // -- Replace all other internal dictionaries
            foreach (var key in stagingUdcKeyedDictionary.Keys)
            {
                UdcKeyedDictionary[key] = stagingUdcKeyedDictionary[key];
            }

            var udcsToRemove = allCurrentUdcs.Except(stagingUdcKeyedDictionary.Keys);

            // -- Remove tags that no longer exist since last check
            foreach (var udc in udcsToRemove)
            {
                UdcKeyedDictionary.TryRemove(udc, out _);
            }
        }


        private HashSet<string> DistinctUdcs { get; }



        //public Dictionary<string, Dictionary<string,PointTag>> FacilityKeyedDictionary = new Dictionary<string, Dictionary<string, PointTag>>();

        //public bool FacilityHasUdc(FacilityTag facTag, string udc)
        //{
        //    return TryGetPointTagForFacility(facTag, udc, out _);
        //}

        //public bool TryGetPointTagForFacility(FacilityTag facTag, string udc, out PointTag tag)
        //{
        //    tag = null;
        //    if (facTag.SiteService == ParentCurrentValueService.SiteService)
        //    {
        //        return FacilityKeyedDictionary.TryGetValue(facTag.FacilityId, out var tagDictForFacility) && 
        //               tagDictForFacility.TryGetValue(udc, out tag);
        //    }

        //    return false;
        //}




    }

}
