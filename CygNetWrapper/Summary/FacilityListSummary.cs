﻿using System.Collections.Generic;
using System.Linq;
using CygNet.Data.Core;
using Techneaux.CygNetWrapper.Facilities;

namespace Techneaux.CygNetWrapper.Summary
{
    public static class FacilityListSummary
    {
        public static List<FacilitySummaryResult> GetSummaryResults(IEnumerable<ICygNetFacility> srcFacList)
        {
            var summaryDict = new Dictionary<string, FacilitySummaryResult>();

            foreach (var fac in srcFacList)
            {
                var key = $"{fac.CvsDomainSiteService}-{fac.Type}";

                if (summaryDict.TryGetValue(key, out var thisSummary))
                {
                    thisSummary.Count++;
                }
                else
                {
                    summaryDict[key] = new FacilitySummaryResult()
                    {
                        CvcSiteService = fac.CvsSiteService,
                        FacilityType = fac.Type,
                        Count = 1
                    };
                }
            }

            return summaryDict.Values.ToList();
        }
        
        public class FacilitySummaryResult
        {
            public SiteService CvcSiteService { get; set; }
            public string FacilityType { get; set; }
            public int Count { get; set; }

            public override string ToString()
            {
                return $"{CvcSiteService}-{FacilityType}";
            }
        }
    }
}
