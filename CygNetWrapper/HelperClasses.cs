﻿using System.ComponentModel;

namespace Techneaux.CygNetWrapper
{
    public enum AttributeValueResults
    {
        [Description("Value was found either in cache or was retrieved from a facility/point record obj")]
        ValueAvailable,

        [Description("Value not available because facility attribute was disabled, otherwise, no error")]
        FacilityAttributeDisabled,

        [Description("Attribute sql value could not be converted from point property enumeration table")]
        PointAttrEnumConversionFailed,

        [Description("There was a general failure converting an SQL point attribute value to a standard value")]
        GeneralPointAttrSqlConversionFailure,

        [Description("Attribute Id was not found in the cache. This occurs when the attribute wasn't pre-cached.")]
        AttributeNotCached,

        [Description("Exception occured while getting the value")]
        ExceptionWhileRetrievingValue
    }

    public static class HelperFunctions
    {
       
    }

}
