@echo off

> %~dp0log.txt (
	echo Starting...
	
	for %%x in (%~dp0*.dll) do (
		regsvr32 %%x /s
		if %ErrorLevel% NEQ 0 echo %%x / %ErrorLevel%
	)

	echo Done.
)

