﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Techneaux.CygNetWrapper
{
    public static class Notes
    {
        public static string GetFacilityNote(string facTag, string uisService, DateTime runDate, string noteType)
        {
            if (facTag == "" || noteType == "")
                return null;                              
            CxNoteLib.NoteClient NoteObj = new CxNoteLib.NoteClient();
            string[] arrSplit = uisService.Split('.');
            NoteObj.Connect(arrSplit[0] + ".NOTE");
            CxNoteLib.Note TempNote = new CxNoteLib.Note();
            TempNote.StartDateTime = runDate;
            TempNote.Body = "";
            CxNoteLib.Association[] arrAssocs = new CxNoteLib.Association[1];
            arrAssocs[0] = new CxNoteLib.Association();
            arrAssocs[0].SetAssociation(facTag, 0);

            Array NoteArray = NoteObj.GetNotesByAssociations(arrAssocs, runDate, runDate.AddDays(1), noteType, true);
            foreach (CxNoteLib.Note Note in NoteArray)
            {
                if (Note.StartDateTime > TempNote.StartDateTime)
                {
                    TempNote = Note;
                }
            }
            if (TempNote.Body != "")
            {
                if (TempNote.Body.Replace("/r/n", " ").Replace(",", " ").Replace(Environment.NewLine, " ").Length <= 239)
                    return TempNote.Body.Replace("/r/n", " ").Replace(",", " ").Replace(Environment.NewLine, " ");
                else
                    return TempNote.Body.Replace("/r/n", " ").Replace(",", " ").Replace(Environment.NewLine, " ").Substring(0, 239);

            }
            else
            {
                return null;
            }
        }


    }
}
