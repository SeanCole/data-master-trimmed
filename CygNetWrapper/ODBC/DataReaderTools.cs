﻿using CygNet.Data.Facilities;
using System.Collections.Generic;
using System.Data.Common;

namespace Techneaux.CygNetWrapper.ODBC
{
    public static class DataReaderTools
    {
        public static List<AttributeDefinition> GetEnabledAttributesFromDbReader(DbDataReader dbReader, Dictionary<string, AttributeDefinition> enabledAttributes)
        {
            // Get columns which are attribute values
            var attrColNames = new List<AttributeDefinition>();
            for (var i = 0; i < dbReader.FieldCount; i++)
            {
                if (enabledAttributes.ContainsKey(dbReader.GetName(i)))
                {
                    attrColNames.Add(enabledAttributes[dbReader.GetName(i)]);
                }
            }

            return attrColNames;
        }
    }
}
