﻿using System;
using System.Data.Odbc;
using System.Threading.Tasks;
using Serilog;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.ODBC
{
    public class CygNetOdbcConnection
    {
        private const string Dsn = "DSN=CygNet;";

        public static async Task<bool> TestOdbcConnection()
        {
            try
            {
                using (var dbConnection = new OdbcConnection(Dsn))
                {
                    await dbConnection.OpenAsync();
                }
                return true;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Warning(ex, "Odbc connection failed");

                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.AdminEvent))
                    Log.Warning(ex, "Failed to establish ODBC connection to CygNet driver. Please make sure the 64-bit ODBC driver is installed and a client connection is available.");

                return false;
            }
        }

        public const string DsnString = "DSN=CygNet;Uid=<username>;Pwd=<password>;";
    }
}
