﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Techneaux.CygNetWrapper
{
    public class CygNetConvertableValueX : ConvertableValue
    {
        public CygNetConvertableValueX(object newVal) : base(newVal)
        {
            ErrorState = ErrorType.NONE;
        }
        public CygNetConvertableValueX(ErrorType errorTypeRef, string ErrorDescriptionRef) : base(null)
        {
            ErrorState = errorTypeRef;
            ErrorDescription = ErrorDescriptionRef;
        }

        public enum ErrorType
        {
            [Description("No Error")]
            NONE,
            [Description("Facility Error")]
            FacError,
            [Description("Attribute Error")]
            AttrError,
            [Description("Point Error")]
            PointErr,
            [Description("Formatting Error")]
            FormattingErr,
            [Description("Expression Error")]
            ExpressionErr,
            [Description("Uknown Error")]
            UknownErr,
            [Description("Blank Error")]
            BlankErr
          

        }
        public ErrorType ErrorState { get; private set; }

        public string ErrorDescription { get; private set; }

        public bool HasError()
        {
            return ErrorState != ErrorType.NONE;
        }
    }
}
