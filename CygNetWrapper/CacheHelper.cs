﻿using System.Collections.Generic;

namespace Techneaux.CygNetWrapper
{
    public static class CacheHelper
    {
        public class StringPool
        {
            private readonly Dictionary<string, string> _thisPool = new Dictionary<string, string>();

            public string Add(string item)
            {
                if (!_thisPool.TryGetValue(item, out var retStr))
                {
                    _thisPool[item] = item;
                    retStr = item;
                }

                return retStr;
            }
        }
    }
}
