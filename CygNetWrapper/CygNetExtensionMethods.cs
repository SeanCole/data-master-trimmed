﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;

namespace Techneaux.CygNetWrapper
{
    public static class CygNetExtensionMethods
    {
       
        public static ReadOnlyCollection<string> GetColumnNames(this DbDataReader thisDbReader)
        {
            var colNames = new List<string>();
            for (var i = 0; i < thisDbReader.FieldCount; i++)
            {
                colNames.Add(thisDbReader.GetName(i));
            }
            
            return new ReadOnlyCollection<string>(colNames);
        }

        public static bool HasColumnName(this DbDataReader thisDbReader, string colName)
        {
            return thisDbReader.GetColumnNames().Contains(colName);
        }
    }
}
