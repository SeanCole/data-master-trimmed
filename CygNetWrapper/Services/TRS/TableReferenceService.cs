﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Odbc;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CygNet.Data.Facilities;
using CygNet.API.Core;
using CygNet.API.Facilities;
using System.Diagnostics;
using System.Collections.Concurrent;
using Serilog;
using Techneaux.CygNetWrapper.Points;
using Techneaux.CygNetWrapper.Facilities;
using Techneaux.CygNetWrapper.Facilities.Attributes;
using Techneaux.CygNetWrapper.Facilities.Filtering;
using Techneaux.CygNetWrapper.Services.PNT;
using CXDDSLib;
using Techneaux.CygNetWrapper.Services.DDS.Devices;
using XmlDataModelUtility;
using Nito.AsyncEx;
using System.Xml.Serialization;

namespace Techneaux.CygNetWrapper.Services.TRS
{
    public partial class TableReferenceService : CygNetService
    {
        const ServiceType ThisServiceType = ServiceType.TRS;

        private readonly AsyncLazy<GenericClient> _serviceCLient;

        public class UdcEntry
        {
            public string udc;
            public string category;
            public string description;
            public UdcEntry(string udcTag, string cat, string desc)
            {
                udc = udcTag;
                category = cat;
                desc = description;
            }
        }

        public async Task<GenericClient> GetServiceClient()
        {
            return await _serviceCLient;
        }
        //public override bool IsServiceAvailable => ServiceClient != null;
        public Table GetUdcTable()
        {
            var servInfo = new ServiceInformation();
            return servInfo.GetEnumerationTable(this.DomainSiteService, "~UDCALL");
        }
        public TableReferenceService(CygNetDomain parentDomain, ServiceDefinition TableServiceDef)
            : base(parentDomain, TableServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ThisServiceType)
                throw new ArgumentException("Service is wrong type");
            //AttrCache = new FacilityAttributeCache(this);

            //_serviceCLient = new AsyncLazy<DdsClient>(async () =>
            //    await Task.Run(() =>
            //    {
            //        var client = new DdsClient();
            //        client.Connect(SiteServiceName);

            //        return client;
            //    }));


            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

   
    }
}
