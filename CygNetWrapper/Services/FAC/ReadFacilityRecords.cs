﻿using CygNet.Data.Core;
using CygNet.Data.Facilities;

namespace Techneaux.CygNetWrapper.Services.FAC
{
    public partial class FacilityService : CygNetService
    {
        public FacilityRecord GetFacilityRecord(string tag)
        {
            return ServiceClient.ReadFacility(new FacilityTag(tag));
        }

        public FacilityRecord GetFacilityRecord(FacilityTag tag)
        {
            return ServiceClient.ReadFacility(tag);
        }
    }
}
