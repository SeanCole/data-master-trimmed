﻿using CygNet.Data.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using Techneaux.CygNetWrapper.Services.DDS;
using Techneaux.CygNetWrapper.Services.FAC;
using Techneaux.CygNetWrapper.Services.FMS;
using Techneaux.CygNetWrapper.Services.PNT;
using Techneaux.CygNetWrapper.Services.TRS;
using Techneaux.CygNetWrapper.Services.VHS;

namespace Techneaux.CygNetWrapper.Services
{
    public static class ServiceUtility
    {
        public static List<ServiceType> GetPossibleAssociationTypes(ServiceType srcType)
        {
            switch (srcType)
            {
                case ServiceType.ACS:
                    return new List<ServiceType>()
                    {
                        ServiceType.ACS,
                        ServiceType.AUD,
                        ServiceType.TRS,
                        ServiceType.ELS,
                        ServiceType.MSS
                    };
                case ServiceType.ARS:
                    return new List<ServiceType>()
                    {
                        ServiceType.ACS,
                        ServiceType.ELS,
                        ServiceType.MSS,
                        ServiceType.AUD
                    };
                case ServiceType.AUD:
                    return new List<ServiceType>()
                    {
                        ServiceType.ACS,
                        ServiceType.TRS,
                        ServiceType.ELS,
                        ServiceType.MSS
                    };
                case ServiceType.BSS:
                    return new List<ServiceType>()
                    {
                        ServiceType.ACS,
                        ServiceType.AUD,
                        ServiceType.TRS,
                        ServiceType.ELS,
                        ServiceType.MSS
                    };
                case ServiceType.CAS:
                    return new List<ServiceType>()
                    {
                        ServiceType.ELS,
                        ServiceType.MSS,
                        ServiceType.ACS,
                        ServiceType.AUD
                    };
                case ServiceType.CVS:
                    return new List<ServiceType>()
                    {
                        ServiceType.ACS,
                        ServiceType.AUD,
                        ServiceType.CAS,
                        ServiceType.ELS,
                        ServiceType.MSS,
                        ServiceType.FAC,
                        ServiceType.GNS,
                        ServiceType.NOTE,
                        ServiceType.TRS,
                        ServiceType.VHS
                    };
                case ServiceType.DBS:
                    return new List<ServiceType>()
                    {
                        ServiceType.ACS,
                        ServiceType.AUD,
                        ServiceType.TRS,
                        ServiceType.ELS,
                        ServiceType.MSS
                    };
                case ServiceType.DDS:

                case ServiceType.ELS:

                case ServiceType.FMS:

                case ServiceType.GMR:

                case ServiceType.GNS:

                case ServiceType.GRP:

                case ServiceType.FAC:

                case ServiceType.MSS:

                case ServiceType.NIL:

                case ServiceType.NOTE:

                case ServiceType.PNT:

                case ServiceType.RSM:

                case ServiceType.TRS:

                case ServiceType.UIS:

                case ServiceType.VHS:

                case ServiceType.AGENT:

                case ServiceType.UNKNOWN:
                default:
                    return Enum.GetValues(typeof(ServiceType)).Cast<ServiceType>().ToList();
            }
        }

        public static IEnumerable<CygNetService> GetCastServiceList(CygNetDomain parentDomain, List<ServiceDefinition> services, ServiceType type)
        {
            switch (type)
            {
                case ServiceType.CVS:
                case ServiceType.UIS:
                    return services.Select(serv => new CurrentValueService(parentDomain, serv));
                case ServiceType.FAC:
                    return services.Select(serv => new FacilityService(parentDomain, serv));
                case ServiceType.PNT:
                    return services.Select(serv => new PointService(parentDomain, serv));
                case ServiceType.ACS:
                case ServiceType.ARS:
                case ServiceType.AUD:
                case ServiceType.BSS:
                case ServiceType.CAS:
                case ServiceType.DBS:
                case ServiceType.DDS:
                case ServiceType.ELS:
                case ServiceType.FMS:
                case ServiceType.GMR:
                case ServiceType.GNS:
                case ServiceType.GRP:
                case ServiceType.MSS:
                case ServiceType.NIL:
                case ServiceType.NOTE:
                case ServiceType.RSM:
                case ServiceType.TRS:
                case ServiceType.VHS:
                case ServiceType.AGENT:
                case ServiceType.UNKNOWN:
                default:
                    return services.Select(serv => new CygNetService(parentDomain, serv));
            }
        }

        public static CygNetService GetDerivedService(CygNetDomain parentDomain, ServiceDefinition srcServ)
        {
            switch (srcServ.ServiceType)
            {
                case ServiceType.TRS:
                    return new TableReferenceService(parentDomain, srcServ);
                case ServiceType.CVS:
                    return new CurrentValueService(parentDomain, srcServ);

                case ServiceType.FMS:
                    return new FmsService(parentDomain, srcServ);

                case ServiceType.FAC:
                    return new FacilityService(parentDomain, srcServ);

                case ServiceType.PNT:
                    return new PointService(parentDomain, srcServ);

                case ServiceType.UIS:
                    return new CurrentValueService(parentDomain, srcServ);

                case ServiceType.VHS:
                    return new ValueHistoryService(parentDomain, srcServ);
                case ServiceType.DDS:
                    return new DeviceDefinitionService(parentDomain, srcServ);
                case ServiceType.ACS:
                case ServiceType.ARS:
                case ServiceType.AUD:
                case ServiceType.BSS:
                case ServiceType.CAS:
                case ServiceType.DBS:
              

                case ServiceType.ELS:
                case ServiceType.GMR:
                case ServiceType.GNS:
                case ServiceType.GRP:
                case ServiceType.MSS:
                case ServiceType.NIL:
                case ServiceType.NOTE:
                case ServiceType.RSM:

                case ServiceType.AGENT:
                case ServiceType.UNKNOWN:

                default:
                    return new CygNetService(parentDomain, srcServ);
            }
        }

    }






}
