﻿using CygNet.Data.Core;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using CxFmsLib;
using Serilog;
using System.Threading;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services.FMS
{
    public class FmsService : CygNetService
    {
        private const ServiceType ThisServiceType = ServiceType.FMS;

        public FmsService(CygNetDomain parentDomain, ServiceDefinition fmsServiceDef)
            : base(parentDomain, fmsServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ThisServiceType)
                throw new ArgumentException("Service is wrong type");
        }

        public FmsService(CygNetDomain parentDomain, DomainSiteService fmsServiceDef)
            : base(parentDomain, fmsServiceDef, ServiceType.FAC)
        {
            if (fmsServiceDef == null)
                throw new ArgumentException("Service cannot be null");

            // CODE HERE => Check service type
            if (ServiceDefinition.ServiceType != ServiceType.FAC)
                throw new ArgumentException("Service is wrong type");
        }

        private readonly FmsClient _serviceClient = new FmsClient();

        public FmsClient ServiceClient
        {
            get
            {
                if (_serviceClient.IsConnected) return _serviceClient;
                try
                {
                    _serviceClient.Connect(SiteService.ToString());

                    return _serviceClient;
                }
                catch (Exception ex)
                {
                    using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                        Log.Error(ex, $"Unhandled exception while creating service client for {DomainSiteService}");

                    return null;
                }
            }
        }

        public override bool IsServiceAvailable => ServiceClient != null && ServiceClient.IsConnected;

        //public async Task<HashSet<FacilityTag>> GetFacilityTagsInGroup(string grpName)
        //{
        //    if (!_isGroupDataRetrieved)
        //    {
        //        await GetGroupNamesAndNodes();
        //    }

        //    if (!_groupFacMap.TryGetValue(grpName, out var facs))
        //    {
        //        facs = new HashSet<FacilityTag>();
        //    }

        //    return facs;
        //}

        public async Task<FmsMeter> TryGetFmsMeterForFacility(FacilityTag srcTag)
        {
            return await TryGetFmsMeterForFacility(srcTag, false);
        }

        public async Task<FmsMeter> TryGetFmsMeterForFacility(FacilityTag srcTag, bool forceRefresh)
        {
            await Task.Run(() => GetGroupNamesAndNodes(forceRefresh));

            return _fmsFacilityInfo.TryGetValue(srcTag, out var mtr) ? mtr : null;
        }

        readonly Dictionary<FacilityTag, FmsMeter> _fmsFacilityInfo = new Dictionary<FacilityTag, FmsMeter>();
        //readonly Dictionary<string, HashSet<FacilityTag>> _groupFacMap = new Dictionary<string, HashSet<FacilityTag>>();
        //readonly Dictionary<string, HashSet<string>> _groupNodeMap = new Dictionary<string, HashSet<string>>();

        bool _isGroupDataRetrieved;

        readonly SemaphoreSlim _grpSemaphore = new SemaphoreSlim(1, 1);

        public async Task GetGroupNamesAndNodes(bool forceRefresh = false)
        {
            try
            {
                await _grpSemaphore.WaitAsync();

                if (_isGroupDataRetrieved && !forceRefresh)
                {
                    return;
                    //return _groupNodeMap;
                }



                //_groupNodeMap.Clear();


                ServiceClient.GetAllGroups(0, out var groupsObjs);

                var groupArray = (Array)groupsObjs;
                var groupSet = new HashSet<string>(groupArray.Cast<string>());

                //groupSet.Clear();
                //groupSet.Add("ACTIVE_METERS");

                Log.Debug($"Groups: {string.Join(",", groupSet)}");

                foreach (var grp in groupSet)
                {
                    try
                    {
                        var thisGrpFacSet = new HashSet<FacilityTag>();
                        //_groupFacMap[grp] = thisGrpFacSet;

                        var nodesObj = ServiceClient.GetNodesInGroup(grp, 0);
                        var nodesArray = (Array)nodesObj;
                        var nodesList = nodesArray.Cast<string>().ToList();

                        if (!nodesList.Any())
                            continue;

                        ServiceClient.GetFacTagsFromNodes(nodesArray, out var facTagsObj);

                        var facTagArray = (Array)facTagsObj;
                        var facTagList = facTagArray.Cast<string>().ToList();

                        Log.Debug($"FmsService: {DomainSiteService}, Node Array: {nodesList.Count}, Fac tag count: {facTagList.Count}");


                        for (var i = 0; i < facTagList.Count; i++)
                        {
                            if (string.IsNullOrWhiteSpace(facTagList[i])) continue;

                            var thisTag = new FacilityTag(facTagList[i]);

                            if (!_fmsFacilityInfo.TryGetValue(thisTag, out var thisFac))
                            {
                                thisFac = new FmsMeter(thisTag);
                                _fmsFacilityInfo[thisTag] = thisFac;
                            }

                            thisFac.AssociatedGroupIds.Add(grp);

                            //var nodes = nodesList.Select(id =>
                            //    new FmsNode {NodeId = id, NodeName = ServiceClient.GetNodeName(id)});

                            thisFac.AssociatedNodeNames.Add(nodesList[i]);

                            thisGrpFacSet.Add(thisTag);
                        }
                    }
                    catch (Exception ex)
                    {
                        using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                            Log.Error(ex, "Unhandled exception reading nodes IDs from FMS group with name ({group})", grp);
                    }
                }

                _isGroupDataRetrieved = true;
                Log.Debug($"FmsService: {DomainSiteService}, FacilityInfo Count: {_fmsFacilityInfo.Count}, with meters: {_fmsFacilityInfo.Values.Count(mtr => mtr.AssociatedNodeNames.Any())}");
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception reading group data from FMS {serv}", SiteServiceName);
            }
            finally
            {
                _grpSemaphore.Release();
            }

            //return _groupNodeMap;
        }
    }
}