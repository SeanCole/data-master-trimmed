﻿using CygNet.Data.Core;
using System.Collections.Generic;

namespace Techneaux.CygNetWrapper.Services.FMS
{
    public class FmsMeter
    {
        public FacilityTag FacTag { get; set; }

        public HashSet<string> AssociatedNodeNames { get; set; } = new HashSet<string>();

        public HashSet<string> AssociatedGroupIds { get; set; } = new HashSet<string>();

        public FmsMeter(FacilityTag newFacTag)
        {
            FacTag = newFacTag;
        }

        public FmsMeter() { }

        public override bool Equals(object obj)
        {
            //       
            // See the full list of guidelines at
            //   http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            //   http://go.microsoft.com/fwlink/?LinkId=85238
            //

            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            // TODO: write your implementation of Equals() here

            return FacTag.Equals(((FmsMeter)obj).FacTag);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            // TODO: write your implementation of GetHashCode() here

            return FacTag.GetHashCode();
        }

    }
}
