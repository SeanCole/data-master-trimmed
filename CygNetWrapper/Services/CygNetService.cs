﻿using System;
using System.Collections.Generic;
using System.Linq;
using CygNet.Data.Core;
using Nito.AsyncEx;
using System.Threading.Tasks;
using CygNet.API.Core;

namespace Techneaux.CygNetWrapper.Services
{
    public class CygNetService : ICygNetService
    {
        public CygNetService(CygNetDomain parentDomain, ServiceDefinition newServiceDef)
        {
            ParentDomain = parentDomain;
            ServiceDefinition = newServiceDef;

            InitLazyServices();
        }

        public virtual bool IsServiceAvailable => ServiceDefinition.ServiceStatus == ServiceStatus.OK;

        public CygNetService(CygNetDomain parentDomain, DomainSiteService newServiceDef, ServiceType newServiceType)
        {
            ParentDomain = parentDomain;
            ServiceDefinition = new ServiceDefinition()
            {
                SiteService = newServiceDef,
                ServiceType = newServiceType
            };
        }

        public DateTime GetServiceLocalTime()
        {
            var servInfoClient = new ServiceInformation();

            var ServTime = servInfoClient.GetCurrentTime(DomainSiteService);

            var CorrectedServiceTime = new DateTime(ServTime.Ticks, DateTimeKind.Local).Add(GetServiceTimezoneOffset());

            return CorrectedServiceTime;
        }

        public TimeSpan GetServiceTimezoneOffset()
        {
            TimeSpan Offset;
            var servInfoClient = new ServiceInformation();
            var minsOffset = servInfoClient.GetServiceInfo(DomainSiteService, new List<string>() {"UTC_OFFSET"});
            Offset = TimeSpan.FromMinutes(int.Parse(minsOffset.Values.First().ToString()));
            return Offset;
        }
        
        public CygNetDomain ParentDomain { get; }

        public ServiceDefinition ServiceDefinition { get; }

        public DomainSiteService DomainSiteService => ServiceDefinition.SiteService;
                
        public SiteService SiteService => ServiceDefinition.SiteService.SiteService;

        public string SiteServiceName => SiteService.ToString();
        public string DomainSiteServiceName => DomainSiteService.ToString();

        public string SqlSiteServiceName => SiteService.ToString().Replace('.', '_');

        public ushort DomainId => ParentDomain.DomainId;

        public ServiceType Type => ServiceDefinition.ServiceType;

        public ServiceStatus Status => ServiceDefinition.ServiceStatus;

        public bool IsRunning => Status == ServiceStatus.OK;

        private void InitLazyServices()
        {
            _assocTrs = new AsyncLazy<CygNetService>(async () => await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.TRS)));
            _assocAud = new AsyncLazy<CygNetService>(async () => await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.AUD)));
            _assocEls = new AsyncLazy<CygNetService>(async () => await Task.Run(() => ParentDomain.GetAssociatedService(this, ServiceType.ELS)));
        }

        private AsyncLazy<CygNetService> _assocTrs;
        public async Task<CygNetService> GetAssociatedTrsService() => await _assocTrs;

        private AsyncLazy<CygNetService> _assocAud;
        public async Task<CygNetService> GetAssociatedAudService() => await _assocAud;

        private AsyncLazy<CygNetService> _assocEls;
        public async Task<CygNetService> GetAssociatedElsService() => await _assocEls;

        public override string ToString() => DomainSiteService.ToString();

        public override bool Equals(object obj)
        {
            if(obj is CygNetService compServ)
            {
                return DomainSiteServiceName == compServ.DomainSiteServiceName;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return DomainSiteServiceName.GetHashCode();
        }
    }
}
