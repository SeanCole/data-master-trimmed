﻿using CygNet.Data.Core;
using CygNet.Data.Historian;
using System;
using TechneauxUtility;

namespace Techneaux.CygNetWrapper.Services.VHS
{
    public class CygNetHistoryEntry
    {
        public enum TimeStampType
        {
            Utc,
            LocalClient,
            Server
        }

        public const TimeStampType SelectedTimestampType = TimeStampType.Server;


        public CygNetHistoryEntry()
        {
            //SourceHistoricalEntry = new HistoricalEntry();
        }

        //public HistoricalEntry SourceHistoricalEntry { get; private set; }

        public CygNetHistoryEntry(HistoricalEntry newEntry)
        {
            //SourceHistoricalEntry = newEntry;

            if (newEntry != null)
            {
                switch (newEntry.ValueType)
                {
                    case HistoricalEntryValueType.UTF8:
                        RawValue = newEntry.GetValueAsString();

                        break;
                    case HistoricalEntryValueType.Double:
                        RawValue = newEntry.GetValueAsDouble();

                        break;
                    case HistoricalEntryValueType.Int64:
                        RawValue = newEntry.GetValueAsInt64();

                        break;
                }
            }            

            BaseStatus = newEntry.BaseStatus;
            UserStatus = newEntry.UserStatus;

            RawTimestamp = newEntry.Timestamp;
        }

        public ConvertibleValue Value { get; private set; }

        public double NumericValue { get; private set; }
        public bool IsNumeric => !double.IsNaN(NumericValue);

        private object _rawValue;
        public object RawValue
        {
            get => _rawValue;
            set
            {
                _rawValue = value;

                if (double.TryParse(RawValue.ToString().Trim(), out double thisDouble))
                {
                    NumericValue = thisDouble;
                    Value = new ConvertibleValue(NumericValue);
                }
                else
                {
                    NumericValue = double.NaN;
                    Value = new ConvertibleValue(value);
                }                
            }
        }

        public DateTime RawTimestamp
        {
            get;
            set;
            
        }

        //returns proper timestamp rounded to seconds
        public DateTime AdjustedTimestamp
        {
            get
            { 
                return LocalClientTimestamp;
            }
            set
            {
                //set raw here
            }         
        }
        
        public DateTime LocalClientTimestamp => RawTimestamp.ToLocalTime();

        public DateTime UtcTimestamp => RawTimestamp.ToUniversalTime();

        public BaseStatusFlags BaseStatus { get; set; }

        public UserStatusFlags UserStatus { get; set; }

        public CygNetHistoryEntry DeepCopy()
        {
            var CopiedEntry = new CygNetHistoryEntry()
            {
                BaseStatus = BaseStatus,
                UserStatus = UserStatus,
                RawTimestamp = new DateTime(RawTimestamp.Ticks, RawTimestamp.Kind),


            };

            if (RawValue is double || RawValue is long)
            {
                CopiedEntry.RawValue = RawValue;
            }
            else if (RawValue is string)
            {
                CopiedEntry.RawValue = RawValue.ToString();
            }
            else
            {
                CopiedEntry.RawValue = RawValue;
            }

            return CopiedEntry;
        }
    }
}
