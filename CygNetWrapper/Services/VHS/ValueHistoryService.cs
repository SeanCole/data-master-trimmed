﻿using CygNet.Data.Core;
using CygNet.Data.Historian;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using Serilog;
using Serilog.Context;
using TechneauxUtility;

using static XmlDataModelUtility.GlobalLoggers;

namespace Techneaux.CygNetWrapper.Services.VHS
{
    public class ValueHistoryService : CygNetService
    {
        const ServiceType ThisServiceType = ServiceType.VHS;

        public ValueHistoryService(CygNetDomain parentDomain, ServiceDefinition vhsServiceDef)
            : base(parentDomain, vhsServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ThisServiceType)
                throw new ArgumentException("Service is wrong type");

            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        public ValueHistoryService(CygNetDomain parentDomain, DomainSiteService vhsServiceDef)
            : base(parentDomain, vhsServiceDef, ServiceType.FAC)
        {
            if (vhsServiceDef == null)
                throw new ArgumentException("Service cannot be null");

            // CODE HERE => Check service type
            if (ServiceDefinition.ServiceType != ServiceType.FAC)
                throw new ArgumentException("Service is wrong type");

            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        private CygNet.API.Historian.Client _serviceClient;

        public CygNet.API.Historian.Client ServiceClient
        {
            get
            {
                if (_serviceClient == null)
                    _serviceClient = new CygNet.API.Historian.Client(DomainSiteService);

                return _serviceClient;
            }
        }
        public List<CygNetHistoryEntry> GetHistory(
          PointTag tag,
          DateTime earliestDate,
          DateTime latestDate,
          IProgress<int> progress,
          bool includeUnreliable,
          bool includeNumericOnly,
          CancellationToken ct)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                progress,
                ct,
                numEntries: null,
                includeUnreliable,
                includeNumericOnly,
                ceilingEnabled: false,
                floorEnabled: false,
                ceiling: 0,
                floor: 0,
                HistoryDirection.Forward);
        }
        public List<CygNetHistoryEntry> GetHistory(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            IProgress<int> progress,
            CancellationToken ct)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                progress,
                ct,
                numEntries: null,
                includeUnreliable: true,
                includeNumericOnly: true,
                ceilingEnabled: false,
                floorEnabled: false,
                ceiling: 0,
                floor: 0,
                HistoryDirection.Forward);
        }

        public List<CygNetHistoryEntry> GetHistory(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries: null,
                includeUnreliable: true,
                includeNumericOnly: true,
                ceilingEnabled: false,
                floorEnabled: false,
                ceiling: 0,
                floor: 0,
                HistoryDirection.Forward);
        }
        public List<CygNetHistoryEntry> GetHistoryFilterCeiling(PointTag tag, DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double ceilingFilter)
        {
            return GetHistory(
                        tag,
                        earliestDate,
                        latestDate,
                        null,
                        ct,
                        numEntries: null,
                        includeUnreliable: true,
                        includeNumericOnly: true,
                        ceilingEnabled: true,
                        floorEnabled: false,
                        ceiling: ceilingFilter,
                        floor: 0,
                        HistoryDirection.Forward);
        }
        public List<CygNetHistoryEntry> GetHistoryFilterRange(PointTag tag, DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double ceilingFilter, double floorFilter)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries: null,
                includeUnreliable: true,
                includeNumericOnly: true,
                ceilingEnabled: true,
                floorEnabled: true,
                ceiling: ceilingFilter,
                floor: floorFilter,
                HistoryDirection.Forward);
        }
        public List<CygNetHistoryEntry> GetHistoryFilterFloor(PointTag tag, DateTime earliestDate, DateTime latestDate, IProgress<int> progress, bool includeUnreliable, bool includeNumericOnly, CancellationToken ct, double floorFilter)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries: null,
                includeUnreliable: true,
                includeNumericOnly: true,
                ceilingEnabled: false,
                floorEnabled: true,
                ceiling: 0,
                floor: floorFilter,
                HistoryDirection.Forward);
        }
       

        public List<CygNetHistoryEntry> GetEarliestHistoryInInterval(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            int numEntries,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries: numEntries,
                includeUnreliable: includeUnreliable,
                includeNumericOnly: includeNumericOnly,
                ceilingEnabled : enableCeiling,
                floorEnabled: enableFloor,
                ceiling:ceiling,
                floor:floor,
                HistoryDirection.Forward);
        }

        private enum HistoryDirection
        {
            [Description("Starts enumerating at earliest date in interval, goes forward, getting later values each iteration")]
            Forward,

            [Description("Starts enumerating at the latest date in interval, goes backwward, collecting earlier values each iteration")]
            Reverse
        }

        private IEnumerable<HistoricalEntry> GetHistoryEnumerator(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            HistoryDirection histDirection)
        {
            switch (histDirection)
            {
                case HistoryDirection.Forward:
                    return GetHistEnumForward(tag, earliestDate, latestDate);
                case HistoryDirection.Reverse:
                    return GetHistEnumReverse(tag, earliestDate, latestDate);

                default:
                    throw new ArgumentOutOfRangeException(nameof(histDirection), "Unknown history direction");
            }
        }

        private List<CygNetHistoryEntry> GetHistory(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            IProgress<int> progress,
            CancellationToken ct,
            int? numEntries,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool ceilingEnabled,
            bool floorEnabled,
            double ceiling,
            double floor,
            HistoryDirection histEnumDirection
            )
        {
            
            earliestDate = earliestDate.ToUniversalTime();
            latestDate = latestDate.ToUniversalTime();

            int maxEntries;
            // If numEntries not defined, take all values in the interval
            if (numEntries.HasValue)
            {
                // If num entries is less than 1, argument exception
                if (numEntries < 1)
                    throw new ArgumentOutOfRangeException(nameof(numEntries), "Number of entries must be at least 1");

                maxEntries = numEntries.Value;
            }
            else
            {
                maxEntries = int.MaxValue;
            }

            var histList = new List<CygNetHistoryEntry>();

            try
            {
                var hist = GetHistoryEnumerator(tag, earliestDate, latestDate, histEnumDirection);

               var currentIntProgress = 0;
                var totalMinutes = (latestDate - earliestDate).TotalMinutes;
               var counter = 0;
                progress?.Report(0);

                foreach (var entry in hist)
                {
                    ct.ThrowIfCancellationRequested();

                    // Include unreliable as needed
                    if (!includeUnreliable)
                    {
                        // If entry is unreliable, just continue to next one
                        if (entry.BaseStatus.HasFlag(BaseStatusFlags.Unreliable))
                            continue;
                    }
                    

                    var newHistEntry = new CygNetHistoryEntry(entry);

                    // If only numeric entries included, and value is not numeric, continue to next value
                    if (includeNumericOnly)
                    {
                        if (!newHistEntry.IsNumeric)
                            continue;
                        var pointValue = newHistEntry.NumericValue;

                        if (ceilingEnabled && floorEnabled)
                        {
                            if (pointValue > ceiling || pointValue < floor)
                                continue;
                        }
                        else if (ceilingEnabled && !floorEnabled)
                        {
                            if (pointValue > ceiling)
                                continue;
                        }
                        else if (!ceilingEnabled && floorEnabled)
                        {
                            if (pointValue < floor)
                                continue;
                        }

                    }



                    // If this history entry is one of several with same timestamp, take only the first one
                    if (entry.TimeOrdinal > 0)
                    {
                        continue;
                    }


                    // Check if timestamp in range
                    if (entry.Timestamp < earliestDate || entry.Timestamp > latestDate)
                        continue;

                    // All checks passed, add new entry and increment counter
                    histList.Add(newHistEntry);
                    counter++;

                    // Check if max entries reached
                    if (counter >= maxEntries)
                        break;

                    // Update progress
                    var currentProgress = Math.Floor(100 * Math.Max((double)counter / maxEntries,
                                                         (entry.Timestamp - earliestDate).TotalMinutes) / totalMinutes);

                    if (currentProgress > currentIntProgress)
                    {
                        currentIntProgress = (int)currentProgress;
                        currentIntProgress = currentIntProgress < 0 ? 0 : currentIntProgress;
                        currentIntProgress = currentIntProgress > 100 ? 100 : currentIntProgress;

                        //Console.WriteLine($"Progress={currentIntProgress}");

                        progress?.Report(currentIntProgress);
                    }
                }
            }
            catch (MessagingException ex) when (ex.Code == 11)
            {
                // Eat exceptions where not a history point
            }
            catch (OperationCanceledException)
            {
                throw;
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception while getting history entries");

                throw;
            }
            finally
            {
                progress?.Report(100);
            }
            return histList;
        }
        


        public List<CygNetHistoryEntry> GetLatestHistoryInInterval(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            int? numEntries,
            bool includeUnreliable,
            bool includeNumericOnly)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries: numEntries,
                includeUnreliable: includeUnreliable,
                includeNumericOnly: includeNumericOnly,
                ceilingEnabled: false,
                floorEnabled: false,
                ceiling: 0,
                floor: 0,
                HistoryDirection.Reverse);
        }

        public List<CygNetHistoryEntry> GetLatestHistoryInInterval(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            IProgress<int> progress,
            int? numEntries,
            bool includeUnreliable,
            bool includeNumericOnly)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                progress,
                ct,
                numEntries: numEntries,
                includeUnreliable: includeUnreliable,
                includeNumericOnly: includeNumericOnly,
                ceilingEnabled: false,
                floorEnabled: false,
                ceiling: 0,
                floor: 0,
                HistoryDirection.Reverse);
        }

        public List<CygNetHistoryEntry> GetLatestHistoryInIntervalWithFilter(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate,
            CancellationToken ct,
            int? numEntries,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool ceilingEnabled,
            bool floorEnabled,
            double ceiling,
            double floor)
        {
            return GetHistory(
                tag,
                earliestDate,
                latestDate,
                null,
                ct,
                numEntries: numEntries,
                includeUnreliable: includeUnreliable,
                includeNumericOnly: includeNumericOnly,
                ceilingEnabled: ceilingEnabled,
                floorEnabled: floorEnabled,
                ceiling: ceiling,
                floor: floor,
                HistoryDirection.Reverse);
        }


        //public List<CygNetHistoryEntry> GetHistoryReverse(
        //    PointTag tag,
        //    DateTime earliestDate,
        //    DateTime latestDate,
        //    CancellationToken ct,
        //    int? numEntries = null)
        //{
        //    var hist = GetHistEnumReverse(tag, earliestDate, latestDate);

        //    var timeOffset = GetServiceTimezoneOffset();

        //    var maxEntries = numEntries ?? int.MaxValue;
        //    var counter = 0;

        //    var histList = new List<CygNetHistoryEntry>();
        //    foreach (var entry in hist)
        //    {
        //        var newHistEntry = new CygNetHistoryEntry(entry)
        //        {
        //            ServerUtcOffset = timeOffset
        //        };

        //        histList.Add(newHistEntry);

        //        counter++;

        //        if (counter >= maxEntries)
        //            break;

        //        ct.ThrowIfCancellationRequested();
        //    }

        //    return histList;
        //}

        private IEnumerable<HistoricalEntry> GetHistEnumForward(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate)
        {
            var thisName = new Name { ID = tag.GetTagPointIdFull() };

           var hist = ServiceClient.GetHistoricalEntries(
                thisName,
                earliestDate.ToUniversalTime(),
                latestDate.ToUniversalTime().AddSeconds(10),
                false);
            //foreach (var temp in hist)
            //{
            //    if (temp.Timestamp < earliestDate.ToUniversalTime())
            //    {
            //         Console.WriteLine("d");
            //    }
            //}
            return hist.SkipWhile(entry => entry.Timestamp < earliestDate.ToUniversalTime());
        }

        private IEnumerable<HistoricalEntry> GetHistEnumReverse(
            PointTag tag,
            DateTime earliestDate,
            DateTime latestDate)
        {
            var thisName = new Name { ID = tag.GetTagPointIdFull() };

            var hist = ServiceClient.GetHistoricalEntriesReverse(
                thisName,
                earliestDate.ToUniversalTime(),
                latestDate.ToUniversalTime(),
                false);

            return hist;
        }

        public bool HasHistoryInInterval(
            PointTag pointTag,
            DateTime earliestDate,
            DateTime latestDate,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            var theHist = GetEarliestHistoryInInterval(
                pointTag,
                earliestDate,
                latestDate,
                CancellationToken.None,
                1,
                includeUnreliable,
                includeNumericOnly,
                enableCeiling,
                enableFloor,
                ceiling,
                floor);

            if (theHist != null && theHist.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool HasHistory(
            PointTag pointTag,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            var earliestDate = DateTime.Now.AddYears(-20);
            var latestDate = DateTime.Now;

            return HasHistoryInInterval(
                pointTag,
                earliestDate,
                latestDate,
                includeUnreliable,
                includeNumericOnly,
                enableCeiling,
                enableFloor,
                ceiling,
                floor
                );
        }

        public bool HasHistoryBefore(
            PointTag pointTag,
            DateTime latestDate,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            var earliestDate = DateTime.Now.AddYears(-20);

            return HasHistoryInInterval(
                pointTag,
                earliestDate,
                latestDate,
                includeUnreliable,
                includeNumericOnly,
                enableCeiling,
                enableFloor,
                ceiling,
                floor);
        }

        public bool HasHistoryAfter(
            PointTag pointTag,
            DateTime earliestDate,
            bool includeUnreliable,
            bool includeNumericOnly,
            bool enableCeiling,
            bool enableFloor,
            double ceiling,
            double floor)
        {
            var latestDate = DateTime.Now;

            return HasHistoryInInterval(
                pointTag,
                earliestDate,
                latestDate,
                includeUnreliable,
                includeNumericOnly,
                enableCeiling,
                enableFloor,
                ceiling,
                floor);
        }


        public NameStatistics GetHistStats(PointTag pointTag)
        {
            var thisName = new Name { ID = pointTag.GetTagPointIdFull() };

            var histStats = ServiceClient.GetNameStatistics(thisName);

            return histStats;
        }


        //Public Function DeleteHistoryByDate(pointTag As String, earliestDate As Date, latestDate As Date) As Boolean
        //    ' Test if the vhs is ready
        //    If Not Me.IsReady Then Return False

        //    If HistoryAvailableInInterval(pointTag, earliestDate, latestDate) Then
        //        Dim myDelValReq As New CxVhsLib.DeleteHistoryValueExReq
        //        Dim myResp As New CxVhsLib.DeleteHistoryValueExResp

        //        myDelValReq.TagStringEx = New CxVhsLib.HistoryTagStringEx With {.TagString = pointTag}
        //myDelValReq.operations = 1

        //        Dim numRetryAttempts As Integer = 0
        //        Dim deletionSuccess As Boolean = False

        //        Do Until deletionSuccess OrElse numRetryAttempts = 4
        //            For Each entry As CxVhsLib.ValueEntryEx In GetPointHistory(pointTag, earliestDate, latestDate)
        //                myDelValReq.ValueEntryEx = entry
        //                _vhsClient.DeleteHistoryValueEx(myDelValReq, myResp)
        //            Next
        //            deletionSuccess = Not HistoryAvailableInInterval(pointTag, earliestDate, latestDate)
        //            numRetryAttempts += 1
        //        Loop
        //        If deletionSuccess Then
        //            Return True
        //        Else
        //            Return False
        //        End If
        //    Else
        //        Return True
        //    End If
        //End Function

        //Public Function DeleteAllHistory(pointTag As String) As Boolean
        //    ' Test if the vhs is ready
        //    If Not Me.IsReady Then Return False

        //    Dim myPointDelReq As New CxVhsLib.DeleteHistoryPointReq
        //    Dim myResp As New CxVhsLib.DeleteHistoryPointResp

        //    myPointDelReq.TagString = New CxVhsLib.HistoryTagString With {.TagString = pointTag}

        //If _vhsClient.DeleteHistoryPoint(myPointDelReq, myResp) = 0 Then
        //    Return True
        //Else
        //        Return False
        //    End If
        //End Function
    }
}