﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;
using XmlDataModelUtility;
using static XmlDataModelUtility.XmlEnhancedPrimatives;

namespace Techneaux.CygNetWrapper.Services.DDS.Devices
{
    // SEAN HERE (make sure all properties are initialized, DONT implement notifycopydm)

    [XmlRoot(ElementName = "UdcMapping")]
    public class UdcMapping
    {
        [XmlAttribute(AttributeName = "data_element_id")]
        public string ElementId { get; set; } = "";

        [XmlAttribute(AttributeName = "facility")]
        public string Facility { get; set; } = "";

        [XmlAttribute(AttributeName = "UDC")]
        public string Udc { get; set; } = "";
    }


    public class DatagroupElement
    {
        [XmlAttribute(AttributeName = "Id")]
        public string ElmId { get; set; } = "";

        [XmlAttribute(AttributeName = "desc")]
        public string Desc { get; set; } = "";

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; } = "";

        [XmlAttribute(AttributeName = "regNum")]
        public uint RegNum { get; set; } = 0;
    }

    public class ModbusReadWriteBlock
    {
        [XmlAttribute(AttributeName = "Id")]
        public string BlockId { get; set; } = "";

        [XmlAttribute(AttributeName = "regNum")]
        public uint RegNum { get; set; }

        [XmlAttribute(AttributeName = "regOff")]
        public int RegOff { get; set; }

        [XmlAttribute(AttributeName = "regCnt")]
        public uint RegCnt { get; set; }

        [XmlAttribute(AttributeName = "funcCode")]
        public byte FuncCode { get; set; }

        //[XmlAttribute(AttributeName = "regBitLen")]
        //public byte RegBitLen { get; set; }

        [XmlAttribute(AttributeName = "regByteLen")]
        public byte RegByteLen { get; set; }
    }

    [XmlType("DgElements")]
    public class DgElements
    {
        [XmlAttribute("byteOrder")]
        public string ByteOrder { get; set; } = "";

        [XmlAttribute("secLev")]
        public uint SecurityLevel { get; set; } = 4;

        [XmlElement("DgElement")]
        public List<DatagroupElement> ChildElements;
    }


    public class NamedDatagroup
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Name { get; set; }

        [XmlElement(ElementName = "dgElements")]
        //[XmlArray("DgElements")]
        //[XmlArrayItem("DgElement")]
        //public List<DgElements> DgElements { get; set; } = new List<DgElements>();
        public DgElements DgElements { get; set; } = new DgElements();

        [XmlArray(ElementName = "modbusReadBlocks")]
        public List<ModbusReadWriteBlock> ModbusReadBlocks { get; set; } = new List<ModbusReadWriteBlock>();

        [XmlArray(ElementName = "modbusWriteBlocks")]
        public List<ModbusReadWriteBlock> ModbusWriteBlocks { get; set; } = new List<ModbusReadWriteBlock>();

        [XmlAttribute(AttributeName = "niceName")]
        public string NiceName { get; set; }

        // ===== IsDeviceDataGroup ====
        [XmlAttribute(AttributeName = "devDG")]
        public string IsDeviceDataGroupXml
        {
            get => _devDG.ToString();
            set => _devDG = value == "True";
        }
        private bool _devDG;
        [XmlIgnore]
        public bool IsDeviceDataGroup
        {
            get => _devDG;
            set => _devDG = value;
        }


        // ===== AudoAdd ====
        [XmlAttribute(AttributeName = "autoAdd")]
        public uint AudoAddXml
        {
            get => _autoAdd ? (uint)1 : 0;
            set => _autoAdd = value == 1;
        }
        private bool _autoAdd;
        [XmlIgnore]
        public bool AudoAdd
        {
            get => _autoAdd;
            set => _autoAdd = value;
        }

        [XmlAttribute(AttributeName = "baseOrd")]
        public int BaseOrd { get; set; }

        [XmlAttribute(AttributeName = "majorRev")]
        public int MajorRev { get; set; }

        [XmlAttribute(AttributeName = "minorRev")]
        public int MinorRev { get; set; }

        // ===== CanSend ====
        [XmlAttribute(AttributeName = "canSend")]
        public string CanSendXml
        {
            get => _canSend.ToString();
            set => _canSend = value == "True";
        }
        private bool _canSend;
        [XmlIgnore]
        public bool CanSend
        {
            get => _canSend;
            set => _canSend = value;
        }
        // =========      

        // ===== CanRecv ====
        [XmlAttribute(AttributeName = "canRecv")]
        public string CanRecvXml
        {
            get => _canRecv.ToString();
            set => _canRecv = value == "True";
        }
        private bool _canRecv;
        [XmlIgnore]
        public bool CanRecv
        {
            get => _canRecv;
            set => _canRecv = value;
        }
        // =========    

        // ===== IsUccSend ====
        [XmlAttribute(AttributeName = "uccSend")]
        public string IsUccSendXml
        {
            get => _uccSend.ToString();
            set => _uccSend = value == "True";
        }
        private bool _uccSend;
        [XmlIgnore]
        public bool IsUccSend
        {
            get => _uccSend;
            set => _uccSend = value;
        }
        // =========

        // ===== IsUccRecv ====
        [XmlAttribute(AttributeName = "uccRecv")]
        public string IsUccRecvXml
        {
            get => _uccRecv.ToString();
            set => _uccRecv = value == "True";
        }
        private bool _uccRecv;
        [XmlIgnore]
        public bool IsUccRecv
        {
            get => _uccRecv;
            set => _uccRecv = value;
        }
        // =========  

    }

    [XmlRoot(ElementName = "DgDefinition")]
    public class DgDefinition
    {
        [XmlElement(ElementName = "NamedDatagroup")]
        public NamedDatagroup NamedDatagroup { get; set; } = new NamedDatagroup();

        // ===== Copyable ====
        [XmlAttribute(AttributeName = "copyable")]
        public string CopyableXml
        {
            get => _copyable.ToString();
            set => _copyable = value == "True";
        }
        private bool _copyable;
        [XmlIgnore]
        public bool Copyable
        {
            get => _copyable;
            set => _copyable = value;
        }
        // =========

        [XmlAttribute(AttributeName = "format")]
        public string Format { get; set; } = "";

        [XmlRoot(ElementName = "DataGroup")]
        public class DataGroup
        {
            [XmlElement(ElementName = "DataGroupAttributes")]
            public DataGroupAttributes Attributes { get; set; } = new DataGroupAttributes();

            [XmlArray(ElementName = "UdcMappings")]
            public List<UdcMapping> UdcMappings { get; set; } = new List<UdcMapping>();

            [XmlElement(ElementName = "MetaData")]
            public DataGroupMetaData MetaData { get; set; } = new DataGroupMetaData();

            [XmlRoot(ElementName = "DataGroupAttributes")]
            public class DataGroupAttributes
            {
                [XmlElement(ElementName = "Ordinal")]
                public int Ordinal { get; set; } = 0;

                [XmlElement(ElementName = "Description")]
                public string Description { get; set; } = "";

                [XmlElement(ElementName = "FacilityId")]
                public string FacilityId { get; set; } = "";

                // ===== AudoAdd ====
                [XmlElement(ElementName = "BelongsToDevice")]
                public uint BelongsToDeviceXml
                {
                    get => _BelongsToDevice ? (uint)1 : 0;
                    set => _BelongsToDevice = value == 1;
                }
                private bool _BelongsToDevice = false;
                [XmlIgnore]
                public bool BelongsToDevice
                {
                    get => _BelongsToDevice;
                    set => _BelongsToDevice = value;
                }

                [XmlElement(ElementName = "TxRetentionType")]
                public uint TxRetentionType { get; set; } = 1;

                [XmlElement(ElementName = "TxFailedMax")]
                public uint TxFailedMax { get; set; } = 10;

                [XmlElement(ElementName = "TxSuccessfulMax")]
                public uint TxSuccessfulMax { get; set; } = 20;

                [XmlElement(ElementName = "TxSuccessfulMin")]
                public uint TxSuccessfulMin { get; set; } = 0;

                [XmlElement(ElementName = "TxSuccessfulDays")]
                public uint TxSuccessfulDays { get; set; } = 7;

                [XmlElement(ElementName = "TxReplicationEnabled")]
                public string TxReplicationEnabled { get; set; } = "0";

                // ===== DataGroupInstalled ====
                [XmlElement(ElementName = "DataGroupInstalled")]
                public uint DataGroupInstalledXml
                {
                    get => _DataGroupInstalled ? (uint)1 : 0;
                    set => _DataGroupInstalled = value == 1;
                }
                private bool _DataGroupInstalled = true;
                [XmlIgnore]
                public bool DataGroupInstalled
                {
                    get => _DataGroupInstalled;
                    set => _DataGroupInstalled = value;
                }
                // =========

                // ===== DataGroupRequired ====
                [XmlElement(ElementName = "DataGroupRequired")]
                public uint DataGroupRequiredXml
                {
                    get => _DataGroupRequired ? (uint)1 : 0;
                    set => _DataGroupRequired = value == 1;
                }
                private bool _DataGroupRequired = false;
                [XmlIgnore]
                public bool DataGroupRequired
                {
                    get => _DataGroupRequired;
                    set => _DataGroupRequired = value;
                }
                // =========


                [XmlElement(ElementName = "DataGroupSecurityApp")]
                public string DataGroupSecurityApp { get; set; } = "DDS";

                [XmlElement(ElementName = "DataGroupSecurityEvent")]
                public string DataGroupSecurityEvent { get; set; } = "ACCESS";

                [XmlElement(ElementName = "DataGroupSpecific")]
                public string DataGroupSpecific { get; set; } = "";

                [XmlElement(ElementName = "DataGroupType")]
                public string DataGroupType { get; set; } = "";

                // ===== DataGroupVisible ====
                [XmlElement(ElementName = "DataGroupVisible")]
                public uint DataGroupVisibleXml
                {
                    get => _DataGroupVisible ? (uint)1 : 0;
                    set => _DataGroupVisible = value == 1;
                }
                private bool _DataGroupVisible = true;
                [XmlIgnore]
                public bool DataGroupVisible
                {
                    get => _DataGroupVisible;
                    set => _DataGroupVisible = value;
                }
                // =========

                // ===== DefaultsFacility ====
                [XmlElement(ElementName = "DefaultsFacility")]
                public uint DefaultsFacilityXml
                {
                    get => _DefaultsFacility ? (uint)1 : 0;
                    set => _DefaultsFacility = value == 1;
                }
                private bool _DefaultsFacility = true;
                [XmlIgnore]
                public bool DefaultsFacility
                {
                    get => _DefaultsFacility;
                    set => _DefaultsFacility = value;
                }
                // =========

                // ===== InheritsSecurity ====
                [XmlElement(ElementName = "InheritsSecurity")]
                public uint InheritsSecurityXml
                {
                    get => _InheritsSecurity ? (uint)1 : 0;
                    set => _InheritsSecurity = value == 1;
                }
                private bool _InheritsSecurity = false;
                [XmlIgnore]
                public bool InheritsSecurity
                {
                    get => _InheritsSecurity;
                    set => _InheritsSecurity = value;
                }
                // =========               

                // ===== WebEnabled ====
                [XmlElement(ElementName = "WebEnabled")]
                public uint WebEnabledXml
                {
                    get => _WebEnabled ? (uint)1 : 0;
                    set => _WebEnabled = value == 1;
                }
                private bool _WebEnabled = false;
                [XmlIgnore]
                public bool WebEnabled
                {
                    get => _WebEnabled;
                    set => _WebEnabled = value;
                }
                // =========

                [XmlElement(ElementName = "SystemFlags")]
                public uint SystemFlags { get; set; } = 0;

                // ===== UserDefined ====
                [XmlElement(ElementName = "UserDefined")]
                public uint UserDefinedXml
                {
                    get => _UserDefined ? (uint)1 : 0;
                    set => _UserDefined = value == 1;
                }
                private bool _UserDefined = false;
                [XmlIgnore]
                public bool UserDefined
                {
                    get => _UserDefined;
                    set => _UserDefined = value;
                }
                // =========
            }

            [XmlRoot(ElementName = "MetaData")]
            public class DataGroupMetaData
            {
                [XmlElement(ElementName = "DgDataStore")]
                public DgDataStore DataStore { get; set; } = new DgDataStore();


                [XmlRoot(ElementName = "DgDataStore")]
                public class DgDataStore
                {
                    [XmlElement(ElementName = "DgDefinition")]
                    public DgDefinition DgDefinition { get; set; } = new DgDefinition();

                    [XmlAttribute(AttributeName = "version")]
                    public uint Version { get; set; } = 1;
                }
            }
        }
    }


    [XmlRoot(ElementName = "UisCommand")]
    public class UisCommand
    {
        [XmlElement(ElementName = "CommandAttributes")]
        public CommandAttributes Attributes { get; set; } = new CommandAttributes();

        [XmlElement(ElementName = "CommandParameters")]
        public string CommandParameters { get; set; } = "";

        [XmlArray(ElementName = "CommandComponents")]
        public List<Component> CommandComponents { get; set; } = new List<Component>();



        [XmlRoot(ElementName = "CommandAttributes")]
        public class CommandAttributes
        {
            private string _Name = "";

            [XmlElement(ElementName = "Name")]
            public string Name
            {
                get => _Name;
                set
                {

                    if (value.Length > 10)
                    {
                        _Name = value.Substring(0, 10).ToUpper();
                    }
                    _Name = value.ToUpper();

                }

            }

            [XmlElement(ElementName = "Facility")]
            public string Facility { get; set; } = "";

            [XmlElement(ElementName = "Type")]
            public string Type { get; set; } = "";

            [XmlElement(ElementName = "Description")]
            public string Description { get; set; } = "";

            // ===== IsVisible ====
            [XmlElement(ElementName = "IsVisible")]
            public uint IsVisibleXml
            {
                get => _IsVisible ? (uint)1 : 0;
                set => _IsVisible = value == 1;
            }
            private bool _IsVisible = true;
            [XmlIgnore]
            public bool IsVisible
            {
                get => _IsVisible;
                set => _IsVisible = value;
            }
            // =========

            [XmlElement(ElementName = "CanBeScheduled")]
            public uint CanBeScheduledXml
            {
                get => _CanBeScheduled ? (uint)1 : 0;
                set => _CanBeScheduled = value == 1;
            }
            private bool _CanBeScheduled = true;
            [XmlIgnore]
            public bool CanBeScheduled
            {
                get => _CanBeScheduled;
                set => _CanBeScheduled = value;
            }
            // =========

            // Convert to Bool01
            [XmlElement(ElementName = "ClientCanInvoke")]
            public string ClientCanInvoke { get; set; } = "1";

            [XmlElement(ElementName = "Flags")]
            public string Flags { get; set; } = "0";

            [XmlElement(ElementName = "SecurityApp")]
            public string SecurityApp { get; set; } = "DDS";

            [XmlElement(ElementName = "SecurityEvent")]
            public string SecurityEvent { get; set; } = "ACCESS";

            // ===== InheritsSecurity ====
            [XmlElement(ElementName = "InheritsSecurity")]
            public uint InheritsSecurityXml
            {
                get => _InheritsSecurity ? (uint)1 : 0;
                set => _InheritsSecurity = value == 1;
            }
            private bool _InheritsSecurity = false;
            [XmlIgnore]
            public bool InheritsSecurity
            {
                get => _InheritsSecurity;
                set => _InheritsSecurity = value;
            }
            // =========

            [XmlElement(ElementName = "Prototype")]
            public uint Prototype { get; set; } = 0;

            [XmlElement(ElementName = "AuditDetail")]
            public string AuditDetail { get; set; } = "Service defined detail";

            [XmlElement(ElementName = "State")]
            public uint State { get; set; } = 0;

            [XmlElement(ElementName = "LockTime")]
            public double LockTime { get; set; } = 0;

            [XmlElement(ElementName = "LockUser")]
            public string LockUser { get; set; } = "";

            [XmlElement(ElementName = "LockComment")]
            public string LockComment { get; set; } = "";

            [XmlElement(ElementName = "Ver1")]
            public uint Ver1 { get; set; } = 0;

            [XmlElement(ElementName = "Ver2")]
            public uint Ver2 { get; set; } = 0;

            [XmlElement(ElementName = "Ver3")]
            public uint Ver3 { get; set; } = 0;

            [XmlElement(ElementName = "Ver4")]
            public uint Ver4 { get; set; } = 0;

            [XmlElement(ElementName = "VerTime")]
            public double VerTime { get; set; } = 0;

            [XmlElement(ElementName = "VerUser")]
            public string VerUser { get; set; } = "";

            [XmlElement(ElementName = "VerComment")]
            public string VerComment { get; set; } = "";
        }

        // UIS command component
        [XmlRoot(ElementName = "Component")]
        public class Component
        {
            [XmlElement(ElementName = "Param")]
            public List<ComponentParam> Param { get; set; } = new List<ComponentParam>();

            [XmlAttribute(AttributeName = "type")]
            public string Type { get; set; } = "";

            [XmlAttribute(AttributeName = "position")]
            public int Position { get; set; } = 0;


            [XmlRoot(ElementName = "Param")]
            public class ComponentParam
            {
                [XmlAttribute(AttributeName = "key")]
                public string Key { get; set; } = "";

                [XmlAttribute(AttributeName = "value")]
                public string Value { get; set; } = "";
            }
        }
    }

    [XmlRoot(ElementName = "DeviceDefinitions")]
    public class DeviceDefinitions
    {
        [XmlElement(ElementName = "Device")]
        public CygNetDevice Device { get; set; } = new CygNetDevice();

        [XmlAttribute(AttributeName = "sourceDDS")]
        public string SourceDds { get; set; } = "";

        //[XmlAttribute(AttributeName = "created")]
        //public string Created { get; set; }

        //[XmlAttribute(AttributeName = "userId")]
        //public string UserId { get; set; }

        [XmlRoot(ElementName = "Device")]
        public class CygNetDevice
        {
            [XmlElement(ElementName = "DeviceAttributes")]
            public DeviceAttributes Attributes { get; set; } = new DeviceAttributes();


            [XmlArray(ElementName = "FacilityLinks", IsNullable = true)]
            public List<FacilityLink> FacilityLinks { get; set; } = new List<FacilityLink>();

            [XmlArray(ElementName = "DataGroups", IsNullable = true)]
            public List<DgDefinition.DataGroup> DataGroups { get; set; } = new List<DgDefinition.DataGroup>();

            [XmlArray(ElementName = "UisCommands", IsNullable = true)]
            public List<UisCommand> UisCommands { get; set; } = new List<UisCommand>();

            [XmlAttribute(AttributeName = "category")]
            public string Category { get; set; } = "RD";

            [XmlAttribute(AttributeName = "device_id")]
            public string Id { get; set; } = "";

            [CategoryOrder("General", 1)]
            [CategoryOrder("Polling", 2)]
            [CategoryOrder("Failover", 3)]
            [CategoryOrder("Special", 4)]
            [Description("Device Properties")]
            [XmlRoot(ElementName = "DeviceAttributes")]
            public class DeviceAttributes
            {
                [ReadOnly(true)]
                [Category("General")]
                [DisplayName("Device Type")]
                [XmlElement(ElementName = "DeviceType")]
                public string DeviceType { get; set; } = "RedLion_Compr_Master";

                [Category("General")]
                [XmlElement(ElementName = "DeviceDescription")]
                public string DeviceDescription { get; set; } = "New Device";

                [Description("Security App")]
                [Category("General")]
                [XmlElement(ElementName = "SecurityApp")]
                public string SecurityApp { get; set; } = "DDS";

                [Category("General")]
                [XmlElement(ElementName = "SecurityEvent")]
                public string SecurityEvent { get; set; } = "ACCESS";

                [Category("General")]
                [XmlElement(ElementName = "DeviceManufacturer")]
                public string DeviceManufacturer { get; set; } = "Techneaux";

                [Category("General")]
                [XmlElement(ElementName = "DeviceModel")]
                public string DeviceModel { get; set; } = "";

                [Category("General")]
                [XmlElement(ElementName = "DeviceRevision")]
                public string DeviceRevision { get; set; } = "";

                [Category("General")]
                [XmlElement(ElementName = "DeviceClass")]
                public string DeviceClass { get; set; } = "";

                // ===== DaylightFlag ====
                [Browsable(false)]
                [XmlElement(ElementName = "DaylightFlag")]
                public string DaylightFlagXml
                {
                    get => _DaylightFlag ? "1" : "0";
                    set => _DaylightFlag = value == "1";
                }
                private bool _DaylightFlag = false;

                [Category("Special")]
                [Description("Daylight Savings Adjust?")]
                [XmlIgnore]
                public bool DaylightFlag
                {
                    get => _DaylightFlag;
                    set => _DaylightFlag = value;
                }
                // =========   

                // ===== DeviceEnabled ====
                [Browsable(false)]
                [XmlElement(ElementName = "DeviceEnabled")]
                public uint DeviceEnabledXml
                {
                    get => _DeviceEnabled ? (uint)1 : 0;
                    set => _DeviceEnabled = value == 1;
                }
                private bool _DeviceEnabled;

                [Category("General")]
                [Description("Device Enabled?")]
                [XmlIgnore]
                public bool DeviceEnabled
                {
                    get => _DeviceEnabled;
                    set => _DeviceEnabled = value;
                }
                // =========      

                // ===== TimezoneCorrection ====

                [Browsable(false)]
                [XmlElement(ElementName = "TimezoneCorrection")]
                public string TimezoneCorrectionXml
                {
                    get => _TimezoneCorrection ? "1" : "0";
                    set => _TimezoneCorrection = value == "1";
                }
                private bool _TimezoneCorrection = false;
                [Category("Special")]
                [XmlIgnore]
                public bool TimezoneCorrection
                {
                    get => _TimezoneCorrection;
                    set => _TimezoneCorrection = value;
                }
                // =========

                [XmlElement(ElementName = "DeviceAddress")]
                public uint DeviceAddress { get; set; }

                // ===== WakeEnabled ====
                [Browsable(false)]
                [XmlElement(ElementName = "WakeEnabled")]
                public uint WakeEnabledXml
                {
                    get => _WakeEnabled ? (uint)1 : 0;
                    set => _WakeEnabled = value == 1;
                }
                private bool _WakeEnabled;
                [Category("Special")]
                [XmlIgnore]
                public bool WakeEnabled
                {
                    get => _WakeEnabled;
                    set => _WakeEnabled = value;
                }
                // =========

                // ===== CryoutEnabled ====
                [Browsable(false)]
                [XmlElement(ElementName = "CryoutEnabled")]
                public uint CryoutEnabledXml
                {
                    get => _CryoutEnabled ? (uint)1 : 0;
                    set => _CryoutEnabled = value == 1;
                }
                private bool _CryoutEnabled;
                [XmlIgnore]
                [Category("Special")]
                public bool CryoutEnabled
                {
                    get => _CryoutEnabled;
                    set => _CryoutEnabled = value;
                }
                // =========

                // ===== CommFailoverEnabled ====
                [Browsable(false)]
                [XmlElement(ElementName = "CommFailoverEnabled")]
                public uint CommFailoverEnabledXml
                {
                    get => _CommFailoverEnabled ? (uint)1 : 0;
                    set => _CommFailoverEnabled = value == 1;
                }
                private bool _CommFailoverEnabled;
                [XmlIgnore]
                [Category("Failover")]
                public bool CommFailoverEnabled
                {
                    get => _CommFailoverEnabled;
                    set => _CommFailoverEnabled = value;
                }
                // =========

                // ===== FailoverControlMessages ====
                [Browsable(false)]
                [XmlElement(ElementName = "FailoverControlMessages")]
                public uint FailoverControlMessagesXml
                {
                    get => _FailoverControlMessages ? (uint)1 : 0;
                    set => _FailoverControlMessages = value == 1;
                }
                private bool _FailoverControlMessages;
                [Category("Failover")]
                [XmlIgnore]
                public bool FailoverControlMessages
                {
                    get => _FailoverControlMessages;
                    set => _FailoverControlMessages = value;
                }
                // =========

                // ===== FailbackOnFailure ====
                [Browsable(false)]
                [XmlElement(ElementName = "FailbackOnFailure")]
                public uint FailbackOnFailureXml
                {
                    get => _FailbackOnFailure ? (uint)1 : 0;
                    set => _FailbackOnFailure = value == 1;
                }
                private bool _FailbackOnFailure;
                [XmlIgnore]
                [Category("Failover")]
                public bool FailbackOnFailure
                {
                    get => _FailbackOnFailure;
                    set => _FailbackOnFailure = value;
                }
                // =========

                // ===== FailoverSequence ====
                [Browsable(false)]
                [XmlElement(ElementName = "FailoverSequence")]
                public uint FailoverSequenceXml
                {
                    get => _FailoverSequence ? (uint)1 : 0;
                    set => _FailoverSequence = value == 1;
                }
                private bool _FailoverSequence;
                [XmlIgnore]
                [Category("Failover")]
                public bool FailoverSequence
                {
                    get => _FailoverSequence;
                    set => _FailoverSequence = value;
                }
                // =========

                [XmlElement(ElementName = "CommunicationSelection")]
                public string CommunicationSelection { get; set; } = "0";

                [Category("Polling")]
                [XmlElement(ElementName = "CommunicationId1")]
                public string CommunicationId1 { get; set; } = "1";

                [Category("Polling")]
                [XmlElement(ElementName = "CommunicationId2")]
                public string CommunicationId2 { get; set; } = "";

                [Category("Polling")]
                [XmlElement(ElementName = "CommunicationId3")]
                public string CommunicationId3 { get; set; } = "";

                [Category("Polling")]
                [XmlElement(ElementName = "PollingAttemptLimit1")]
                public uint PollingAttemptLimit1 { get; set; } = 1;

                [Category("Polling")]
                [XmlElement(ElementName = "PollingAttemptLimit2")]
                public uint PollingAttemptLimit2 { get; set; } = 1;

                [Category("Polling")]
                [XmlElement(ElementName = "PollingAttemptLimit3")]
                public uint PollingAttemptLimit3 { get; set; } = 1;

                [Category("Polling")]
                [XmlElement(ElementName = "MessageTimeout1")]
                public uint MessageTimeout1Ms { get; set; } = 5000;

                [Category("Polling")]
                [XmlElement(ElementName = "MessageTimeout2")]
                public uint MessageTimeout2Ms { get; set; } = 5000;

                [Category("Polling")]
                [XmlElement(ElementName = "MessageTimeout3")]
                public uint MessageTimeout3Ms { get; set; } = 5000;

                [Category("Polling")]
                [XmlElement(ElementName = "MessageDelay1")]
                public uint MessageDelay1 { get; set; } = 100;

                [Category("Polling")]
                [XmlElement(ElementName = "MessageDelay2")]
                public uint MessageDelay2 { get; set; } = 100;

                [Category("Polling")]
                [XmlElement(ElementName = "MessageDelay3")]
                public uint MessageDelay3 { get; set; } = 100;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverSeconds1")]
                public uint FailoverSeconds1 { get; set; } = 0;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverSeconds2")]
                public uint FailoverSeconds2 { get; set; } = 0;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverSeconds3")]
                public uint FailoverSeconds3 { get; set; } = 0;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverSecondsSubsequent1")]
                public uint FailoverSecondsSubsequent1 { get; set; } = 0;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverSecondsSubsequent2")]
                public uint FailoverSecondsSubsequent2 { get; set; } = 0;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverSecondsSubsequent3")]
                public uint FailoverSecondsSubsequent3 { get; set; } = 0;

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverRestoreSeconds2")]
                public uint FailoverRestoreSeconds2 { get; set; }

                [Category("Failover")]
                [XmlElement(ElementName = "FailoverRestoreSeconds3")]
                public uint FailoverRestoreSeconds3 { get; set; }

                // ===== FailoverAvailable2 ====
                [Browsable(false)]
                [XmlElement(ElementName = "FailoverAvailable2")]
                public uint FailoverAvailable2Xml
                {
                    get => _FailoverAvailable2 ? (uint)1 : 0;
                    set => _FailoverAvailable2 = value == 1;
                }
                private bool _FailoverAvailable2;
                [Category("Failover")]
                [XmlIgnore]
                public bool FailoverAvailable2
                {
                    get => _FailoverAvailable2;
                    set => _FailoverAvailable2 = value;
                }
                // =========

                // ===== FailoverAvailable3 ====
                [Browsable(false)]
                [XmlElement(ElementName = "FailoverAvailable3")]
                public uint FailoverAvailable3Xml
                {
                    get => _FailoverAvailable3 ? (uint)1 : 0;
                    set => _FailoverAvailable3 = value == 1;
                }
                private bool _FailoverAvailable3;
                [Category("Failover")]
                [XmlIgnore]
                public bool FailoverAvailable3
                {
                    get => _FailoverAvailable3;
                    set => _FailoverAvailable3 = value;
                }
                // =========       

                [Category("General")]
                [XmlElement(ElementName = "CommSpecificInfo")]
                public string CommSpecificInfo { get; set; } = "";

                [Category("General")]
                [XmlElement(ElementName = "DeviceSpecificInfo")]
                public string DeviceSpecificInfo { get; set; } = "CryoutCmd=;modbusMode=Tcpip;";
            }

            [XmlRoot(ElementName = "FacilityLink")]
            public class FacilityLink
            {
                [XmlAttribute(AttributeName = "ordinal")]
                public int Ordinal { get; set; }

                [XmlAttribute(AttributeName = "id")]
                public string Id { get; set; } = "";
            }
        }
    }

}




