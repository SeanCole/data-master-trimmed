﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CygNet.Data.Core;
using CXDDSLib;
using Techneaux.CygNetWrapper.Services.DDS.Devices;
using XmlDataModelUtility;
using Nito.AsyncEx;
using System.Xml.Serialization;

namespace Techneaux.CygNetWrapper.Services.DDS
{
    public partial class DeviceDefinitionService : CygNetService
    {
        const ServiceType ThisServiceType = ServiceType.DDS;

        private readonly AsyncLazy<DdsClient> _serviceClient;

        public async Task<DdsClient> GetServiceClient()
        {
            return await _serviceClient;
        }

        //public override bool IsServiceAvailable => ServiceClient != null;

        public DeviceDefinitionService(CygNetDomain parentDomain, ServiceDefinition DeviceServiceDef)
            : base(parentDomain, DeviceServiceDef)
        {
            if (ServiceDefinition == null)
                throw new ArgumentException("Service cannot be null");
            if (ServiceDefinition.ServiceType != ThisServiceType)
                throw new ArgumentException("Service is wrong type");
            //AttrCache = new FacilityAttributeCache(this);

            _serviceClient = new AsyncLazy<DdsClient>(async () =>
                await Task.Run(() =>
                {
                    var client = new DdsClient();
                    client.Connect(SiteServiceName);

                    return client;
                }));
            //Task.Run(() => ReadFacServiceAttributesAsync());
        }

        public async Task<CurrentValueService> GetAssociatedUis()
        {
            return (await ParentDomain.GetAssociatedService(this, ServiceType.UIS)) as CurrentValueService;
        }

        public async Task<bool> AddOrReplaceDevice(DeviceDefinitions.CygNetDevice cygDev)
        {
            var DevDefs = new DeviceDefinitions
            {
                SourceDds = SiteServiceName
            };
            
            var newDevArray =(object)new List<string>{cygDev.Id}.ToArray();
            DevDefs.Device =cygDev;
            
            var root = new XmlRootAttribute("DeviceDefinitions");
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add("", "");

            var devXml = XmlFileSerialization<DeviceDefinitions>.GetXmlString(DevDefs, root, ns);
            var temp = "<?xml version=\"1.0\" encoding=\"utf-16\"?>";
            devXml = devXml.Replace(temp, "");
            var servClient = await GetServiceClient();
           
            var namedIdDataGroupList = DevDefs.Device.DataGroups.Select(x => x.MetaData.DataStore.DgDefinition.NamedDatagroup.Name);
            var dgElementList = DevDefs.Device.DataGroups.Select(x => x.MetaData.DataStore.DgDefinition.NamedDatagroup.DgElements.ChildElements.Select(y => y.ElmId).ToList()).ToList();
            var blockList = DevDefs.Device.DataGroups.Select(x => x.MetaData.DataStore.DgDefinition.NamedDatagroup.ModbusReadBlocks.Select(y => y.BlockId).ToList()).ToList();
            foreach (var id in namedIdDataGroupList)
            {
                var replacementString = $"<NamedDatagroup Id=\"{id}\"";
                var newString = $"<{id}";
                var replacementTag = $"</{id}>";
                var oldTag = "</NamedDatagroup>";
                devXml = devXml.Replace(replacementString, newString);
                var index = devXml.IndexOf(oldTag);
                if(index >= 0)
                {
                    devXml = devXml.Substring(0, index) + replacementTag + devXml.Substring(index + oldTag.Length);
                }

            }
            foreach (var elmList in dgElementList)
            {
                foreach (var elm in elmList)
                {
                    var replacementElm = $"<DgElement Id=\"{elm}\"";
                    var newElm = $"<{elm}";
                    devXml = devXml.Replace(replacementElm, newElm);
                }                
            }
            foreach (var subList in blockList)
            {
                foreach (var block in subList)
                {
                    var replacementBlock = $"<ModbusReadWriteBlock Id=\"{block}\"";
                    var newBlock = $"<{block}";
                    devXml = devXml.Replace(replacementBlock, newBlock);
                }
            }

            servClient.ImportDevices(ref newDevArray, devXml, true);

            return false;
        }

        //public FacilityService(CygNetDomain parentDomain, DomainSiteService facServiceDef)
        //    : base(parentDomain, facServiceDef, ServiceType.FAC)
        //{
        //    if (facServiceDef == null)
        //        throw new ArgumentException("Service cannot be null");

        //    // CODE HERE => Check service type
        //    if (ServiceDefinition.ServiceType != ServiceType.FAC)
        //        throw new ArgumentException("Service is wrong type");

        //    AttrCache = new FacilityAttributeCache(this);

        //    //Task.Run(() => ReadFacServiceAttributesAsync());
        //}

        //public CygNetService AssociatedTrsService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.TRS);
        //    }
        //}

        //public CygNetService AssociatedAudService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.AUD);
        //    }
        //}

        //private AsyncLazy<FacilityService> AssocFac;
        //public CygNetService AssociatedMssService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.MSS);
        //    }
        //}

        //public CygNetService AssociatedMssService
        //{
        //    get
        //    {
        //        return ParentDomain.GetAssociatedService(this, ServiceType.MSS);
        //    }
        //}

        //public async Task<Dictionary<SiteService, PointService>> GetAssociatedPntServices()
        //{
        //    return (await ParentDomain.GetAssociatedUis(this)).ToDictionary(serv => serv.SiteService, serv => serv);
        //}

        //public async Task<Dictionary<SiteService, PointService>> GetAssociatedPntServices()
        //{
        //    return (await ParentDomain.GetAssociatedPntServices(this)).ToDictionary(serv => serv.SiteService, serv => serv);
        //}


    }
}
