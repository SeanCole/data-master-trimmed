﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Serilog;
using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using Serilog.Context;
using TechneauxUtility;
using static XmlDataModelUtility.GlobalLoggers;

namespace TechneauxWpfControls
{
    public static class WpfLicenseCheck
    {
        public async static Task<bool> CheckLicensing()
        {
            try
            {

                if (CvsSubscriptionLicensing.InDeveloperMode)
                    return true;

                var mainWindow = Application.Current.MainWindow as MetroWindow;

                var publicKeyString =
                    CvsSubscriptionLicensing.ReadPublicKeyStringFromAssembly(Assembly.GetExecutingAssembly(),
                        "DM_public.key");

                var licenseStatus = CvsSubscriptionLicensing.TryReadingLicense(publicKeyString);

                var thisMessageBoxOptions = new MetroDialogSettings
                {
                    AnimateHide = false,
                    AnimateShow = false
                };

                switch (licenseStatus)
                {
                    case CvsSubscriptionLicensing.LicenseReadStatus.NoLicenseFileFound:
                        await mainWindow.ShowMessageAsync("License Error!",
                            "Could not find a license file, please put a valid license file in the <License> subfolder for the application.",
                            MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                        return false;
                    case CvsSubscriptionLicensing.LicenseReadStatus.PublicKeyMissing:
                        await mainWindow.ShowMessageAsync("License Error!",
                            "Decryption error, please contact Techneaux.",
                            MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                        return false;
                    case CvsSubscriptionLicensing.LicenseReadStatus.CouldNotReadLicense:
                        await mainWindow.ShowMessageAsync("License Error!",
                            "License file was found but could not be read or was invalid. Please contact Techneaux.",
                            MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                        return false;
                    case CvsSubscriptionLicensing.LicenseReadStatus.MultipleLicensesFound:
                        await mainWindow.ShowMessageAsync("License Error!",
                            "Multiple license files were found, please make sure there is only one license file in the <License> subfolder for the application.",
                            MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                        return false;
                    case CvsSubscriptionLicensing.LicenseReadStatus.ValidLicenseFound:
                        // Check if domain is authorized first
                        if (!CvsSubscriptionLicensing.IsCurrentNetworkDomainAuthorized)
                        {
                            await mainWindow.ShowMessageAsync("License Error!",
                                $"Your current network domain ({Environment.UserDomainName}) is not authorized in the current license. Please contact Techneaux for an updated license.",
                                MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                            return false;
                        }

                        // Check for expiration 
                        if (CvsSubscriptionLicensing.IsCurrentlyExpired)
                        {
                            // Regular subscription is expired, now check for grace period
                            if (CvsSubscriptionLicensing.IsGracePeriodExpired)
                            {
                                await mainWindow.ShowMessageAsync("License Error!",
                                    $"Your license expired on {CvsSubscriptionLicensing.ExpirationDate:MM/dd/yyyy}, " +
                                    $"and your grace period of {CvsSubscriptionLicensing.GracePeriodDays} days for renewal has also passed. " + Environment.NewLine + Environment.NewLine +
                                    $"This client and the service will not operate until a new license is provided. Please contact Techneaux.",
                                    MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                                return false;
                            }
                            else
                            {
                                await mainWindow.ShowMessageAsync("License Expired!",
                                    $"Your license expired on {CvsSubscriptionLicensing.ExpirationDate:MM/dd/yyyy}, " +
                                    $"but you have a {CvsSubscriptionLicensing.GracePeriodDays} day grace period after expiration to renew before the Data Master client and service stop operating." +
                                    Environment.NewLine + Environment.NewLine +
                                    $"You have {CvsSubscriptionLicensing.DaysLeftInGracePeriod} days left in this grace period. Please contact Techneaux to renew or cancel.",
                                    MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                                return false;
                            }
                        }
                        else if (CvsSubscriptionLicensing.DaysBeforeRegularExpiration < 90)
                        {
                            await mainWindow.ShowMessageAsync("License Reminder",
                                $"Your license will expire on {CvsSubscriptionLicensing.ExpirationDate:MM/dd/yyyy}, {CvsSubscriptionLicensing.DaysBeforeRegularExpiration} days from now." +
                                Environment.NewLine + Environment.NewLine +
                                $"After the license expires, you have a {CvsSubscriptionLicensing.GracePeriodDays} day grace period to renew before the Data Master client and service stop operating." +
                                $" Please contact Techneaux to renew or cancel.",
                                MessageDialogStyle.Affirmative, thisMessageBoxOptions);
                            return true;
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            catch (Exception ex)
            {
                using (LogContext.PushProperty(nameof(SpecialLogTypes), SpecialLogTypes.UnhandledException))
                    Log.Error(ex, "Unhandled exception parsing license");

                return false;
            }
            return true;
        }
    }
}
