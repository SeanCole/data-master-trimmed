using System.Windows.Controls;
using System.Windows.Data;

namespace TechneauxWpfControls.CygNetFacilityFiltering.View
{
    /// <summary>
    /// Interaction logic for FacilityFilter.xaml
    /// </summary>
    public partial class FacilityFilter : UserControl
    {
        public FacilityFilter()
        {
            InitializeComponent();
        }

        private void AllFacilitiesFound_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void FacilitySearchSummary_TargetUpdated(object sender, DataTransferEventArgs e)
        {

            GridView gv = FacilitySearchSummary.View as GridView;
            if (gv != null)
            {
                foreach (var c in gv.Columns)
                {
                   
                    if (double.IsNaN(c.Width))
                    {
                        c.Width = c.ActualWidth;
                    }
                    c.Width = double.NaN;
                }
            }
        }
        private void AllFacilitiesFound_TargetUpdated(object sender, DataTransferEventArgs e)
        {

            GridView gv = AllFacilitiesFound.View as GridView;
            if (gv != null)
            {
                foreach (var c in gv.Columns)
                {
                 
                    if (double.IsNaN(c.Width))
                    {
                        c.Width = c.ActualWidth;
                    }
                    c.Width = double.NaN;
                }
            }
        }
    }
}
