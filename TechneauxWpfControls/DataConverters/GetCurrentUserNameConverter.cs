using GenericRuleModel.Helper;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;
using static TechneauxReportingDataModel.SqlHistory.SubOptions.SqlGeneralOptions;

namespace TechneauxWpfControls.DataConverters
{
    public class GetCurrentUserNameConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is GenericRuleModel.Helper.ValueDescription))
            {
                throw new ArgumentException("Invalid object type");
            }

            var newValueDescription = value as ValueDescription;
            if (!Enum.IsDefined(typeof(AuthenticationTypes), (int)newValueDescription.Value))
            {
                throw new ArgumentException("Invalid object type");
            }

            var authType = (AuthenticationTypes)newValueDescription.Value;

            var strParameter = parameter as string;
            if (!string.IsNullOrWhiteSpace(strParameter))
            {
                switch (strParameter)
                {
                    case "vis":
                        if (authType == AuthenticationTypes.Integrated)
                        {

                            return Visibility.Visible; 
                        }
                        else
                        {
                            return Visibility.Collapsed;
                        }
                    case "txt":
                        if (authType == AuthenticationTypes.Integrated)
                        {
                            try
                            {
                                var returnString = "Current User: " + Environment.UserDomainName + "\\" + Environment.UserName;
                                return returnString;
                            }
                            catch
                            {
                                
                                return "Current User: Unknown";
                            }
                        }
                        else
                        {
                            return null;
                        }
                    default:
                        throw new ArgumentException("Invalid parameter value");
                }

            }
            else
            {
                throw new ArgumentException("Invalid parameter value");
            }

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
