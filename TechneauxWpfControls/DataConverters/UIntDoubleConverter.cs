using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;

namespace TechneauxWpfControls.DataConverters
{
    public class UIntDoubleConverter : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return System.Convert.ToDouble(0); 
            }
            if (value as uint? == 0)
            {
                return System.Convert.ToDouble(0);
            }

            return System.Convert.ToDouble(value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return System.Convert.ToUInt32(0);
            }
            if (value as uint? == 0)
            {
                return System.Convert.ToUInt32(0);
            }
            return System.Convert.ToUInt32(value);
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
