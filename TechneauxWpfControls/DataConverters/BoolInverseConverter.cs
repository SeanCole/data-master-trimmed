using System;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Markup;



namespace TechneauxWpfControls.DataConverters
{
    public class BoolInverseConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value == null)
            {
                return false;
            }
            else if (value is bool && (bool)value == false)
            {
                return true;

            }
            else if (value is bool && (bool)value == true)
            {
                return false;
            }
            else
                return false; 
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
