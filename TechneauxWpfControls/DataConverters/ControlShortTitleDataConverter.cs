using System;
using System.Windows.Data;
using System.Globalization;
using System.Reflection;
using System.Windows.Markup;
using XmlDataModelUtility;



namespace TechneauxWpfControls.DataConverters
{
    public class ControlShortTitleDataConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
            if (value == null)
            {
                return "";
            }

            var propertyName = parameter as string;
            var type = value.GetType();

            if(type.BaseType.Equals(typeof(Enum)))
            {
                type = type.DeclaringType;
            }                            
            var property = type.GetProperty(propertyName);

            var attributes = property.GetCustomAttributes(typeof(HelperClasses.ControlTitle), false);
            foreach (HelperClasses.ControlTitle PropertyAtttribute in attributes)
            {
                return PropertyAtttribute.ShortTitle;
            }
            return "";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
        
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

    }
}
