﻿using GenericRuleModel.Rules;
using System.ComponentModel;
using XmlDataModelUtility;

namespace ExpressionRuleModel.Rules
{
    public class CygNetExpressionElement : NotifyCopyDataModel
    {
        public CygNetExpressionElement() : this(true)
        {
        }

        public CygNetExpressionElement(bool IsFullRuleVal = true)
        {
            ExpName = "";
            ExpType = CygNetExpressionType.CygNetRule;
            CygRule = new CygNetExpressionRule(IsFullRuleVal);
            StringOps = new StringOperations();
            FormatConfig = new FormattingOptions();
        }

        [HelperClasses.ControlTitle("Name", "")]
        public string ExpName
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }


        public enum CygNetExpressionType
        {
            [Description("CygNet Rule")]
            CygNetRule,

            [Description("Reference")]
            Reference
        }

        [HelperClasses.ControlTitle("Type", "")]
        public CygNetExpressionType ExpType
        {
            get => GetPropertyValue<CygNetExpressionType>();
            set => SetPropertyValue(value);
        }

        [HelperClasses.ControlTitle("CygNet Rule", "")]
        public CygNetExpressionRule CygRule
        {
            get => GetPropertyValue<CygNetExpressionRule>();
            set => SetPropertyValue(value);
        }

        //Reference configuration
        [HelperClasses.ControlTitle("Reference", "")]
        public string Reference
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        public FormattingOptions FormatConfig
        {
            get => GetPropertyValue<FormattingOptions>();
            set => SetPropertyValue(value);
        }

        [HelperClasses.ControlTitle("String Operation", "")]
        public StringOperations StringOps
        {
            get => GetPropertyValue<StringOperations>();
            set => SetPropertyValue(value);
        }

        [HelperClasses.ControlTitle("Test Value", "")]
        public string TestValue
        {
            get => GetPropertyValue<string>();
            set => SetPropertyValue(value);
        }

        internal bool HasRandomNumberGeneratorChild()
        {
            if (CygRule.CygRule is RandomNumber)
            {
                return true;
            }

            else return false;
        }
    }
}
